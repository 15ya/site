
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Global Public Space Toolkit: From Global Principles to Local Policies and Practice"
date: 2015-01-01
summary: "Despite its importance in promoting sustainable urban development, public space has not been given the attention it deserves in literature and, more importantly, in the global policy arena. Yet there is a growing body of principles and sound policies for improving access to good public space in our  ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/03/Global_Public_Space_Toolkit.jpg.webp?itok=gc77wziJ"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/03/Global_Public_Space_Toolkit.jpg.webp?itok=gc77wziJ"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['public space', 'streetscape']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://unhabitat.org/global-public-space-toolkit-from-global-principles-to-local-policies-and-practice)

> **Abstract** 
>
> Despite its importance in promoting sustainable urban development, public space has not been given the attention it deserves in literature and, more importantly, in the global policy arena. Yet there is a growing body of principles and sound policies for improving access to good public space in our cities, as well as a growing patrimony of good practices from different urban settings around the world. 
This toolkit will be a practical reference for local governments to frame and implement principles, policy recommendations and development initiatives on public space and for central governments to aid their efforts with material support and enabling legislation. It will also serve the purpose of demonstrating the value of the involvement of the citizenry and civil society in securing, developing and managing public space in the city.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Green Infrastructure and Health Guide"
date: 2018-01-01
summary: "The guide was prepared by members of the Oregon Health and Outdoors Initiative—the Oregon Public Health Institute and the Willamette Partnership—in collaboration with the Green Infrastructure Leadership Exchange. The guide proposes green infrastructure solutions to improving prevalent health issues  ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/01/Green-Infrastructure-Health-Guide-cover-232x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/01/Green-Infrastructure-Health-Guide-cover-232x300.jpg"
categories: ['local government', 'planning', 'urban design']
tags: ['urban nature', 'infrastructure', 'urban development', 'health']  
showDate: false
showReadTime: false
---

**Willamette Partnership**, 2018.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/green-infrastructure-and-health-guide-jan-2019/)

> **Abstract** 
>
> The guide was prepared by members of the Oregon Health and Outdoors Initiative—the Oregon Public Health Institute and the Willamette Partnership—in collaboration with the Green Infrastructure Leadership Exchange. The guide proposes green infrastructure solutions to improving prevalent health issues in the United States (like heart disease, cancer, depression, and others) through meaningful work in communities, providing: 
Evidence linking time in green spaces to improvements in health
Health care and green infrastructure key terms
Methods to identify health needs of communities
Methods for community engagement
Design guidelines for green infrastructure siting and design
Steps to evaluate health benefits from green infrastructure


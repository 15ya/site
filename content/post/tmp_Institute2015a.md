
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Public Life Diversity Toolkit 1.0"
date: 2015-01-01
summary: "A prototype for measuring social mixing and economic integration in public space ..."
featureImage: "https://image.isu.pub/160209111144-bf9887304e7ea8e3953805f9d92e22f4/jpg/page_1_thumb_large.jpg"
thumbnail: "https://image.isu.pub/160209111144-bf9887304e7ea8e3953805f9d92e22f4/jpg/page_1_thumb_large.jpg"
categories: ['architecture', 'landscape', 'urban design']
tags: ['gender', 'age', 'participation', 'public space']  
showDate: false
showReadTime: false
---

**Gehl Institute**, 2015.
[DOWNLOAD HERE](https://issuu.com/gehlarchitects/docs/gehl_publiclifediversitytoolkit_pag)

> **Abstract** 
>
> A prototype for measuring social mixing and economic integration in public space



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A Guide to Implementing Coastal Nature-Based Solutions for Singapore"
date: 2022-01-01
summary: "To strengthen resilience to climate change, cities can leverage Nature-based Solutions that could be applied adaptively to simultaneously restore nature and provide human benefits. This guide was jointly developed through research convening a wide range of stakeholders and experts. It aims to docume ..."
featureImage: "https://www.clc.gov.sg/images/default-source/publications/books/cover/nbs-guide.jpg?sfvrsn=a7dde3b_2"
thumbnail: "https://www.clc.gov.sg/images/default-source/publications/books/cover/nbs-guide.jpg?sfvrsn=a7dde3b_2"
categories: ['local government', 'planning', 'urban design']
tags: ['water', 'nbs', 'urban nature', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**Centre for Liveable Cities Singapore**, 2022.
[DOWNLOAD HERE](https://www.clc.gov.sg/research-publications/publications/commentaries-reports/view/a-guide-to-implementing-coastal-nature-based-solutions-for-singapore)

> **Abstract** 
>
> To strengthen resilience to climate change, cities can leverage Nature-based Solutions that could be applied adaptively to simultaneously restore nature and provide human benefits. This guide was jointly developed through research convening a wide range of stakeholders and experts. It aims to document a shared understanding on the important principles and enablers for Nature-based Solutions to be applied in an urban coastal context, to reduce climate risks, regenerate ecosystem services and deliver greater societal gains for a City in Nature.


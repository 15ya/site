
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Strengthening Citizen Action: Communication for Civil Society Organisations"
date: 2016-01-01
summary: "This toolkit has been designed to add to the skills you already use for communicating about your work, as well as advocacy and mobilisingcommunities for civic action. 
The materials in the kit will assist you to:
-Design communication strategies based on research, evidence and a solid understanding  ..."
featureImage: "https://civicus.org/images/Comms_Toolkit.png"
thumbnail: "https://civicus.org/images/Comms_Toolkit.png"
categories: ['planning', 'urban design']
tags: ['governance', 'participation']  
showDate: false
showReadTime: false
---

**CIVIUS**, 2016.
[DOWNLOAD HERE](https://civicus.org/index.php/fr/medias-ressources/150-toolkits/3633-strengthening-citizen-action-communication-for-civil-society-organisations)

> **Abstract** 
>
> This toolkit has been designed to add to the skills you already use for communicating about your work, as well as advocacy and mobilisingcommunities for civic action. 
The materials in the kit will assist you to:
-Design communication strategies based on research, evidence and a solid understanding of the context.
-Identify tipping points for action based on analysis of audiences and their contexts.
-Create effective communication materials that work together, including on social media.
-Implement communication programmes and measure their progress.
-Evaluate the contribution of your communication programme to your organisational effectivenes, and change you are trying to encourage.
It will provide you with some basic information on the fundamentals of creating effective communication programmes to communicate about what you do and encourage people to take action. It also provides ideas and examples of how to create useful and relevant communication materials on the web and social media, how to manage your relationships with the media, and how to stay in touch with your audience and supporters.


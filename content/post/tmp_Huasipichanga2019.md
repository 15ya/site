
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Huasipichanga Methodology: How to organize a co-creation process to reactivate public space?"
date: 2019-01-01
summary: "This manual is thought to guide you through the process of co-creation in the (re)activation of public spaces. It explains every phase you can implement while linking them to a specific tool and validation practice that will help you prove the previous step and facilitate the development of the next ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_Huasipichanga-1024x406.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_Huasipichanga-1024x406.png"
categories: ['planning', 'landscape', 'urban design']
tags: ['participation', 'public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://www.huasipichanga.com/our-story-1)

> **Abstract** 
>
> This manual is thought to guide you through the process of co-creation in the (re)activation of public spaces. It explains every phase you can implement while linking them to a specific tool and validation practice that will help you prove the previous step and facilitate the development of the next one. If you are starting a project and you need a step-by-step process to frame your ideas, this tool is useful for you.


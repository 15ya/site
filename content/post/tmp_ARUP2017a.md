
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Cities Alive: Designing for Urban Childhoods"
date: 2017-01-01
summary: "A child-friendly approach to urban planning is a vital part of creating inclusive cities that work better for everyone. Designing for urban childhoods inspires us to respond positively to the challenges, and sets out actions that can help take us to a more child-friendly future – moving well beyond  ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/c/cities-alive-urban-childhoods.jpg?h=243&mw=340&w=340&hash=01D846C2147D974AEE8620D4ECA51B26"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/c/cities-alive-urban-childhoods.jpg?h=243&mw=340&w=340&hash=01D846C2147D974AEE8620D4ECA51B26"
categories: ['planning', 'landscape', 'urban design']
tags: ['age', 'streetscape', 'public space', 'mobility', 'urban nature', 'participation']  
showDate: false
showReadTime: false
---

**ARUP**, 2017.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/cities-alive-designing-for-urban-childhoods)

> **Abstract** 
>
> A child-friendly approach to urban planning is a vital part of creating inclusive cities that work better for everyone. Designing for urban childhoods inspires us to respond positively to the challenges, and sets out actions that can help take us to a more child-friendly future – moving well beyond simply providing playgrounds.
The amount of time children spend playing outdoors, their ability to get around independently, and their level of contact with nature are strong indicators of how a city is performing, not just for children but for all generations of city dwellers. If cities fail to address the needs of children, they risk economic and cultural impacts as families move away.
Through 40 global case studies, 14 recommended interventions and 15 actions for city leaders, developers and investors and built environment professionals, the report shows how we can create healthier and more inclusive, resilient and competitive cities for us to live, work and grow up in.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "SDG Project Assessment Tool – Volume 1: General Framework"
date: 2020-01-01
summary: "The SDG Project Assessment Tool (referred to as SDG Tool) is developed by UN-Habitat as an offline, digital and user-friendly instrument to guide City Authorities and Delivery Partners in the development of more inclusive, sustainable and effective urban projects. The General Framework of the SDG Pr ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/SDG%20tool%20Vol1.png.webp?itok=uJejJMuq"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/SDG%20tool%20Vol1.png.webp?itok=uJejJMuq"
categories: ['local government', 'planning']
tags: ['sdg']  
showDate: false
showReadTime: false
---

**Global Future Cities Programme**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/sdg-project-assessment-tool-volume-1-general-framework)

> **Abstract** 
>
> The SDG Project Assessment Tool (referred to as SDG Tool) is developed by UN-Habitat as an offline, digital and user-friendly instrument to guide City Authorities and Delivery Partners in the development of more inclusive, sustainable and effective urban projects. The General Framework of the SDG Project Assessment Tool (SDG Tool) comprises the complete list of Sustainability Principles, aligned to the Sustainable Development Goals (SDGs), and the related Performance Criteria. The General Framework represents the substantial component of the SDG Tool. It covers the three thematic pillars of the Global Future Cities Programme: Urban Planning, Transport and Resilience. Starting from this comprehensive set of principles and performance criteria a tailor-made SDG Project Assessment Worksheet will be derived for each project of the Programme.


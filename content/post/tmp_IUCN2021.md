
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for planning and monitoring corporate biodiversity performance"
date: 2021-01-01
summary: "These guidelines offer an approach for developing a corporate-level biodiversity strategic plan, including measurable goals and objectives and a set of core linked  indicators, that will allow companies to measure their biodiversity performance across their operations. The Guidelines can be used by  ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2021-009-En.PNG?itok=VsiMYiXJ"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2021-009-En.PNG?itok=VsiMYiXJ"
categories: ['local government']
tags: ['urban nature', 'sustainability']  
showDate: false
showReadTime: false
---

**IUCN**, 2021.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/49301)

> **Abstract** 
>
> These guidelines offer an approach for developing a corporate-level biodiversity strategic plan, including measurable goals and objectives and a set of core linked  indicators, that will allow companies to measure their biodiversity performance across their operations. The Guidelines can be used by any company in any sector that has impacts and dependencies on biodiversity, whether large or small, national or multinational. They are aimed at sustainability teams, managers and other company staff whose roles include strategic planning and reporting related to biodiversity. The focus is on a full-cycle, results-based management approach (not just risk analyses, goal setting or indicator development), since assessing pressures on biodiversity, and planning and developing measurable goals, are key prerequisites for monitoring. They also explain how, by choosing and using appropriate core indicators and building internal capacity and partnerships, companies can aggregate and use biodiversity data at the corporate level in a meaningful way.


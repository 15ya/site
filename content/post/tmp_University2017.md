
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Multi-Stakeholder Partnerships Tool Guide"
date: 2017-01-01
summary: "As a companion to the MSP Guide, this Tool Guide offers you 60 process tools serving different purposes. Even this large number, is just a sample of the hundreds of tools available. We have chosen these because they are the ones we find especially useful to support MSP processes. 
We have grouped th ..."
featureImage: "https://mspguideorg.files.wordpress.com/2021/12/tool-guide-2.png"
thumbnail: "https://mspguideorg.files.wordpress.com/2021/12/tool-guide-2.png"
categories: ['local government', 'planning', 'urban design']
tags: ['governance', 'participation']  
showDate: false
showReadTime: false
---

**Wageningen University**, 2017.
[DOWNLOAD HERE](https://mspguide.org/the-msp-tool-guide/)

> **Abstract** 
>
> As a companion to the MSP Guide, this Tool Guide offers you 60 process tools serving different purposes. Even this large number, is just a sample of the hundreds of tools available. We have chosen these because they are the ones we find especially useful to support MSP processes. 
We have grouped the tools by six purposes – connection, issue exploration and shared language, divergence, co-creation, convergence, and commitment – inspired by the work of Sam Kaner (2014) and the Rockefeller Foundation’s GATHER guide. 
These purposes often coincide with a particular stage of an MSP: connecting, for example, will usually happen at the start. The six purposes are consistent with Kolb’s experiential learning cycle (Section 4, Principle 7 of the MSP Guide). The learning styles developed by Kolb indicate that some people are competent in divergent learning and others in convergent learning. Different types of learners flourish at different stages of a process.


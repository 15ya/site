
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Planning by Doing (Volume II of Makers on Market)"
date: 2016-01-01
summary: "How small, citizen-powered projects inform large planning decisions ..."
featureImage: "https://image.isu.pub/160303230552-498e407e96d57e1cca1a5ea27483e108/jpg/page_1.jpg"
thumbnail: "https://image.isu.pub/160303230552-498e407e96d57e1cca1a5ea27483e108/jpg/page_1.jpg"
categories: ['planning', 'landscape', 'urban design']
tags: ['governance', 'participation', 'public space', 'streetscape']  
showDate: false
showReadTime: false
---

**Gehl Institute**, 2016.
[DOWNLOAD HERE](https://issuu.com/gehlarchitects/docs/planning_by_doing_print)

> **Abstract** 
>
> How small, citizen-powered projects inform large planning decisions


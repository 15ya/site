
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Open Call for Ideas Handbook"
date: 2021-01-01
summary: "The „Open Call“ is a tool for the joint appropriation and design of public space that mobilises the local community to engage. The temporary activation of public spaces (“enabling spots”) in the frame of an Open Call aims to show its potential uses and to encourage the local community to make them t ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2023/04/PE_Toolbox_-_Cover_Open-Call-for-Ideas-Handbook_PlaceCity-Vienna-1024x497.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2023/04/PE_Toolbox_-_Cover_Open-Call-for-Ideas-Handbook_PlaceCity-Vienna-1024x497.png"
categories: ['planning', 'landscape', 'urban design']
tags: ['public space', 'participation']  
showDate: false
showReadTime: false
---

**Superwien**, 2021.
[DOWNLOAD HERE](https://placemaking-europe.eu/tool/open-call-for-ideas-handbook/)

> **Abstract** 
>
> The „Open Call“ is a tool for the joint appropriation and design of public space that mobilises the local community to engage. The temporary activation of public spaces (“enabling spots”) in the frame of an Open Call aims to show its potential uses and to encourage the local community to make them their own. By getting the chance to test and experiment different uses in public space without taking any risk, the neighbourhood is invited to shape their immediate living environment. On the long- term, the community-led activities (including events and construction projects) that have been initiated by the Open Call can contribute to strengthening the neighbourhood. Moreover, through the active engagement with public space, awareness of one‘s own living environment is intensified.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Roadmaps Towards Resilience Through NBS"
date: 2022-01-01
summary: "In this section a roadmapping process is outlined which allows cities engage different urban stakeholders around the planning and project development of NBS. It includes the development of joint ambitions and visions, the assessment of the status quo of an urban system, as well as the co-creation of ..."
featureImage: "https://unalab.eu/sites/default/files/2022-02/RM_video_1.JPG"
thumbnail: "https://unalab.eu/sites/default/files/2022-02/RM_video_1.JPG"
categories: ['local government', 'planning']
tags: ['sustainability', 'urban nature', 'nbs', 'resilience', 'climate change', 'governance']  
showDate: false
showReadTime: false
---

**UNaLab**, 2022.
[DOWNLOAD HERE](https://unalab.eu/en/roadmaps-towards-resilience-through-nbs)

> **Abstract** 
>
> In this section a roadmapping process is outlined which allows cities engage different urban stakeholders around the planning and project development of NBS. It includes the development of joint ambitions and visions, the assessment of the status quo of an urban system, as well as the co-creation of roadmaps towards a more climate resilient future. The process has been tested and implemented in the 5 UNaLab Follower Cities Stavanger (NO), Cannes (FR), Prague (CZ), Castellón (ES) and Basaksehir (TU).



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Handbook of Territorial and Local Development Strategies"
date: 2022-01-01
summary: "All places are important to the future wellbeing of Europe. The European Union (EU) is committed to ensuring that the development potential of places is uncovered and valorised. Integrated territorial and local development strategies promoted by EU cohesion policy are relevant tools to sustain this  ..."
featureImage: "https://publications.jrc.ec.europa.eu/repository/cover/JRC130788_cover.jpg"
thumbnail: "https://publications.jrc.ec.europa.eu/repository/cover/JRC130788_cover.jpg"
categories: ['local government', 'planning']
tags: ['urban development', 'housing', 'services', 'land use', 'governance']  
showDate: false
showReadTime: false
---

**JRC**, 2022.
[DOWNLOAD HERE](https://publications.jrc.ec.europa.eu/repository/handle/JRC130788)

> **Abstract** 
>
> All places are important to the future wellbeing of Europe. The European Union (EU) is committed to ensuring that the development potential of places is uncovered and valorised. Integrated territorial and local development strategies promoted by EU cohesion policy are relevant tools to sustain this process.
The ‘Handbook of Territorial and Local Development Strategies’ provides valuable knowledge on how to design and implement integrated strategies in areas other than urban areas. It aims to serve managing authorities of operational programmes, local strategy owners as well as other stakeholders involved in the process. 
The Handbook is a joint initiative by the European Commission’s Directorates-General for Regional and Urban Policy (DG REGIO) and the Joint Research Centre (JRC), and it benefits from the active contribution of policymakers, practitioners and scholars.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Playful Cities Toolkit: Resources for reclaiming play in cities"
date: 2021-01-01
summary: "Play is essential for children’s well-being and happiness, as well as for their healthy physical and emotional growth. The built environment is a critical play and learning resource for children: in cities, there is constant opportunity to realise the potential of playful learning experiences. The c ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/p/playful-cities-toolkit-cover.jpg?h=240&mw=340&w=340&hash=D62EC22CEB1CCDD853E9D6311E098B2B"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/p/playful-cities-toolkit-cover.jpg?h=240&mw=340&w=340&hash=D62EC22CEB1CCDD853E9D6311E098B2B"
categories: ['planning', 'landscape', 'urban design']
tags: ['age', 'urban nature', 'streetscape', 'infrastructure', 'participation', 'governance', 'health']  
showDate: false
showReadTime: false
---

**ARUP**, 2021.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/playful-cities-toolkit-resources-for-reclaiming-play-in-cities)

> **Abstract** 
>
> Play is essential for children’s well-being and happiness, as well as for their healthy physical and emotional growth. The built environment is a critical play and learning resource for children: in cities, there is constant opportunity to realise the potential of playful learning experiences. The city scape invites and nurtures children’s innate curiosity during play, whilst providing the interactions necessary for children to practice the relational skills that enable our communities and society to be adaptable, sustainable and flourishing. 
But to date there has been limited guidance on how to design neighbourhoods that encourage play beyond playgrounds, and that consider all of the environments in which children spend their time, including public spaces. The Playful Cities Toolkit, developed by Arup and the LEGO Foundation in partnership with the Real Play Coalition, provides a range of resources to understand the complexity of play in cities, to guide the design of play-based interventions, and to measure their impact. By bringing together the fields of play and placemaking, its application maximises the chance for children to engage in playful activities in their life.


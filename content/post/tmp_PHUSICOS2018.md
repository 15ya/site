
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guiding Framework for Tailored Living Lab Establishment"
date: 2018-01-01
summary: "Multi-stakeholder participation is an overarching issue of PHUSICOS and, as such, forms a foundation to foster innovation at all levels and at all case study sites. Workpackage 3 is dedicated to employ a Living Lab approach as key mechanism of local stakeholder involvement for the purpose of success ..."
featureImage: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/LL%20framework.png?itok=ASkVkR_L"
thumbnail: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/LL%20framework.png?itok=ASkVkR_L"
categories: ['planning', 'local government']
tags: ['participation', 'governance', 'nbs', 'resilience']  
showDate: false
showReadTime: false
---

**PHUSICOS**, 2018.
[DOWNLOAD HERE](https://networknature.eu/product/29398)

> **Abstract** 
>
> Multi-stakeholder participation is an overarching issue of PHUSICOS and, as such, forms a foundation to foster innovation at all levels and at all case study sites. Workpackage 3 is dedicated to employ a Living Lab approach as key mechanism of local stakeholder involvement for the purpose of successfully accompanying the intended NBSs’ design, planning, implementation and evaluation. This report outlines a framework for initiating participatory processes and establishing Living Labs at the project’s demonstrator and concept case study sites.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Pro-Poor Climate Action in Informal Settlements"
date: 2018-01-01
summary: "Urbanization is one of the global megatrends of our time, unstoppable and irreversible. In 30 years, two-thirds of the world’s population will live in urban areas; 90 per cent of this urban growth will take place in less developed regions such as East Asia, South Asia, and Sub-Saharan Africa. These  ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2018/12/publication_pic.png.webp?itok=7am0jWRh"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2018/12/publication_pic.png.webp?itok=7am0jWRh"
categories: ['local government', 'planning']
tags: ['informality', 'governance', 'sustainability', 'resilience', 'climate change', 'equity']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2018.
[DOWNLOAD HERE](https://unhabitat.org/pro-poor-climate-action-in-informal-settlement)

> **Abstract** 
>
> Urbanization is one of the global megatrends of our time, unstoppable and irreversible. In 30 years, two-thirds of the world’s population will live in urban areas; 90 per cent of this urban growth will take place in less developed regions such as East Asia, South Asia, and Sub-Saharan Africa. These are regions where capacity and resources are already constrained, and development challenges are ever more complex and concentrated. Urbanization in such areas is largely unplanned, fuelling the continuous growth of informal settlements, the physical manifestation of urban poverty and inequality. Currently home to some 1 billion people, informal settlements are where the impact of climate change is most acute and where resilience must be strengthened.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Park[ing] Day for Fitness"
date: 2022-01-01
summary: "This tool was inspired by the Parking Day concept by Rebar of taking over a parking spot for your own human-scale and community use. The mission of the Park(ing) Day for Fitness events is to draw attention to the many (untapped) possibilities that our cities offer for physical activity, the need for ..."
featureImage: "https://parkingdayforfitness.bgbeactive.org/images/resources/12-PPT-Manual.png"
thumbnail: "https://parkingdayforfitness.bgbeactive.org/images/resources/12-PPT-Manual.png"
categories: ['landscape', 'urban design']
tags: ['public space', 'streetscape']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2022.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/parking-day-for-fitness/)

> **Abstract** 
>
> This tool was inspired by the Parking Day concept by Rebar of taking over a parking spot for your own human-scale and community use. The mission of the Park(ing) Day for Fitness events is to draw attention to the many (untapped) possibilities that our cities offer for physical activity, the need for more open spaces where people can exercise safely, and further, to provoke a public debate on how to adapt in times of crisis and improve the quality of the urban environment. In the instructions you will find a handful of inspiration for all levels: from light interventions to those that require a little more time, as well as practical tips and tricks for turning a parking spot into a place for workout sessions, ping-pong tournaments or chalk games of your childhood.


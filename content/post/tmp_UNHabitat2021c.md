
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Financing Sustainable Urban Development"
date: 2021-01-01
summary: "The intention of the initiative is to identify lessons on how to strengthen urban finance, building on a range of city case studies from countries selected to represent different levels of urbanisation and structural transformation, examples, and discussions with key stakeholders. ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/06/Financing%20Sustainable%20Urban%20Development.JPG.webp?itok=ilwgrNie"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/06/Financing%20Sustainable%20Urban%20Development.JPG.webp?itok=ilwgrNie"
categories: ['local government']
tags: ['finance', 'governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/financing-sustainable-urban-development)

> **Abstract** 
>
> The intention of the initiative is to identify lessons on how to strengthen urban finance, building on a range of city case studies from countries selected to represent different levels of urbanisation and structural transformation, examples, and discussions with key stakeholders.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "CoM SSA SEACAP Toolbox"
date: 2019-01-01
summary: "The CoM SSA SEACAP Toolbox provides an easy-to-use step-by-step guide to support the development of a SEACAP from start to finish. It is a hands-on tool, designed as a series of slide presentations, presented as individual modules, to support local governments implement the recommendations in the SE ..."
featureImage: "https://comssa.org/resources/front/images/logo_header_en.png"
thumbnail: "https://comssa.org/resources/front/images/logo_header_en.png"
categories: ['local government', 'planning', 'urban design']
tags: ['sustainability', 'governance']  
showDate: false
showReadTime: false
---

**CoM**, 2019.
[DOWNLOAD HERE](https://comssa.org/seacap-toolbox/en)

> **Abstract** 
>
> The CoM SSA SEACAP Toolbox provides an easy-to-use step-by-step guide to support the development of a SEACAP from start to finish. It is a hands-on tool, designed as a series of slide presentations, presented as individual modules, to support local governments implement the recommendations in the SEACAP guidebook. It complements the existing SEACAP Guidebook.


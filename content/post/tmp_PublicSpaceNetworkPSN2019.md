
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Placemaking Toolkit in rapid urbanization context"
date: 2019-01-01
summary: "This toolkit developed by Public Space Network aims to provide guidelines to anybody who wants to transform a neglected public open space in their area into a clean and safe community space, such as a community park, a playground or a safe vibrant pedestrian-friendly street. It is written within the ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'urban design']
tags: ['informality', 'public space', 'streetscape', 'participation']  
showDate: false
showReadTime: false
---

**Public Space Network**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/rapid-urbanization-context/)

> **Abstract** 
>
> This toolkit developed by Public Space Network aims to provide guidelines to anybody who wants to transform a neglected public open space in their area into a clean and safe community space, such as a community park, a playground or a safe vibrant pedestrian-friendly street. It is written within the context of cities that face challenges of rapid urbanization while having limited resources. The toolkit therefore assembles experiences from working with community groups that have transformed such spaces across Nairobi, Kenya and is meant to guide low-cost interventions in public space transformation. However, the guide is also very useful for residents of more developed cities to get inspired and for anyone who is determined to change the status quo and change their neighborhood through community efforts and limited resources.


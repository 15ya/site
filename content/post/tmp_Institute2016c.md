
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Action Oriented Planning Tools"
date: 2016-01-01
summary: "A Guide for Practitioners. This report defines practices of Action-Oriented Planning. It is intended to be a how-to for urban change managers interested in engaging communities in new ways, to reach better outcomes in their cities. It outlines how Action-Oriented Planning layers on to the traditiona ..."
featureImage: "https://image.isu.pub/160322181151-af68eb2152f1dd7a36631072d3efdb1f/jpg/page_1.jpg"
thumbnail: "https://image.isu.pub/160322181151-af68eb2152f1dd7a36631072d3efdb1f/jpg/page_1.jpg"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['governance', 'participation', 'public space']  
showDate: false
showReadTime: false
---

**Gehl Institute**, 2016.
[DOWNLOAD HERE](https://issuu.com/gehlarchitects/docs/action_oriented_planning_february_0)

> **Abstract** 
>
> A Guide for Practitioners. This report defines practices of Action-Oriented Planning. It is intended to be a how-to for urban change managers interested in engaging communities in new ways, to reach better outcomes in their cities. It outlines how Action-Oriented Planning layers on to the traditional planning process, how to approach a pilot project, and how to establish evaluation and measurement methods. It creates typologies of projects using case studies that highlight the behind-the-scenes work involved, and the scale and timeframe of successful projects.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Collection of Municipal Solid Waste in Developing Countries"
date: 2010-01-01
summary: "This book is written for developing countries. Since it seeks to encourage the designing of waste collection systems based on local information, the approach is valid in any country. 
The main focus is on municipal solid waste, which is taken to include waste from households, businesses and institut ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/02/Collection%20of%20municipal%20waste.JPG.webp?itok=2rvJi7V7"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/02/Collection%20of%20municipal%20waste.JPG.webp?itok=2rvJi7V7"
categories: ['local government', 'planning']
tags: ['services', 'infrastructure']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2010.
[DOWNLOAD HERE](https://unhabitat.org/collection-of-municipal-solid-waste-in-developing-countries-2)

> **Abstract** 
>
> This book is written for developing countries. Since it seeks to encourage the designing of waste collection systems based on local information, the approach is valid in any country. 
The main focus is on municipal solid waste, which is taken to include waste from households, businesses and institutions, construction and demolition waste in small quantities, general solid wastes from hospitals (excluding hazardous wastes), waste from smaller industries that is not classified as hazardous, and wastes from streets, public areas and open drains. It is not concerned with wastes from agriculture, larger industries or the mining industries which normally handle their own wastes.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate Change Vulnerability Assessment Manual"
date: 2018-01-01
summary: "This manual is intended to serve as a guide for national and local government officials on how to assess vulnerability of townships of Myanmar to climate change and hazards. The assessment framework presented herein captures a wide array of issues related to climate change and its impacts on townshi ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/11/10_Climate%20Change%20Vulnerability%20Assessment%20Manual_2018.jpg.webp?itok=5JgO5Q1G"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/11/10_Climate%20Change%20Vulnerability%20Assessment%20Manual_2018.jpg.webp?itok=5JgO5Q1G"
categories: ['local government']
tags: ['governance', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2018.
[DOWNLOAD HERE](https://unhabitat.org/climate-change-vulnerability-assessment-manual)

> **Abstract** 
>
> This manual is intended to serve as a guide for national and local government officials on how to assess vulnerability of townships of Myanmar to climate change and hazards. The assessment framework presented herein captures a wide array of issues related to climate change and its impacts on townships and communities. Those are grouped into environmental, socio-economic and infrastructure components. Furthermore, the analytical approach allows for exploring current and future vulnerability, and carrying out a complex spatial analysis. Importantly, the proposed methodological framework is designed for the Myanmar national context since the suggested process and methods have been used in the case studies of Labutta (Delta Zone) and Pakokku (Central Dry Zone) Townships.


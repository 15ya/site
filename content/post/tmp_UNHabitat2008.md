
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Constructed Wetlands Manual"
date: 2008-01-01
summary: "A general guide to the design, construction, operation and maintenance of constructed wetlands for the treatment of domestic wastewater as well as introduction to the design of constructed wetland for sludge drying. ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/07/Constructed-Wetlands-Manual-1.jpg.webp?itok=INkBNdvp"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/07/Constructed-Wetlands-Manual-1.jpg.webp?itok=INkBNdvp"
categories: ['local government', 'planning', 'landscape']
tags: ['water', 'urban nature', 'sustainability', 'nbs', 'risk reduction', 'resilience', 'climate change', 'land use']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2008.
[DOWNLOAD HERE](https://unhabitat.org/constructed-wetlands-manual)

> **Abstract** 
>
> A general guide to the design, construction, operation and maintenance of constructed wetlands for the treatment of domestic wastewater as well as introduction to the design of constructed wetland for sludge drying.


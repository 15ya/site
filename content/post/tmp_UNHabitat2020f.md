
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "City Prosperity Index (CPI)"
date: 2020-01-01
summary: "UN-Habitat’s City Prosperity Index (CPI) is designed to enable city authorities, as well as local and national stakeholders, to identify opportunities and potential areas of intervention for their cities to become more prosperous. The composite index, made of six dimensions serves to define targets  ..."
featureImage: "https://guo-un-habitat.maps.arcgis.com/sharing/rest/content/items/12ec9cf7c11241fb9d9f233213e6e0f7/data"
thumbnail: "https://guo-un-habitat.maps.arcgis.com/sharing/rest/content/items/12ec9cf7c11241fb9d9f233213e6e0f7/data"
categories: ['local government']
tags: ['governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://data.unhabitat.org/pages/city-prosperity-index)

> **Abstract** 
>
> UN-Habitat’s City Prosperity Index (CPI) is designed to enable city authorities, as well as local and national stakeholders, to identify opportunities and potential areas of intervention for their cities to become more prosperous. The composite index, made of six dimensions serves to define targets and goals that can support the formulation of evidence-based policies, including the definition of city-visions and long-term plans that are both ambitious and measurable. 
The CPI is both a metric and a policy dialogue tool, which offers cities from developed and developing countries the possibility to create indicators and baseline information (often for the first time), from where consistent tracking of progress can be build upon. It is also a global monitoring mechanism, adaptable to national and local levels that can provide a general framework that allows cities, countries, and the international community to measure progress and identify possible constraints. By 2020, CPI had been applied to evaluate urban performance in 539 cities in 54 countries spread across all world regions.


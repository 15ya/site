
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A handbook for practitioners for localizing the SDGs"
date: 2019-01-01
summary: "2019, Agenda 2030 in my municipality: A handbook for practitioners for localizing the Sustainable Development Goals (SDGs) ..."
featureImage: "https://www.iawd.at/files/File/library/danubis/Agenda_2030_in_my_municipality.jpg"
thumbnail: "https://www.iawd.at/files/File/library/danubis/Agenda_2030_in_my_municipality.jpg"
categories: ['local government', 'planning']
tags: ['sustainability', 'resilience', 'sdg']  
showDate: false
showReadTime: false
---

**NALAS**, 2019.
[DOWNLOAD HERE](https://www.academia.edu/44541411/Agenda_2030_in_my_municipality_A_handbook_for_practitioners_for_localizing_the_Sustainable_Development_Goals_SDGs_)

> **Abstract** 
>
> 2019, Agenda 2030 in my municipality: A handbook for practitioners for localizing the Sustainable Development Goals (SDGs)


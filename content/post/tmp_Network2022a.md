
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Training guide: Advancing women’s land and property rights in the Somali region"
date: 2022-01-01
summary: "The United Nations Human Settlements Programme (UN-Habitat) and the Global Land Tool Network (GLTN) developed this training guide in response to the increasing demand for knowledge and capacity to secure women’s land and property rights in the Somali region. 
The guide includes five training modules ..."
featureImage: "https://www.rug.nl/research/globalisation-studies-groningen/former-gsg-projects/gltn-logow.jpg"
thumbnail: "https://www.rug.nl/research/globalisation-studies-groningen/former-gsg-projects/gltn-logow.jpg"
categories: ['local government', 'planning']
tags: ['gender', 'housing', 'equity', 'informality']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2022.
[DOWNLOAD HERE](https://gltn.net/2022/08/04/training-guide-advancing-womens-land-and-property-rights-in-the-somali-region/)

> **Abstract** 
>
> The United Nations Human Settlements Programme (UN-Habitat) and the Global Land Tool Network (GLTN) developed this training guide in response to the increasing demand for knowledge and capacity to secure women’s land and property rights in the Somali region. 
The guide includes five training modules, each of which covers a different topic, including information on the rights of women to land and property, tools, best practices and case studies, as well as information for facilitators and training organizers, and tools for supporting training events.
The modules can be used as source material for training workshops or discussion sessions. They can also serve as a toolkit for a dedicated three- or four-day training course.
The guide targets a broad group of stakeholders, such as government representatives, community members, women’s groups, elders and religious leaders, and other relevant actors interested in advancing women’s land and property rights for women’s empowerment and for the socio-economic development of communities.


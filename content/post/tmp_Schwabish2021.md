
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Do No Harm Guide"
date: 2021-01-01
summary: "In their guide, the authors of the Urban Institute’s Do No Harm Guide describe the background for using the guide: 
“People who work with data every day seek to make discoveries, elicit insights, and offer solutions. To do so in an equitable way, data analysts should think intentionally about how we ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2022/10/do-no-harm-guide-233x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2022/10/do-no-harm-guide-233x300.png"
categories: ['local government']
tags: ['data', 'governance', 'equity']  
showDate: false
showReadTime: false
---

**Urban Institute**, 2021.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/do-no-harm-guide/)

> **Abstract** 
>
> In their guide, the authors of the Urban Institute’s Do No Harm Guide describe the background for using the guide: 
“People who work with data every day seek to make discoveries, elicit insights, and offer solutions. To do so in an equitable way, data analysts should think intentionally about how we can learn from and speak to audiences that reflect the rich diversity of the people and communities of focus. Failing to recognize the diversity in these groups will further exacerbate—and potentially contribute to—the inequities that have shaped the world. Today, in the era of big data, data visualization, machine learning, and artificial intelligence, we need to be more purposeful about where data are coming from and how research and the communication of that research can affect people, their communities, and the policies that touch their lives…This guide and the associated checklists and toolkits focus on the often hidden or subtle ways that data analysts and communicators fail to incorporate equitable awareness in the data they use and the products they create…In this guide, we explore ways to help data scientists, researchers, and data communicators take a more purposeful DEI approach to their work.”



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Technical Guidebook For Financing Planned City Extension And Planned City Infill"
date: 2016-01-01
summary: "UN-Habitat, the United Nations agency mandated by the General Assembly to promote socially and environmentally sus­tainable towns and cities, has designed dual methodologies for urban planning for growing cities and governments around the world. These approaches are called planned city extension (PC ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/11/Pages-from-FINAL-Technical-Guidebook-for-Financing_Planned-City-Extension-Planned-City-Infill.jpg.webp?itok=TBZQxjrw"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/11/Pages-from-FINAL-Technical-Guidebook-for-Financing_Planned-City-Extension-Planned-City-Infill.jpg.webp?itok=TBZQxjrw"
categories: ['local government', 'planning']
tags: ['finance', 'governance', 'urban development']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://unhabitat.org/technical-guidebook-for-financing-planned-city-extension-and-planned-city-infill)

> **Abstract** 
>
> UN-Habitat, the United Nations agency mandated by the General Assembly to promote socially and environmentally sus­tainable towns and cities, has designed dual methodologies for urban planning for growing cities and governments around the world. These approaches are called planned city extension (PCE) and planned city infill (PCI), and serve as an alternative to unplanned and chaotic ur­ban expansion. 
UN-Habitat’s PCE and PCI methodolo­gies take an integrated approach to the drafting and subsequent implementation of plans for cities and are based on three complementary pillars: urban planning and design, regulatory framework, and urban finance. They recognize that for urban planning to be implemented suc­cessfully, it is necessary to analyse the pre­vailing regulatory framework and to accu­rately assess the plans’ feasibility from the standpoint of both the private and public sectors.


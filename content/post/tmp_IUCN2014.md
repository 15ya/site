
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A guiding toolkit for increasing climate change resilience"
date: 2014-01-01
summary: "A guiding toolkit for increasing climate change resilience ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2014-011.JPG?itok=aFwlr4Wh"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2014-011.JPG?itok=aFwlr4Wh"
categories: ['local government', 'planning']
tags: ['climate change', 'resilience']  
showDate: false
showReadTime: false
---

**IUCN**, 2014.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/44643)

> **Abstract** 
>
> A guiding toolkit for increasing climate change resilience


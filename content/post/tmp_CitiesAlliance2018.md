
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Human Resources Capacity Benchmarking"
date: 2018-01-01
summary: "A Preliminary Toolkit for Planning and Management. 
African cities face a severe capacity crisis that undermines all aspects of municipal functioning. Failure to address this will result in the failure of African cities. The document presents how the Future Cities Africa Progamme has begun work to u ..."
featureImage: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2021-09/Human%20Resources%20Capacity%20Benchmarking%20Toolkit.png.webp?itok=BV1y3S_D"
thumbnail: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2021-09/Human%20Resources%20Capacity%20Benchmarking%20Toolkit.png.webp?itok=BV1y3S_D"
categories: ['local government']
tags: ['participation', 'governance']  
showDate: false
showReadTime: false
---

**Cities Alliance**, 2018.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/brochure/human-resources-capacity-benchmarking)

> **Abstract** 
>
> A Preliminary Toolkit for Planning and Management. 
African cities face a severe capacity crisis that undermines all aspects of municipal functioning. Failure to address this will result in the failure of African cities. The document presents how the Future Cities Africa Progamme has begun work to understand the depth and breadth of this crisis. In this sense, the briefing note shows facts and indicators conducted by Future Cities Africa programme where is analyzed existing staff capacity in critical frontline services and key support functions in 16 cities of varying sizes across four countries.


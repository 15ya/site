
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Spatial Planning for Health"
date: 2017-01-01
summary: "An evidence resource for planning and designing healthier places ..."
featureImage: "https://cis.ihs.com/CIS/document/PHE/319082/image"
thumbnail: "https://cis.ihs.com/CIS/document/PHE/319082/image"
categories: ['planning', 'urban design']
tags: ['health', 'urban development', 'housing', 'food', 'sustainability', 'urban nature', 'mobility']  
showDate: false
showReadTime: false
---

**Public Health England**, 2017.
[DOWNLOAD HERE](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/729727/spatial_planning_for_health.pdf)

> **Abstract** 
>
> An evidence resource for planning and designing healthier places


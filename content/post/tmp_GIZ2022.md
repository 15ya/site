
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Technical Guidance on Comprehensive Risk Assessment and Planning in the Context of Climate Change"
date: 2022-01-01
summary: "Climate change is increasing the magnitude, frequency, duration and severity of climate-related hazards, leading to complex and cascading risks that make people and systems, more vulnerable today, in in years to come.  A comprehensive understanding of risks is thus a priority. 
To help disaster risk ..."
featureImage: "https://www.preventionweb.net/sites/default/files/styles/por/public/2022-04/Image-technical-guidance-comprehensive-risk.jpg?h=404dea43&itok=pCHJXvGt"
thumbnail: "https://www.preventionweb.net/sites/default/files/styles/por/public/2022-04/Image-technical-guidance-comprehensive-risk.jpg?h=404dea43&itok=pCHJXvGt"
categories: ['local government']
tags: ['infrastructure', 'climate change', 'risk reduction']  
showDate: false
showReadTime: false
---

**GIZ**, 2022.
[DOWNLOAD HERE](https://www.preventionweb.net/publication/technical-guidance-comprehensive-risk-assessment-and-planning-context-climate-change)

> **Abstract** 
>
> Climate change is increasing the magnitude, frequency, duration and severity of climate-related hazards, leading to complex and cascading risks that make people and systems, more vulnerable today, in in years to come.  A comprehensive understanding of risks is thus a priority. 
To help disaster risk reduction and climate change adaptation decision-makers, practitioners and stakeholders unpack the complex risk landscape and develop more responsive plans and policies, the United Nations Office for Disaster Risk Reduction (UNDRR) UNDRR, in partnership with Deutsche Gesellschaft für Internationale Zusammenarbeit (GIZ) GmbH, developed Technical Guidance on Comprehensive Risk Assessment and Planning in the Context of Climate Change. The document offers a framework on how to apply comprehensive risk assessment and planning.  It acknowledges that risks in the context of climate change are complex and systemic due to non-linear interactions among system components and the need for improved risk governance. The guidance can be contextualized to national and local needs. 
This document is also a key contribution to the Plan of Action of the Technical Expert Group on Comprehensive Risk Management of the Warsaw International Mechanism for Loss and Damage associated with Climate Change Impacts. 
The guidance is available to download as (a) Full Document and (b) Executive Summary.


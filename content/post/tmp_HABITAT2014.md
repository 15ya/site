
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Quick Guide for Policy Makers on Pro-poor Urban Climate Resilience in Asia and the Pacific"
date: 2014-01-01
summary: "The “Quick Guide for Policy Makers on Pro-Poor Urban Climate Resilience in Asia and the Pacific” focuses on the need to enhance understanding of the region’s key urban stakeholders on climate change, discusses how it affects efforts to realize sustainable urban development, and explores what actions ..."
featureImage: "https://unhabitat.org/quick-guide-for-policy-makers-on-pro-poor-urban-climate-resilience-in-asia-and-the-pacific"
thumbnail: "https://unhabitat.org/quick-guide-for-policy-makers-on-pro-poor-urban-climate-resilience-in-asia-and-the-pacific"
categories: ['local government', 'planning']
tags: ['informality', 'governance', 'resilience', 'sustainability', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2014.
[DOWNLOAD HERE](https://unhabitat.org/quick-guide-for-policy-makers-on-pro-poor-urban-climate-resilience-in-asia-and-the-pacific)

> **Abstract** 
>
> The “Quick Guide for Policy Makers on Pro-Poor Urban Climate Resilience in Asia and the Pacific” focuses on the need to enhance understanding of the region’s key urban stakeholders on climate change, discusses how it affects efforts to realize sustainable urban development, and explores what actions can be taken to synergize continued commitments to poverty reduction alongside urban climate resilience. It is argued that there are significant overlaps between climate change vulnerability and urban poverty, and that climate change resilience and poverty reduction efforts need not be a trade-off.
Through examples which span the region, the Quick Guide illustrates pro-poor approaches to urban climate resilience that are holistic, flexible and participatory and that can be effective tools to foster inclusive and sustainable development - an essential task for policy makers in meeting the key urban challenges in the Asia-Pacific region in the twenty-first century.


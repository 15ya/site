
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate Finance Decision Making Tree"
date: 2017-01-01
summary: "This climate finance decision-making tree guides local and regional governments through a series of questions that help them consider different financing tools. Each financing tool is described, including advantages, disadvantages and case study examples. Here is an overview of the decision-making t ..."
featureImage: "https://static.e-library.iclei.org/covers/finance-tree.png"
thumbnail: "https://static.e-library.iclei.org/covers/finance-tree.png"
categories: ['local government']
tags: ['governance', 'finance', 'sustainability', 'climate change']  
showDate: false
showReadTime: false
---

**ICLEI**, 2017.
[DOWNLOAD HERE](https://iclei.org/e-library/climate-finance-decision-making-tree-10/)

> **Abstract** 
>
> This climate finance decision-making tree guides local and regional governments through a series of questions that help them consider different financing tools. Each financing tool is described, including advantages, disadvantages and case study examples. Here is an overview of the decision-making tree and the financing tools available to local and regional governments. This publication was made possible thanks to the support of the Urban-LEDS project, which is funded bythe European Union and it has been translated in French, Spanish, Portuguese and Chinese, thanks to the support of the Global Environmental Facility (GEF). It is also available through the Global Platform for Sustainable Cities (GPSC).



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Time Machine Manual"
date: 2019-01-01
summary: "Our urban environment could be seen as the illustration of a book: it helps to explain all the stories and experiences of the past, shaped by its different users through time.The Time Machine can be used as a tool for places with an (expected) redevelopment task with a desire for a connection betwee ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2020/04/The-time-machine-logo-1024x1024.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2020/04/The-time-machine-logo-1024x1024.png"
categories: ['landscape', 'urban design']
tags: ['participation', 'public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/time-machine-manual/)

> **Abstract** 
>
> Our urban environment could be seen as the illustration of a book: it helps to explain all the stories and experiences of the past, shaped by its different users through time.The Time Machine can be used as a tool for places with an (expected) redevelopment task with a desire for a connection between the past and the present. The tool enables involved stakeholders like the owner, developer, (former) resident, users or municipality to look for an alternative way of developing a future vision through co-creation and participation. The Time Machine is used as a stepping stone to a shared narrative to find common values for future development of a building, neighborhood or area in order to collect. You don’t have to be an expert to be able to analyze a place and to look for existing storylines, anecdotes and facts by navigating through official or personal archives and find key players who can help give shape to its sense of place.


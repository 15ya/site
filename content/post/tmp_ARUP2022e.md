
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Cities Alive: Designing cities that work for women"
date: 2022-01-01
summary: "What would cities look like if they were designed for women? Can we create cities that are safer, more inclusive, and equitable for women? How could we achieve this much needed change?  
A gender-inclusive and responsive approach to urban planning and design is essential to the future of our cities, ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/c/ca-cities-for-women-cover.jpg?h=482&mw=340&w=340&hash=BD1566BEA575AA205AF1EFB96810FAF1"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/c/ca-cities-for-women-cover.jpg?h=482&mw=340&w=340&hash=BD1566BEA575AA205AF1EFB96810FAF1"
categories: ['planning', 'urban design']
tags: ['gender', 'public space', 'health', 'equity']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/cities-alive-designing-cities-that-work-for-women)

> **Abstract** 
>
> What would cities look like if they were designed for women? Can we create cities that are safer, more inclusive, and equitable for women? How could we achieve this much needed change?  
A gender-inclusive and responsive approach to urban planning and design is essential to the future of our cities, creating places where everyone can live, work and thrive. By designing urban areas that are responsive to the needs of all women - and increasing the participation of women in urban governance, planning, and design - our cities will become safer, healthier, fairer and more enriching spaces for all.  
Our new report Designing cities that work for women, produced in partnership with the United Nations Development Programme (UNDP) and the University of Liverpool, identifies four key areas of focus for improving women’s lives in cities and outlines a new gender responsive approach to planning and design. It includes a wide range of case studies and supporting recommendations for city planners, developers, community groups and city authorities, globally.


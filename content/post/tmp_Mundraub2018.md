
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Fostering Edible Cities & Landscapes with Mundraub"
date: 2018-01-01
summary: "This handbook contains ten years of knowledge from the organization mundraub and was compiled for the European project EdiCitNet (Edible Cities Network). ..."
featureImage: "https://www.edicitnet.com/wp-content/uploads/fostering-edible-cities-and-landscapes-with-mundraub.jpg"
thumbnail: "https://www.edicitnet.com/wp-content/uploads/fostering-edible-cities-and-landscapes-with-mundraub.jpg"
categories: ['local government', 'landscape', 'planning', 'urban design']
tags: ['food', 'urban nature', 'sustainability', 'health']  
showDate: false
showReadTime: false
---

**Mundraub**, 2018.
[DOWNLOAD HERE](https://www.edicitnet.com/tools-and-guidelines/)

> **Abstract** 
>
> This handbook contains ten years of knowledge from the organization mundraub and was compiled for the European project EdiCitNet (Edible Cities Network).


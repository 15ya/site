
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Exploring new approaches for sharing data in the built environment"
date: 2021-01-01
summary: "At Arup, we decided to change how we work with data. We understood in 2018 that the status quo is no longer delivering on key strategies for sustainability, inclusion and digitisation. Maintaining our competitive advantage and our ability to shape a better world required us to rethink. We have to re ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/e/odi-paper-coverv2.jpg?h=481&mw=340&w=340&hash=A93443EC24FE3958402F6475697B7441"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/e/odi-paper-coverv2.jpg?h=481&mw=340&w=340&hash=A93443EC24FE3958402F6475697B7441"
categories: ['local government']
tags: ['data']  
showDate: false
showReadTime: false
---

**ARUP**, 2021.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/exploring-new-approaches-for-sharing-data-in-the-built-environment)

> **Abstract** 
>
> At Arup, we decided to change how we work with data. We understood in 2018 that the status quo is no longer delivering on key strategies for sustainability, inclusion and digitisation. Maintaining our competitive advantage and our ability to shape a better world required us to rethink. We have to reduce the cost and friction between our clients and us, partners and the ever growing market for refined data. This report with the ODI is a first step towards an open innovation process that can lead to comprehensive changes in our industries Data Infrastructure, Data Spectrum and Data Value Chains, so that we can agree why and how to better share data. 
This reports provides critical foundational concepts including three approaches for sharing, it covers the default model: 1. data-sharing agreements, together with newer approaches for 2. decentralised publishing initiatives, and 3. data pooling


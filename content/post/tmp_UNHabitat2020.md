
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate Change Vulnerability and Risk – A Guide for Community Assessments, Action Planning and Implementation"
date: 2020-01-01
summary: "To ensure that projects and related activities are adequately targeted at reducing climate change vulnerabilities in communities, it is necessary to conduct Vulnerability and Risk Assessments (VRAs) to understand which people and which areas are most at risk and why. 
This information can subsequent ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/11/climate%20change%20vulnerability%20and%20risk%20guide.jpg.webp?itok=B9_YtwYT"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/11/climate%20change%20vulnerability%20and%20risk%20guide.jpg.webp?itok=B9_YtwYT"
categories: ['local government', 'planning']
tags: ['climate change', 'resilience', 'participation', 'governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/climate-change-vulnerability-and-risk-a-guide-for-community-assessments-action-planning-and)

> **Abstract** 
>
> To ensure that projects and related activities are adequately targeted at reducing climate change vulnerabilities in communities, it is necessary to conduct Vulnerability and Risk Assessments (VRAs) to understand which people and which areas are most at risk and why. 
This information can subsequently be used to:
-    Inform participatory action planning processes that lead to community-driven and owned adaptation, as well as settlements upgrading processes.
-    Identify lower risk areas in which climate-resilient infrastructure and housing could be built.
-    Develop targeted early warning systems, training programs in environmental management and risk reduction, community capacity building, alternative livelihood strategies, etc.
-    Select, prioritize, and design appropriate resilient infrastructure development options.
To minimize social and environmental risks of projects, the VRAs conducted are also used to collect information about potential risks (e.g., people in vulnerable situations, natural habitats). Specific VRAs targeting people in vulnerable situations are necessary to capture more detailed disaggregated data focused on climate change related issues, needs, and perceptions of specific groups such as women and youth with the aim to develop differentiated approaches to building resilience.
This guide promotes community-driven development in identifying issues, developing solutions with technical assistance partners, and active participation in implementing projects. Involvement in planning is a critical component of both appropriately selecting projects and building cohesion within communities. Engagement in community committees builds motivation for collective action in project implementation. Full and equal participation of women, youth, and other people in vulnerable situations is essential to reach all genders and all ages and to avoid underrepresentation of specific groups.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The City Water Reslience Approach"
date: 2019-01-01
summary: "With cities worldwide expected to grow an estimated 2 billion residents by 2050, there is an urgent need for urban water management that ensures consistent, adequate and high-quality water services for all. However, the scale and complexity of this need presents new challenges to decision-makers in  ..."
featureImage: "https://www.arup.com/-/media/arup/images/perspectives/themes/water/city-water-resilience-approach/citywaterresiliencecover.jpg?h=424&mw=340&w=300&hash=49F6145948F2C01EC1C042ACFE70834B"
thumbnail: "https://www.arup.com/-/media/arup/images/perspectives/themes/water/city-water-resilience-approach/citywaterresiliencecover.jpg?h=424&mw=340&w=300&hash=49F6145948F2C01EC1C042ACFE70834B"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['water', 'risk reduction', 'nbs', 'infrastructure', 'health', 'governance', 'urban nature', 'land use', 'climate change', 'services']  
showDate: false
showReadTime: false
---

**ARUP**, 2019.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/the-city-water-resilience-approach)

> **Abstract** 
>
> With cities worldwide expected to grow an estimated 2 billion residents by 2050, there is an urgent need for urban water management that ensures consistent, adequate and high-quality water services for all. However, the scale and complexity of this need presents new challenges to decision-makers in government, civil society and the private sector. 
The City Water Resilience Approach (CWRA) responds to a demand for innovative approaches and tools that help cities build water resilience at the urban scale. The CWRA was developed to help cities grow their capacity to provide high quality water resources for all residents, to protect them from water-related hazards, and to connect them through water-based transportation networks (“provide, protect, connect”).
The approach is the result of fieldwork and desk research, collaborative partnerships with subject matter experts, and direct engagement with city partners. Based on this research, the CWRA outlines a process for developing urban water resilience, and provides a suite of tools to help cities grow their capacity to survive and thrive in the face of water-related shocks and stresses.
The approach details five steps to guide cities through initial stakeholder engagement and baseline assessment, through action planning, implementation and monitoring of new initiatives that build water resilience.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Municipal Public-Private Partnership Framework"
date: 2020-01-01
summary: "Municipal governments and staff are seeking to implement public-private partnerships (PPPs) to help meet their infrastructure development needs. This framework is modular, comprising a guidance note, 20 topic guidance on key issues, and a collection of project summaries that highlight innovative app ..."
featureImage: "https://openknowledge.worldbank.org/server/api/core/bitstreams/4f3b3434-d10f-5abc-b23a-c0129646ef9b/content"
thumbnail: "https://openknowledge.worldbank.org/server/api/core/bitstreams/4f3b3434-d10f-5abc-b23a-c0129646ef9b/content"
categories: ['local government', 'planning']
tags: ['governance', 'participation', 'services']  
showDate: false
showReadTime: false
---

**World Bank**, 2020.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/33572)

> **Abstract** 
>
> Municipal governments and staff are seeking to implement public-private partnerships (PPPs) to help meet their infrastructure development needs. This framework is modular, comprising a guidance note, 20 topic guidance on key issues, and a collection of project summaries that highlight innovative approaches to municipal PPP around the world.


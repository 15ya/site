
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban-Rural Land Linkages: A Concept and Framework for Action"
date: 2021-01-01
summary: "This publication presents a framework for tackling urban-rural land challenges. It is designed to help a range of stakeholders in developing countries understand how to adopt an inclusive approach to land management and administration initiatives to produce a balance in urban and rural development.  ..."
featureImage: "https://gltn.net/wp-content/uploads/2021/06/Urban-Rural-Land-Linkages-1-853x880.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2021/06/Urban-Rural-Land-Linkages-1-853x880.jpg"
categories: ['local government']
tags: ['land use', 'urban development']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/urban-rural-land-linkages-a-concept-and-framework-for-action)

> **Abstract** 
>
> This publication presents a framework for tackling urban-rural land challenges. It is designed to help a range of stakeholders in developing countries understand how to adopt an inclusive approach to land management and administration initiatives to produce a balance in urban and rural development. It provides structured guidance for addressing land-specific problems within the intersection of urban and rural development. 
The publication presents action-oriented steps and recommendations that should be pursued in urban-rural interdependent development. It also expands current knowledge about urban-rural linkages in the context of land tenure challenges. It is hoped that the publication will inspire additional policy debate on securing land tenure on an urban-rural continuum rather than viewing these areas in isolation.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Diamond Model User Guide"
date: 2022-01-01
summary: "This is a practical guide showing you how to apply the Diamond Model. The Diamond Model is a concept and a tool that helps you understand, analyse and strategise the value system of your urban food initiative. ..."
featureImage: "https://www.edicitnet.com/wp-content/uploads/Diamond-Model-User-Guide-Urban-Food-Initiatives.jpg"
thumbnail: "https://www.edicitnet.com/wp-content/uploads/Diamond-Model-User-Guide-Urban-Food-Initiatives.jpg"
categories: ['planning', 'landscape']
tags: ['food', 'urban nature', 'sustainability', 'finance']  
showDate: false
showReadTime: false
---

**Edible Cities Network**, 2022.
[DOWNLOAD HERE](https://www.edicitnet.com/diamond-model/)

> **Abstract** 
>
> This is a practical guide showing you how to apply the Diamond Model. The Diamond Model is a concept and a tool that helps you understand, analyse and strategise the value system of your urban food initiative.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Rapid Planning Toolkit: A Mayor's Step-by-Step Guide To Delivery Of Planned Urban Extension"
date: 2020-01-01
summary: "The world’s urban population is set to increase dramatically in the next three decades. Most of this rapid urbanisation will occur in towns and cities of less than one million inhabitants, also referred to as ‘secondary cities’. The speed and scale of growth means that managing urban areas, and plan ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['governance', 'land use', 'infrastructure', 'mobility', 'services', 'urban development']  
showDate: false
showReadTime: false
---

**The Prince's Foundation**, 2020.
[DOWNLOAD HERE](http://www.rapidplanningtoolkit.org/toolkit)

> **Abstract** 
>
> The world’s urban population is set to increase dramatically in the next three decades. Most of this rapid urbanisation will occur in towns and cities of less than one million inhabitants, also referred to as ‘secondary cities’. The speed and scale of growth means that managing urban areas, and planning for new sustainable urban extensions, will be one of the most important challenges facing cities and societies in the 21st century. 
The Rapid Planning Toolkit is a simple and practical tool which intends to assist city mayors, city leaders and key departments in creating robust and implementable walkable neighbourhood plans in rapidly growing cities or towns. The Toolkit advocates that built environment professionals collaborate with local and national governments, technical specialists and local communities to create effective city planning.


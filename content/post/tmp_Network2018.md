
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Framework for Costing and Financing Land Administration Services (CoFLAS)"
date: 2018-01-01
summary: "Framework for Costing and Financing Land Administration Services (CoFLAS) ..."
featureImage: "https://gltn.net/wp-content/uploads/2018/09/COFLAs.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2018/09/COFLAs.jpg"
categories: ['local government']
tags: ['land use', 'finance']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2018.
[DOWNLOAD HERE](https://gltn.net/2018/09/26/framework-for-costing-and-financing-land-administration-services-coflas/)

> **Abstract** 
>
> Framework for Costing and Financing Land Administration Services (CoFLAS)



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Integrating Climate Change into City Development Strategies"
date: 2015-01-01
summary: "This guidebook on integrating climate change into city development strategies (CDS) attempts to provide a modest input into the effort of unifying two key thematic areas, Climate Change and City Development Strategies. This attempt of climate proofing city development strategies is an ongoing proces ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/09/IntegratingClimateChangeintoCDS.jpg.webp?itok=fminespX"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/09/IntegratingClimateChangeintoCDS.jpg.webp?itok=fminespX"
categories: ['local government', 'planning']
tags: ['governance', 'sustainability', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://unhabitat.org/integrating-climate-change-into-city-development-strategies)

> **Abstract** 
>
> This guidebook on integrating climate change into city development strategies (CDS) attempts to provide a modest input into the effort of unifying two key thematic areas, Climate Change and City Development Strategies. This attempt of climate proofing city development strategies is an ongoing process and requires additional effort by governments, academia, and city development partners worldwide.
The World Bank, the United Nations Human Settlement Programme (UN-Habitat) and the United Nations Environment Programme (UNEP) collaborated in a Joint Work Programme (JWP) to help cities address challenges related to climate change, aiming to facilitate a coordinated, focused effort targeting cities and climate change.
The JWP captured current knowledge, and supported local and national decision-makers incorporate climate change adaptation and mitigation into their urban planning policies and practices.
A number of deliverables have been produced during this collaboration, including: an online catalogue to facilitate access to knowledge on cities and climate change, various tools for incorporating climate change into urban policies and practices, analytic and assessment guides, handbook for mayors on climate change adaptation and mitigation in cities.y also be useful for professionals in the urban development field in cities where local governments lack specific personnel working on town planning.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "URBACT Toolbox"
date: 2020-01-01
summary: "The URBACT Toolbox has everything you need to design and implement integrated and participatory actions in your city. Browse through the Toolbox to discover guidance, tools, templates, prompts, explainers and much more to find your way when tackling urban challenges. 
The Toolbox is organised into t ..."
featureImage: "https://urbact.eu/sites/default/files/styles/width_400/public/media/toolbox_action-planning-cycle.jpg?itok=xuNYc8sL"
thumbnail: "https://urbact.eu/sites/default/files/styles/width_400/public/media/toolbox_action-planning-cycle.jpg?itok=xuNYc8sL"
categories: ['planning', 'urban design']
tags: ['participation', 'governance']  
showDate: false
showReadTime: false
---

**URBACT**, 2020.
[DOWNLOAD HERE](https://urbact.eu/toolbox-home)

> **Abstract** 
>
> The URBACT Toolbox has everything you need to design and implement integrated and participatory actions in your city. Browse through the Toolbox to discover guidance, tools, templates, prompts, explainers and much more to find your way when tackling urban challenges. 
The Toolbox is organised into the five stages of the action planning cycle – analysis, planning, resourcing, implementing and measuring – and the crosscutting actions of engaging stakeholders and sharing knowledge. Aligned with the URBACT Method, the Toolbox draws from capacity-building activities.


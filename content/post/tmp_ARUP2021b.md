
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Green and thriving neighbourhoods"
date: 2021-01-01
summary: "The neighbourhood scale in a city offers some unique opportunities to accelerate towards net zero. When designing a new neighbourhood or beginning a major district-scale regeneration project, cities can set a clear vision and step up ambition on climate objectives. Taking advantage of the balance be ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/g/c40-arup-gtn-guidbook.jpg?h=191&mw=340&w=340&hash=36E22BD353462F7D3D102A5BAF38C533"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/g/c40-arup-gtn-guidbook.jpg?h=191&mw=340&w=340&hash=36E22BD353462F7D3D102A5BAF38C533"
categories: ['planning', 'urban design']
tags: ['resilience', 'public space', 'participation', 'energy', 'mobility', 'sustainability', 'equity']  
showDate: false
showReadTime: false
---

**ARUP**, 2021.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/green-and-thriving-neighbourhoods)

> **Abstract** 
>
> The neighbourhood scale in a city offers some unique opportunities to accelerate towards net zero. When designing a new neighbourhood or beginning a major district-scale regeneration project, cities can set a clear vision and step up ambition on climate objectives. Taking advantage of the balance between scale and agility, neighbourhood projects can pioneer new policy, trial innovative partnership arrangements, consider creative ways to increase citizen participation and test new technologies or products that can support the overarching vision. 
Neighbourhoods provide an ideal scale and context for equitable and sustainable recovery and an opportunity to develop replicable, net zero approaches that can be deployed city-wide and beyond. This Guidebook from Arup and C40 provides cities, as well as national governments, private sector developers, residents and communities, with the framework and approaches to develop ambitious net zero and people-centred neighbourhoods that showcase today the future we want to see.


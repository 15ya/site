
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "SDG Project Assessment Tool – Volume 2: User Guide"
date: 2020-01-01
summary: "The SDG Project Assessment Tool (referred to as SDG Tool) is developed by UN-Habitat as an offline, digital and user-friendly instrument to guide City Authorities and Delivery Partners in the development of more inclusive, sustainable and effective urban projects. The main purpose of the SDG Tool is ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/SDG%20tool%20Vol2.png.webp?itok=lea2Xv3b"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/SDG%20tool%20Vol2.png.webp?itok=lea2Xv3b"
categories: ['local government', 'planning']
tags: ['sdg']  
showDate: false
showReadTime: false
---

**Global Future Cities Programme**, 2020.
[DOWNLOAD HERE](https://www.globalfuturecities.org/sdg-project-assesment-tool#:~:text=TheSDGProjectAssessmentTool(referredtoasSDGTool,sustainableandeffectiveurbanprojects.)

> **Abstract** 
>
> The SDG Project Assessment Tool (referred to as SDG Tool) is developed by UN-Habitat as an offline, digital and user-friendly instrument to guide City Authorities and Delivery Partners in the development of more inclusive, sustainable and effective urban projects. The main purpose of the SDG Tool is to increase the alignment of selected urban projects to the SDG's and their city's contexts.


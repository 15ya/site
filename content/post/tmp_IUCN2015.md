
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Protected areas as tools for disaster risk reduction : a handbook for practitioners"
date: 2015-01-01
summary: "Globally, disasters due to natural hazards takes an enormous toll in terms of human lives, destruction to crops and livelihoods, and economic losses. Disaster risk reduction (DRR) has therefore become a critical part of sustainable development strategies. Over the past decades, the role of healthy e ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2015-001.JPG?itok=wo-cdrCT"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2015-001.JPG?itok=wo-cdrCT"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['risk reduction', 'urban nature', 'sustainability', 'resilience', 'climate change', 'land use']  
showDate: false
showReadTime: false
---

**IUCN**, 2015.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/45054)

> **Abstract** 
>
> Globally, disasters due to natural hazards takes an enormous toll in terms of human lives, destruction to crops and livelihoods, and economic losses. Disaster risk reduction (DRR) has therefore become a critical part of sustainable development strategies. Over the past decades, the role of healthy ecosystems in providing cheap, reliable protection against natural hazards has been increasingly recognized. However, DRR strategies based on ecosystem services are failing in many places because natural ecosystems are being degraded and destroyed. As a result, protected areas are being increasingly recognized as potential tools for their role in facilitating DRR. The following handbook provides practical guidance on the effective use of protected areas as tools to reduce the likelihood and impacts of disasters, drawing on case studies. It is aimed at DRR specialists and protected area system administrators and managers.


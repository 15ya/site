
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Enhancing Nationally Determined Contributions through urban climate action"
date: 2020-01-01
summary: "In the coming months and years, Member States will continue to undertake domestic processes to review, strengthen and implement their Nationally Determined Contributions (NDCs). Inclusion of urban climate action and sub-national government stakeholders in NDC formulation, priority setting, targets,  ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/06/NDC%20Guide%2016062020%5B2%5D-1.jpg.webp?itok=TItpjveu"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/06/NDC%20Guide%2016062020%5B2%5D-1.jpg.webp?itok=TItpjveu"
categories: ['local government', 'planning']
tags: ['climate change', 'resilience', 'governance', 'sustainability']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/enhancing-nationally-determined-contributions-ndcs-through-urban-climate-action)

> **Abstract** 
>
> In the coming months and years, Member States will continue to undertake domestic processes to review, strengthen and implement their Nationally Determined Contributions (NDCs). Inclusion of urban climate action and sub-national government stakeholders in NDC formulation, priority setting, targets, governance and implementation has the potential to support government efforts to enhance ambition and delivery of NDCs. Similarly, the NDCs can inform urban policies and priority setting. 
This guide for Enhancing Nationally Determined Contributions through urban climate action provides practical opportunities for incorporating urban climate action and human settlement issues into the current NDC revision and enhancement process, drawing on existing knowledge and networks. 
We hope this guidance can support countries to: 
-    Enhance the ambition of their NDCs in the current 2020 and future revision processes, by harnessing the mitigation and adaptation potential of human settlements and urban climate action to deliver a high quality of life while reducing greenhouse gas (GHG) emissions and building resilience.
-    Support a more integrated approach to NDC development and implementation across national and local governments.
-    Implement their NDCs by aligning the activities of urban stakeholders behind a common vision for human settlements.
-    Embed their climate objectives into urban decision-making across all sectors of government 
-    Create the enabling frameworks towards the implementation of high-ambition NDCs at sub-national level and help climate authorities to engage with urban authorities through a common basis of language and understanding.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Inclusive Community Engagement: Executive Guide"
date: 2019-01-01
summary: "Pursuing inclusive climate action in cities is critical, but not always easy or straightforward. Inclusive community engagement underpins the delivery of equitable climate policies, and helps to ensure that impacts are fairly distributed across the population. 
This executive guide on inclusive enga ..."
featureImage: "https://www.thegpsc.org/sites/gpsc/files/playbook.png"
thumbnail: "https://www.thegpsc.org/sites/gpsc/files/playbook.png"
categories: ['planning', 'urban design']
tags: ['participation', 'sustainability', 'governance', 'equity']  
showDate: false
showReadTime: false
---

**C40 Cities Climate Leadership Group**, 2019.
[DOWNLOAD HERE](https://www.c40knowledgehub.org/s/article/Inclusive-Community-Engagement-Executive-Guide?language=en_US)

> **Abstract** 
>
> Pursuing inclusive climate action in cities is critical, but not always easy or straightforward. Inclusive community engagement underpins the delivery of equitable climate policies, and helps to ensure that impacts are fairly distributed across the population. 
This executive guide on inclusive engagement explains the importance of inclusive climate action in cities, summarises the critical steps for delivering community engagement, and highlights supporting tools and case studies cities can use. These steps, tools and case studies are set out in full in a separate, linked resource, The Inclusive Community Engagement Playbook.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The How to Guide for Integrating Impact Evaluation into Programming"
date: 2021-01-01
summary: "The objective of the “HOW TO” GUIDE FOR INTEGRATING IMPACT EVALUATION INTO PROGRAMMING is to provide a clear step-by-step guide to establishing evaluations for implementers of land tenure and governance interventions. The “How to” Guide elaborates key steps, processes and tools needed by evaluators  ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/The-How-To-cover.jpg.webp?itok=XzbHLk4F"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/The-How-To-cover.jpg.webp?itok=XzbHLk4F"
categories: ['planning']
tags: ['urban development', 'governance', 'land use']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/the-how-to-guide-for-integrating-impact-evaluation-into-programming)

> **Abstract** 
>
> The objective of the “HOW TO” GUIDE FOR INTEGRATING IMPACT EVALUATION INTO PROGRAMMING is to provide a clear step-by-step guide to establishing evaluations for implementers of land tenure and governance interventions. The “How to” Guide elaborates key steps, processes and tools needed by evaluators and practitioners for integrating impact evaluation into programming and to support better planning and implementation of land tenure and governance interventions. 
This publication serve as an additional tool that complements the Guidelines for Impact Evaluation of Land Tenure and Governance Interventions published in 2019. Users of this “How to” Guide should also read and understand the original Guidelines for Impact Evaluation of Land Tenure and Governance Interventions. Both publications ( the Guidelines for Impact Evaluation of Land Tenure and Governance Interventions; and the How to” Guide for Integrating Impact Evaluation into Programming) are a product of a joint partnership between IFAD, GLTN and UN-Habitat, and in consultation with the Global Donor Working Group on Land (GDWGL), to improve access to tools needed to evaluate land tenure and governance interventions.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "City Resilience Index"
date: 2017-01-01
summary: "City resilience reflects the overall ‘capacity of a city (individuals, communities, institutions, businesses and systems) to survive, adapt and thrive no matter what kinds of chronic stresses or acute shocks they experience’. (Rockefeller Foundation: 2013). 
The City Resilience Index, developed by A ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/c/city_resilience_index.jpg?h=481&mw=340&w=340&hash=687283558774191FBC00273CA9575EEE"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/c/city_resilience_index.jpg?h=481&mw=340&w=340&hash=687283558774191FBC00273CA9575EEE"
categories: ['local government', 'planning']
tags: ['resilience', 'risk reduction', 'governance', 'sustainability', 'participation', 'mobility', 'land use', 'climate change']  
showDate: false
showReadTime: false
---

**ARUP**, 2017.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/city-resilience-index)

> **Abstract** 
>
> City resilience reflects the overall ‘capacity of a city (individuals, communities, institutions, businesses and systems) to survive, adapt and thrive no matter what kinds of chronic stresses or acute shocks they experience’. (Rockefeller Foundation: 2013). 
The City Resilience Index, developed by Arup with support from the Rockefeller Foundation, provides a comprehensive, technically robust, globally applicable basis for measuring city resilience. 
It is comprised of 52 indicators, which are assessed based on responses to 156 questions; through a combination of qualitative and quantitative data. The responses are aggregated and presented in relation to the 12 goals (or indices) in the Framework.


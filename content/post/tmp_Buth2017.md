
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for Climate Impact and Vulnerability Assessments"
date: 2017-01-01
summary: "These guidelines of the Interministerial Working Group on Adaptation to Climate Change of the German Federal Government (IMA Adaptation) provide methodological recommendations for the execution of climate impact and vulnerability assessments at regional and national level. They are intended to suppo ..."
featureImage: "https://www.umweltbundesamt.de/sites/default/files/styles/600w850h/public/medien/376/bilder/guidelines_for_climate_impact_and_vulnerability_assessments_cover.png?itok=cGkdn4xB"
thumbnail: "https://www.umweltbundesamt.de/sites/default/files/styles/600w850h/public/medien/376/bilder/guidelines_for_climate_impact_and_vulnerability_assessments_cover.png?itok=cGkdn4xB"
categories: ['local government']
tags: ['risk reduction', 'climate change']  
showDate: false
showReadTime: false
---

**Umweltbundesamt**, 2017.
[DOWNLOAD HERE](https://www.umweltbundesamt.de/en/publikationen/guidelines-for-climate-impact-vulnerability)

> **Abstract** 
>
> These guidelines of the Interministerial Working Group on Adaptation to Climate Change of the German Federal Government (IMA Adaptation) provide methodological recommendations for the execution of climate impact and vulnerability assessments at regional and national level. They are intended to support the methodical preparatory work. The guidelines are aimed at federal and state (Land) authorities. It is also aimed at federal and state (Land) funding agencies, research institutes and advisory bodies as well as other interested parties both in Germany and abroad. They are based on the Vulnerability Network’s experience with the first vulnerability assessment in Germany and the experience of the states (Länder).


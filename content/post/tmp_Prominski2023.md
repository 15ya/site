
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "River. Space. Design, 3rd edition"
date: 2023-01-01
summary: "Urban riverbanks are attractive locations and highly prized recreational environments. However, they must meet the requirements of flood control, open space design and ecology at the same time, often a challenging task for the designer in very confined spaces. The book, the result of a study lasting ..."
featureImage: "https://image.isu.pub/230310145934-d0a3e374f7fbbee59de3bb4047f1c9ef/jpg/page_1_thumb_large.jpg"
thumbnail: "https://image.isu.pub/230310145934-d0a3e374f7fbbee59de3bb4047f1c9ef/jpg/page_1_thumb_large.jpg"
categories: ['planning', 'landscape', 'urban design']
tags: ['water', 'urban nature', 'sustainability', 'nbs', 'risk reduction', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**Martin Prominski, Antje Stokman, Daniel Stimberg, Hinnerk Voermanek, Susanne Zeller, Katarina Bajc, Nengshi Zheng**, 2023.
[DOWNLOAD HERE](https://issuu.com/birkhauser.ch/docs/riverspacedesign_3a_leseprobe)

> **Abstract** 
>
> Urban riverbanks are attractive locations and highly prized recreational environments. However, they must meet the requirements of flood control, open space design and ecology at the same time, often a challenging task for the designer in very confined spaces. The book, the result of a study lasting several years, subjects more than 60 exemplary projects to a comparative analysis. The result is a systematic catalogue of strategies and innovative design tools. The designer and planner thus obtains an overview of the range of design possibilities. 
Eight new case studies from China, Italy, New Zealand, the Netherlands, Germany and Switzerland were selected and added for the enlarged edition of this reference work on riverbank design.


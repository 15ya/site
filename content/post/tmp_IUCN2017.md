
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Gender-responsive restoration guidelines"
date: 2017-01-01
summary: "The Restoration Opportunities Assessment Methodology (ROAM) was developed by IUCN and the World Resources Institute (WRI) to assist countries in identifying opportunities for forest landscape restoration (FLR), analysing priority areas at a national or sub-national level, and designing and implement ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2017-009.JPG?itok=FAEOW4_6"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2017-009.JPG?itok=FAEOW4_6"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['gender', 'urban nature', 'sustainability', 'resilience', 'land use']  
showDate: false
showReadTime: false
---

**IUCN**, 2017.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/46693)

> **Abstract** 
>
> The Restoration Opportunities Assessment Methodology (ROAM) was developed by IUCN and the World Resources Institute (WRI) to assist countries in identifying opportunities for forest landscape restoration (FLR), analysing priority areas at a national or sub-national level, and designing and implementing FLR interventions. As part of IUCN’s effort to update the methodology, these guidelines have been developed to ensure the application of ROAM and the ensuing FLR implementation, including any policy uptake and land-use planning, is gender responsive.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Integrating health in urban and territorial planning: A sourcebook for urban leaders, health and planning professionals"
date: 2020-01-01
summary: "This sourcebook aims to detail why health needs to be part of UTP and how to make this happen. It brings together two vital elements we need to build habitable cities on a habitable planet: Processes to guide the development of human settlements – in this document referred to as “urban and territori ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/FINAL%2020002_Integrating%20Health%20in%20Urban%20and%20Territorial%20Planning_A%20sourcebook_Page_001.jpg.webp?itok=HhaUXdSi"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/FINAL%2020002_Integrating%20Health%20in%20Urban%20and%20Territorial%20Planning_A%20sourcebook_Page_001.jpg.webp?itok=HhaUXdSi"
categories: ['local government', 'planning']
tags: ['health', 'equity']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/integrating-health-in-urban-and-territorial-planning-a-sourcebook-for-urban-leaders-health-and)

> **Abstract** 
>
> This sourcebook aims to detail why health needs to be part of UTP and how to make this happen. It brings together two vital elements we need to build habitable cities on a habitable planet: Processes to guide the development of human settlements – in this document referred to as “urban and territorial planning (UTP)”; and Concern for human health, well-being and health equity at all levels – from local to global, and from human to planetary health. 
This sourcebook identifies a comprehensive selection of existing resources and tools to support the incorporation of health into UTP, including advocacy frameworks, entry points and guidance, as well as tools and illustrative case studies. It does not provide prescriptions for specific scenarios – these should be determined by context, people and available resources.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Proximityof Care Design Guide"
date: 2021-01-01
summary: "Proximity of Care Design Guide supports the design and implementation of child centred interventions in vulnerable urban environments, with benefits for the entire community. Developed by Arup and the Bernard van Leer Foundation. Make informed design choices for effective interventions. 
Design for  ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'architecture', 'planning', 'urban design']
tags: ['age', 'participation', 'streetscape', 'public space', 'urban nature', 'health']  
showDate: false
showReadTime: false
---

**ARUP**, 2021.
[DOWNLOAD HERE](https://www.proximityofcare.com/design)

> **Abstract** 
>
> Proximity of Care Design Guide supports the design and implementation of child centred interventions in vulnerable urban environments, with benefits for the entire community. Developed by Arup and the Bernard van Leer Foundation. Make informed design choices for effective interventions. 
Design for early childhood development in vulnerable urban contexts can be complex. Making informed design decisions is critical to an effective and context sensitive intervention that will have lasting positive change for young children, their caregivers and pregnant women, with benefits for the whole community where they live.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Makers on Market"
date: 2016-01-01
summary: "Lessons from San Francisco's Market Street Prototyping Festival Volume 1 ..."
featureImage: "https://image.isu.pub/160127184110-e060fdd5d6defba0f055c1c7042cf578/jpg/page_1.jpg"
thumbnail: "https://image.isu.pub/160127184110-e060fdd5d6defba0f055c1c7042cf578/jpg/page_1.jpg"
categories: ['landscape', 'urban design']
tags: ['participation', 'public space', 'streetscape', 'services', 'infrastructure', 'food']  
showDate: false
showReadTime: false
---

**Gehl Institute**, 2016.
[DOWNLOAD HERE](https://issuu.com/gehlarchitects/docs/mspf_report_final_20151218_final)

> **Abstract** 
>
> Lessons from San Francisco's Market Street Prototyping Festival Volume 1


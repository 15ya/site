
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Public Life Diversity Toolkit 2.0"
date: 2016-01-01
summary: "A prototype for measuring social mixing and economic integration in public space ..."
featureImage: "https://image.isu.pub/160621180258-77721d57536b01e99d6fc8337831024f/jpg/page_1.jpg"
thumbnail: "https://image.isu.pub/160621180258-77721d57536b01e99d6fc8337831024f/jpg/page_1.jpg"
categories: ['architecture', 'landscape', 'urban design']
tags: ['public space', 'gender', 'age', 'participation']  
showDate: false
showReadTime: false
---

**Gehl Institute**, 2016.
[DOWNLOAD HERE](https://issuu.com/gehlarchitects/docs/public_life_diversity_toolkit_v2_fo)

> **Abstract** 
>
> A prototype for measuring social mixing and economic integration in public space


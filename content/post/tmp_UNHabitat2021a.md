
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate Proofing Toolkit: For Basic Urban Infrastructure with a Focus on Water and Sanitation"
date: 2021-01-01
summary: "The Climate Proofing Toolkit is a set of steps, tasks and tools to provide guidance to policymakers, planners, practitioners, engineers and utility managers to ensure that the potential climate change impacts are factored in the design, construction, location and operation of current and future basi ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/Climate_Proofing%20Toolkit.png.webp?itok=UJVNdtU9"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/Climate_Proofing%20Toolkit.png.webp?itok=UJVNdtU9"
categories: ['local government', 'planning']
tags: ['water', 'sustainability', 'governance', 'resilience', 'infrastructure', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/climate-proofing-toolkit-for-basic-urban-infrastructure-with-a-focus-on-water-and-sanitation)

> **Abstract** 
>
> The Climate Proofing Toolkit is a set of steps, tasks and tools to provide guidance to policymakers, planners, practitioners, engineers and utility managers to ensure that the potential climate change impacts are factored in the design, construction, location and operation of current and future basic urban infrastructure, with a focus on water and sanitation. Thus, as a toolkit, it does not propose a one-size-fits-all solution.
The overall goal of the toolkit is to ensure that climate-related risks and impacts are factored in the design, construction, location and operation of current and future basic urban infrastructure. The toolkit outlines current capacity gaps and proposes specific actions for climate-resilient infrastructure (planning, designing, building and operating) that anticipates, prepares for and adapts to changing climate conditions. The toolkit’s development was guided by current thinking on the anticipated climate change impacts on basic urban infrastructure, UN-Habitat’s own learning and experiences gained in climate change-related programmes, and the experiences and practices of other development partners.


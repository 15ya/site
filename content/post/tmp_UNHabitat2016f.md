
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Metropolitan Governance: A Framework for Capacity Assessment. Guidance Notes and Toolbox"
date: 2016-01-01
summary: "UN-Habitat and GIZ have developed the Metropolitan Capacity Assessment Methodology (MetroCAM) as a joint contribution to implement urban sustainable development agendas and bring them to the metropolitan scale. It complements the Unpacking Metropolitan Governance series. With this modular toolbox, u ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/11/Pages-from-GIZ-UNH-MetroCAM.jpg.webp?itok=KJnhoeOQ"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/11/Pages-from-GIZ-UNH-MetroCAM.jpg.webp?itok=KJnhoeOQ"
categories: ['local government']
tags: ['governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://unhabitat.org/metropolitan-governance-a-framework-for-capacity-assessment-guidance-notes-and-toolbox)

> **Abstract** 
>
> UN-Habitat and GIZ have developed the Metropolitan Capacity Assessment Methodology (MetroCAM) as a joint contribution to implement urban sustainable development agendas and bring them to the metropolitan scale. It complements the Unpacking Metropolitan Governance series. With this modular toolbox, urban stakeholders are able to assess existing capacity in a metropolitan setting, future needs, and potential trigger points and finally identify options for change. By tackling a particular metropolitan challenge, the MetroCAM provides instruments and incentives for municipalities to cooperate beyond administrative boundaries. 
The MetroCAM is also a process to foster dialogue and build consensus. The first part of the methodology illustrates the required initial steps for a successful assessment, covering aspects like the composition of the assessment team, finding consensus on the most pressing metropolitan challenge and sequencing activities based on resource and time constraints. The heart of the methodology comprises of 12 tools – a bouquet of options which can be selected regarding the specific needs and challenges. The methodology is primarily directed to local and regional governments’ representatives willing to drive the change and engages urban practitioners, metropolitan actors, researchers and development organisations.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The New Urban Agenda Monitoring Framework"
date: 2022-01-01
summary: "The New Urban Agenda Monitoring Framework is essential for tracking progress, assessing impact and assessing whether the New Urban Agenda’s implementation is on track and well executed. It also allows the residents of a city or country to hold local and central governments accountable for implementa ..."
featureImage: "https://habitat3.org/wp-content/themes/habitat3/img/nua-en-si.jpg"
thumbnail: "https://habitat3.org/wp-content/themes/habitat3/img/nua-en-si.jpg"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['public space', 'infrastructure', 'health', 'informality', 'land use', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2022.
[DOWNLOAD HERE](https://www.urbanagendaplatform.org/data_analytics)

> **Abstract** 
>
> The New Urban Agenda Monitoring Framework is essential for tracking progress, assessing impact and assessing whether the New Urban Agenda’s implementation is on track and well executed. It also allows the residents of a city or country to hold local and central governments accountable for implementation of the New Urban Agenda (NUA § 161).


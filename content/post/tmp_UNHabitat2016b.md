
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Fit-For-Purpose Land Administration : Guiding Principles for Country Implementation"
date: 2016-01-01
summary: "Many developed countries have strong land institutions and laws that protect the citizens’ relationship with land and provide land administration services to secure and often guarantee land rights. These services directly support land markets that underpin modern economies. Security of tenure is tak ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2018/05/Fit-For-Purpose-Land-Administration.jpg.webp?itok=rFPzJdf4"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2018/05/Fit-For-Purpose-Land-Administration.jpg.webp?itok=rFPzJdf4"
categories: ['local government']
tags: ['land use', 'governance', 'services']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://unhabitat.org/fit-for-purpose-land-administration-guiding-principles-for-country-implementation)

> **Abstract** 
>
> Many developed countries have strong land institutions and laws that protect the citizens’ relationship with land and provide land administration services to secure and often guarantee land rights. These services directly support land markets that underpin modern economies. Security of tenure is taken for granted.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "City-Wide Public Space Strategies: A Guidebook for City Leaders"
date: 2020-01-01
summary: "Public space is more than well designed physical places. It is an arena for social interaction and active citizenship that can spark social and economic development and drive environmental sustainability. The design, provision and maintenance of well connected systems of public space are integral to ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/03/City-Wide%20Public%20Space%20Strategies%20A%20Guidebook%20for%20City%20Leaders%20-%20cover.png.webp?itok=6zB8bEIm"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/03/City-Wide%20Public%20Space%20Strategies%20A%20Guidebook%20for%20City%20Leaders%20-%20cover.png.webp?itok=6zB8bEIm"
categories: ['local government', 'planning']
tags: ['public space']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/city-wide-public-space-strategies-a-guidebook-for-city-leaders)

> **Abstract** 
>
> Public space is more than well designed physical places. It is an arena for social interaction and active citizenship that can spark social and economic development and drive environmental sustainability. The design, provision and maintenance of well connected systems of public space are integral to achieving a safe and accessible city. However, cities must move beyond typically site-specific approaches to addressing public space if sustainable and longer lasting benefits are to be achieved. Establishing and implementing a city-wide strategy that approaches a city as a multi-functional and connected urban system can ensure the best chances of proactively driving good urban development.
A thorough strategy offers cities an action-oriented approach encompassing not only spatial goals, but governance arrangements, implementation plans, budgetary needs and measurable indicators. It should be formulated to overcome common obstacles to the successful provision of public spaces throughout a city. With adequate political support and funding, a city-wide public space strategy can deliver a well-distributed, accessible and inclusive public space system.
City-Wide Public Space Strategies: a Compendium of Inspiring Practices offers summaries and assessments of 26 such strategies from different cities in all regions of the world. It also proposes a new set of typologies of strategies and a framework with which strategies can be evaluated. This compendium is complemented by City-Wide Public Space Strategies: a Guidebook for City Leaders and together they provide city leaders, including mayors, local authorities, urban planners and designers, with the knowledge and tools necessary to support them in developing and implementing city-wide public space strategies. Building on the Global Public Space Toolkit published by UN-Habitat in 2016, this set of publications supports the strengthening of local government capacity, providing actionable policy guidance and driving transformative change in multiple global contexts.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tools for Equitable Climate Resilience: Fostering Community-Led Research and Knowledge"
date: 2021-01-01
summary: "River Network has developed new Tools for Equitable Climate Resilience with two methods for addressing climate risks: (1) Leadership Development, and (2) Community Based Participatory Research. In this toolkit – Fostering Community-Led Research and Knowledge – three methods are described and case st ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/02/RN-Community-led-research-toolkit-231x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/02/RN-Community-led-research-toolkit-231x300.jpg"
categories: ['local government', 'planning', 'urban design']
tags: ['water', 'equity', 'climate change', 'governance', 'nbs', 'resilience']  
showDate: false
showReadTime: false
---

**River Network**, 2021.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/tools-for-equitable-climate-resilience-fostering-community-led-research-and-knowledge-feb-2021/)

> **Abstract** 
>
> River Network has developed new Tools for Equitable Climate Resilience with two methods for addressing climate risks: (1) Leadership Development, and (2) Community Based Participatory Research. In this toolkit – Fostering Community-Led Research and Knowledge – three methods are described and case studies from organizations implementing such programs are provided. The toolkit also includes a Project and Planning Facilitation Guide and a list of additional resources.


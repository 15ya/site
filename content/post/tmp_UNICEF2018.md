
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Child Friendly Cities Initiative"
date: 2018-01-01
summary: "A child-friendly city is one which implements the UN Convention on the Rights of the Child at the local level. As such, the guiding principles of building a child-friendly city mirror the overarching principles of the Convention. ..."
featureImage: "https://s25924.pcdn.co/wp-content/uploads/2018/03/cfc-logo-NEW.png"
thumbnail: "https://s25924.pcdn.co/wp-content/uploads/2018/03/cfc-logo-NEW.png"
categories: ['planning', 'urban design']
tags: ['age', 'public space', 'streetscape', 'participation', 'governance', 'health']  
showDate: false
showReadTime: false
---

**UNICEF**, 2018.
[DOWNLOAD HERE](https://childfriendlycities.org/guiding-principles/)

> **Abstract** 
>
> A child-friendly city is one which implements the UN Convention on the Rights of the Child at the local level. As such, the guiding principles of building a child-friendly city mirror the overarching principles of the Convention.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Florum Manual: How to use mobile furniture for public space as a catalyst to enable community-led activities?"
date: 2020-01-01
summary: "In the central areas of Floridsdorf in Vienna, the local public library “Weisselbad” has emerged as one of the most active local actors which functions as a multiplier to the local community. As the library shows interest in interacting more with the neighbourhood and being more visible in public sp ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'architecture', 'urban design']
tags: ['participation', 'infrastructure', 'streetscape', 'public space']  
showDate: false
showReadTime: false
---

**superwien**, 2020.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/florum-manual/)

> **Abstract** 
>
> In the central areas of Floridsdorf in Vienna, the local public library “Weisselbad” has emerged as one of the most active local actors which functions as a multiplier to the local community. As the library shows interest in interacting more with the neighbourhood and being more visible in public space, the idea of constructing a mobile furniture that can be used for small events, such as readings or workshops in the public park next to the library came up. The idea evolved to a mobile and flexible furniture for the entire neighbourhood. Being the owner of the furniture, the library functions not only as a user of the furniture, but also as a platform to lend it to neighbours and help them organise their events. Thus, the library takes an active role in revitalising the neighbourhood and acts as a platform for a lively neighbourhood.


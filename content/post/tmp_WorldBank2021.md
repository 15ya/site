
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A Catalogue of Nature-based Solutions for Urban Resilience"
date: 2021-01-01
summary: "Cities worldwide are facing resilience challenges as climate risks interact with urbanization, loss of biodiversity and ecosystem services, poverty, and rising socioeconomic inequality. Extreme precipitation events, flooding, heatwaves, and droughts are causing economic losses, social insecurity, an ..."
featureImage: "https://openknowledge.worldbank.org/server/api/core/bitstreams/8b3982d6-b27f-5502-a103-5dc1f87e94ad/content"
thumbnail: "https://openknowledge.worldbank.org/server/api/core/bitstreams/8b3982d6-b27f-5502-a103-5dc1f87e94ad/content"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['water', 'nbs', 'urban nature', 'infrastructure', 'public space', 'resilience', 'risk reduction', 'sustainability']  
showDate: false
showReadTime: false
---

**World Bank**, 2021.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/36507)

> **Abstract** 
>
> Cities worldwide are facing resilience challenges as climate risks interact with urbanization, loss of biodiversity and ecosystem services, poverty, and rising socioeconomic inequality. Extreme precipitation events, flooding, heatwaves, and droughts are causing economic losses, social insecurity, and affecting wellbeing. Over time, urban resilience challenges are expected to grow, driven by processes such as urbanization, land use, and climate change. Whereas climate change is expected to increase the frequency and intensity of some natural hazards, urbanization can also lead to higher exposure of people and assets in cities. More than half of the global population lives in cities, and more than seventy percent are expected to do so by 2050. Nature-based solutions are approaches that use nature and natural processes for delivering infrastructure, services, and integrative solutions to meet the rising challenge of urban resilience. The catalogue of Nature-based solutions for urban resilience has been developed as a guidance document to support the growing demand for NBS by enabling an initial identification of potential investments in nature-based solutions. The document is structured as follows: Chapter 2 describes generic principles for integrating NBS into urban environments. Chapter 3 provides a reader’s guide and holds the Catalogue of the fourteen NBS families.


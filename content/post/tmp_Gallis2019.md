
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Plant and seed swap tool"
date: 2019-01-01
summary: "The free manual will walk you through all steps of how to organize a plant or seed swap and provide you with information on how you can engage your community in the best way. You will learn about how this is a great way to explore sharing economy and how saving seeds is a political act. We also have ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'urban design']
tags: ['participation', 'public space', 'urban nature', 'sustainability']  
showDate: false
showReadTime: false
---

**Nabolagshager**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/plant-and-seed-swap/)

> **Abstract** 
>
> The free manual will walk you through all steps of how to organize a plant or seed swap and provide you with information on how you can engage your community in the best way. You will learn about how this is a great way to explore sharing economy and how saving seeds is a political act. We also have a section about troubleshooting and some pro tips from our experience so your swap will be a joyful and smooth experience. We share some tips for seasonal swaps and community engagement ranging from collaborations to communication. We hope you get inspired and we would love to hear all about your plant or seed swap event!


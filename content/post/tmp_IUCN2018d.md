
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Biodiversity guidelines for forest landscape restoration opportunities assessments"
date: 2018-01-01
summary: "Biodiversity is inherent in forest landscape restoration. As global initiatives like the Bonn Challenge and New York Declaration on Forests inspire nations to pursue sustainable landscapes and economic growth, on the ground, biodiversity binds people and nature to their shared future. These guidelin ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2018-022-En.PNG?itok=wkDSpAGq"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2018-022-En.PNG?itok=wkDSpAGq"
categories: ['landscape', 'local government']
tags: ['sustainability', 'urban nature', 'land use']  
showDate: false
showReadTime: false
---

**IUCN**, 2018.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/47713)

> **Abstract** 
>
> Biodiversity is inherent in forest landscape restoration. As global initiatives like the Bonn Challenge and New York Declaration on Forests inspire nations to pursue sustainable landscapes and economic growth, on the ground, biodiversity binds people and nature to their shared future. These guidelines are intended to provide more context, more resources and fresh perspectives to the ongoing global interaction between biodiversity conservation and forest landscape restoration. They do so in the context of the methodology used by dozens of countries and jurisdictions to help practitioners working on identifying and realising their landscape restoration goals — and they should be interpreted as a companion to the Restoration Opportunities Assessment Methodology (ROAM).


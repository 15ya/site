
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Sustainable Development Goals Acceleration Toolkit"
date: 2017-01-01
summary: "The Sustainable Development Goals Acceleration Toolkit is an online compendium of system-level diagnostics, models, methodologies and guidance for analyzing interconnections among the SDGs, assessing how to contribute to the pledge by Member States to ‘leave no one behind’. ..."
featureImage: "https://sdghelpdesk.unescap.org/sites/default/files/2018-03/acc-tool.jpg"
thumbnail: "https://sdghelpdesk.unescap.org/sites/default/files/2018-03/acc-tool.jpg"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['sdg']  
showDate: false
showReadTime: false
---

**UN Development Programme**, 2017.
[DOWNLOAD HERE](https://sdgintegration.undp.org/sdg-acceleration-toolkit?utm_source=EN&amp;utm_medium=GSR&amp;utm_content=US_UNDP_PaidSearch_Brand_English&amp;utm_campaign=CENTRAL&amp;c_src=CENTRAL&amp;c_src2=GSR&amp;gclid=Cj0KCQjw-JyUBhCuARIsANUqQ_Jd8ebDxd5_QENkb9bVt0I_TLuxWB1dE4LvqbYK0l8C3_5oOxTozlkaAkSkEALw_wcB)

> **Abstract** 
>
> The Sustainable Development Goals Acceleration Toolkit is an online compendium of system-level diagnostics, models, methodologies and guidance for analyzing interconnections among the SDGs, assessing how to contribute to the pledge by Member States to ‘leave no one behind’.


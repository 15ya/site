
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate-Resilient Urban Expansion Planning"
date: 2022-01-01
summary: "Municipalities in secondary cities in rapidly urbanising countries face grave climate risks. With limited resources, they will be forced to confront sea level rise, storm surge, extreme precipitation, drought, landslide risk, and other geophysical shocks. Social and economic dislocations will change ..."
featureImage: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2022-06/citiesalliance_climate-resilienturbanexpansionplanning_2022_cover.jpg.webp?itok=KZGghBFe"
thumbnail: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2022-06/citiesalliance_climate-resilienturbanexpansionplanning_2022_cover.jpg.webp?itok=KZGghBFe"
categories: ['local government', 'planning', 'urban design']
tags: ['governance', 'land use', 'infrastructure', 'housing', 'resilience', 'sustainability', 'climate change', 'urban development']  
showDate: false
showReadTime: false
---

**Cities Alliance**, 2022.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/cities-alliance-knowledge/climate-resilient-urban-expansion-planning)

> **Abstract** 
>
> Municipalities in secondary cities in rapidly urbanising countries face grave climate risks. With limited resources, they will be forced to confront sea level rise, storm surge, extreme precipitation, drought, landslide risk, and other geophysical shocks. Social and economic dislocations will change migration patterns and, in some cases, could lead to large forced displacement. These challenges add to existing obstacles, including inadequate service provision, rapid urban expansion, and the proliferation of informal settlements. 
Rapidly growing cities are primarily expanding into peripheral areas, which are often poorly planned and disorderly. Fragmented growth patterns undermine the formation of metropolitan labour markets and can impede socioeconomic progress, particularly for rural-urban migrants. 
Climate-Resilient Urban Expansion Planning argues that municipal governments can take meaningful action to prepare for climate change using the same methodology that is used to help secondary cities plan for rapid population growth – urban expansion planning. 
Urban expansion planning is a lightweight technique to organise land in the peripheral areas of cities, where growth is likely to occur, by laying out arterial roads and protecting environmentally sensitive open spaces in advance of development. The approach is focused on acquiring land for infrastructure before development occurs and can be modified to incorporate data on likely climate risks on the urban periphery. 
Municipalities can use urban expansion planning to create a framework for adaptation and mitigation investments by reserving land for drainage and resilient infrastructure, encouraging titling and formalisation of informal settlements, reserving land for flood control, protecting water supply areas, and enhancing connectivity and opportunities for the provision of public transit. Planned investments in urban infrastructure can be made more orderly and climate resilient, and land for future necessary adaptation investments can be reserved in advance of settlement. 
In addition, urban expansion planning can help to guide growth away from high-risk areas by offering alternative sites along the arterial road network. This can help promote higher density and more compact development, making future adaptation programmes more cost-effective, helping cities attract needed funds, and supporting mitigation targets in the transport sector.


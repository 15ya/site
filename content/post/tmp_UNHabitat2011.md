
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Innovative Land and Property Taxation"
date: 2011-01-01
summary: "This publication, Innovative Land and Property Taxation, is derived from a 2009 conference in Warsaw, Poland. It presents the ways in which land and property taxation policies, legal frameworks, tools and approaches to sustainable urban development have been experimented with around the World. Its k ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Innovative-Land-and-Property-Taxation.jpg.webp?itok=uPvs_qEe"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Innovative-Land-and-Property-Taxation.jpg.webp?itok=uPvs_qEe"
categories: ['local government']
tags: ['finance', 'governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2011.
[DOWNLOAD HERE](https://unhabitat.org/innovative-land-and-property-taxation)

> **Abstract** 
>
> This publication, Innovative Land and Property Taxation, is derived from a 2009 conference in Warsaw, Poland. It presents the ways in which land and property taxation policies, legal frameworks, tools and approaches to sustainable urban development have been experimented with around the World. Its key finding is the prominent role that land-based financing and local authorities play at the core of urban development. Carrying ten policy lessons, it is a worthwhile reference for policy makers at local and national governments, researchers, land and property tax specialists, urban economists and other urban development specialists.


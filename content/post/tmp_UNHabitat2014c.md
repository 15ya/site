
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Streets as Tools for Urban Transformation in Slums"
date: 2014-01-01
summary: "This publication highlights the global problem of slums and advocates for using streets as tools for urban transformation. A street-led approach to citywide slum upgrading is promoted which advocates for a shift from piecemeal project based to programme scale upgrading. This publication draws from m ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Streets-as-Tools-for-Urban-Transformation-in-Slums.jpg.webp?itok=OC2j94QH"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Streets-as-Tools-for-Urban-Transformation-in-Slums.jpg.webp?itok=OC2j94QH"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['streetscape', 'public space', 'informality', 'land use']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2014.
[DOWNLOAD HERE](https://unhabitat.org/streets-as-tools-for-urban-transformation-in-slums)

> **Abstract** 
>
> This publication highlights the global problem of slums and advocates for using streets as tools for urban transformation. A street-led approach to citywide slum upgrading is promoted which advocates for a shift from piecemeal project based to programme scale upgrading. This publication draws from many slum upgrading experiences worldwide and encourages a relatively easy to implement approach. It views slums as integral parts of a city system which are spatially segregated and disconnected due to an absence of streets and open spaces. Therefore, taking advantage of streets as the natural conduits that connect slums with the city, UN-Habitat suggests a fundamental shift towards the opening of streets as the driving force for citywide slum upgrading.


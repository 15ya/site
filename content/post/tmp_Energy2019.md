
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A Guide to Community-Centered Engagement in the District of Columbia"
date: 2019-01-01
summary: "This guide—informed by a Partners for Places 2017/2018 pilot project by the District Department of Energy and Environment (DOEE), Georgetown Climate Center, and other partners—outlines a community engagement model in which government and the community collaborate as equal partners. In this model, th ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/03/DOEE_community_engagement_guide-232x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/03/DOEE_community_engagement_guide-232x300.jpg"
categories: ['local government', 'planning']
tags: ['equity', 'governance', 'participation']  
showDate: false
showReadTime: false
---

**District Department of Energy and Environment and Georgetown Climate Ceneter**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/a-guide-to-community-centered-engagement-in-the-district-of-columbia-2019/)

> **Abstract** 
>
> This guide—informed by a Partners for Places 2017/2018 pilot project by the District Department of Energy and Environment (DOEE), Georgetown Climate Center, and other partners—outlines a community engagement model in which government and the community collaborate as equal partners. In this model, the community members and government staff create projects that honor the expertise and vision of the community together with the technical expertise and implementation authority of the government. It is an important structure for integrating racial equity into planning processes.


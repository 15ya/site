
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Street Design Guide"
date: 2013-01-01
summary: "Streets comprise more than 80% of public space in cities, but they often fail to provide their surrounding communities with a space where people can safely walk, bicycle, drive, take transit, and socialize. Cities are leading the movement to redesign and reinvest in our streets as cherished public s ..."
featureImage: "https://islandpress.org/sites/default/files/BookCovers/9781610914949.jpg"
thumbnail: "https://islandpress.org/sites/default/files/BookCovers/9781610914949.jpg"
categories: ['urban design', 'landscape']
tags: ['mobility', 'land use', 'infrastructure', 'streetscape', 'services']  
showDate: false
showReadTime: false
---

**NACTO**, 2013.
[DOWNLOAD HERE](https://nacto.org/publication/urban-street-design-guide/)

> **Abstract** 
>
> Streets comprise more than 80% of public space in cities, but they often fail to provide their surrounding communities with a space where people can safely walk, bicycle, drive, take transit, and socialize. Cities are leading the movement to redesign and reinvest in our streets as cherished public spaces for people, as well as critical arteries for traffic. The Urban Street Design Guide charts the principles and practices of the nation’s foremost engineers, planners, and designers working in cities today.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for invasive species planning and management on islands"
date: 2018-01-01
summary: "‘Invasive species’ (often called pests, weeds and diseases) are plants, animals, disease agents and other organisms taken beyond their natural range by people, deliberately or unintentionally, and which become destructive to the environment or human livelihoods. Islands are particularly vulnerable t ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2018-030-En.PNG?itok=uSEa8_mB"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2018-030-En.PNG?itok=uSEa8_mB"
categories: ['landscape', 'planning']
tags: ['urban nature', 'sustainability', 'climate change']  
showDate: false
showReadTime: false
---

**IUCN**, 2018.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/47764)

> **Abstract** 
>
> ‘Invasive species’ (often called pests, weeds and diseases) are plants, animals, disease agents and other organisms taken beyond their natural range by people, deliberately or unintentionally, and which become destructive to the environment or human livelihoods. Islands are particularly vulnerable to invasive species, owing to the evolution of their native animals and plants in isolation from predators and diseases, and the dependence of island peoples on imports, travel and tourism, which lead to high rates of arrival of new pests. These Guidelines are designed to assist anyone planning and programming the management of invasive species on islands, with the aim of reducing the negative impacts of invasives on islands’ rich and fragile natural heritage, communities and livelihoods. The document provides guidance for anyone who has to find, plan and prioritise funds and resources for invasive species management and research, on islands anywhere, including for the design of national invasive species strategies and action plans. It provides support for islanders and island agencies working on invasives, as well as guidance for international and regional agencies in providing assistance to them. A major aim is to help and guide the development of more objective, realistic and achievable invasive species plans and programmes.


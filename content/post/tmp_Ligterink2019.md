
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Place Game Manual"
date: 2019-01-01
summary: "If you want to improve the sense of place in an area you work on, create a higher sense of involvement in the local community using a simple, flexible, and effective tool, we offer a solution – the place game! A place game will not only ignite a robust source of creative ideas, it will also mobilize ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'urban design']
tags: ['participation', 'public space']  
showDate: false
showReadTime: false
---

**Project for Public Spaces**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/place-game-manual/)

> **Abstract** 
>
> If you want to improve the sense of place in an area you work on, create a higher sense of involvement in the local community using a simple, flexible, and effective tool, we offer a solution – the place game! A place game will not only ignite a robust source of creative ideas, it will also mobilize agency, build social capital, and catalyses economic feedback into the community. The beauty of the place game is its power to bring all types of stakeholders together who are all actively seeking to instill positive change into the place. Together, you’re able to identify what is working in a public space, and conversely, identify those aspects that could be improved upon, based on observations of how people are, or are not, using the space.


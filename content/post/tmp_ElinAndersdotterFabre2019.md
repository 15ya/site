
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Girls Catalogue"
date: 2019-01-01
summary: "This catalogue contains new methods and tools for local urban development and planning that promote sustainable urban public spaces, that are applied in Swedish low-income areas, or in informal urban settlements in the global south. This publication shows that a feminist approach to urban livability ..."
featureImage: "https://globalutmaning.se/wp-content/uploads/2022/03/Skarmavbild-2022-03-18-kl.-09.42.55.png"
thumbnail: "https://globalutmaning.se/wp-content/uploads/2022/03/Skarmavbild-2022-03-18-kl.-09.42.55.png"
categories: ['planning', 'urban design']
tags: ['gender', 'public space']  
showDate: false
showReadTime: false
---

**Global Utmaning**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/urban-girls-catalogue/)

> **Abstract** 
>
> This catalogue contains new methods and tools for local urban development and planning that promote sustainable urban public spaces, that are applied in Swedish low-income areas, or in informal urban settlements in the global south. This publication shows that a feminist approach to urban livability is important in understanding the diversity of perspectives on livability in a public space, and that these perspectives matter for how we understand planning principles. In Urban Girls Catalogue they mapped good examples, stories and lessons learned form multi-stakeholders globally, highlighted ongoing initiatives and payed attention to implementing the SDGs at a local level with feminist urban planning techniques as core tools. Their motto is: cities planned for and by girls work for everyone.


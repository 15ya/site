
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Placemaking Pils Manual"
date: 2019-01-01
summary: "We at Nabolagshager started the tradition of Placemaking Pils in Oslo. The tool combines listening to speed presentations of some of the most inspiring placemaking examples from your network while socializing over a beer. It was the answer to the question: “how to share the amazing placemaking proje ..."
featureImage: "https://nabolagshager.files.wordpress.com/2020/11/audience-at-placemaking-pils_credit-nabolagshager.jpg?w=1024"
thumbnail: "https://nabolagshager.files.wordpress.com/2020/11/audience-at-placemaking-pils_credit-nabolagshager.jpg?w=1024"
categories: ['planning']
tags: ['participation']  
showDate: false
showReadTime: false
---

**Placemaking Europe**, 2019.
[DOWNLOAD HERE](https://issuu.com/stipo9/docs/placemaking_pils_manual_draft)

> **Abstract** 
>
> We at Nabolagshager started the tradition of Placemaking Pils in Oslo. The tool combines listening to speed presentations of some of the most inspiring placemaking examples from your network while socializing over a beer. It was the answer to the question: “how to share the amazing placemaking projects with other placemakers to create a supportive group and make it accessible to people from the outside”


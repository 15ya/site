
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidance and toolkit for impact assessments in a World Heritage context"
date: 2022-01-01
summary: "As the World Heritage Convention celebrates its 50th anniversary in 2022, over 1100 sites around the world are recognized as World Heritage - places  that are so valuable to humanity that there conservation has been deemed our collective responsibility. Yet many of these exceptional places face  inc ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2022-031-En.PNG?itok=rh3XGsPC"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2022-031-En.PNG?itok=rh3XGsPC"
categories: ['local government', 'planning', 'architecture', 'urban design']
tags: ['housing', 'urban development', 'land use', 'conservation']  
showDate: false
showReadTime: false
---

**IUCN**, 2022.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/50022)

> **Abstract** 
>
> As the World Heritage Convention celebrates its 50th anniversary in 2022, over 1100 sites around the world are recognized as World Heritage - places  that are so valuable to humanity that there conservation has been deemed our collective responsibility. Yet many of these exceptional places face  increasing pressure from diverse types of development projects within and around the sites. Assessing the impacts of such projects is essential to both prevent damage to World Heritage and identify sustainable options. This Guidance and toolkit explains the process for achieving these goals. Offering practical tips and tools including checklists and a glossary, it provides a framework for conducting impact assessments for cultural and natural heritage sites.


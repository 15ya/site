
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "NBS Best Practices Kit"
date: 2022-01-01
summary: "In this NBS best practices booklet, recommendations, experiences and best practice examples are collected with the aim of maximising the replication of project results and activities, and ensuring the sustainability and long-term impact of the UNaLab project. ..."
featureImage: "https://unalab.eu/sites/default/files/2022-09/D7.12%20%20UNaLab%20NBS%20Best%20Practices%20Kit_2022-09-08_3935.png"
thumbnail: "https://unalab.eu/sites/default/files/2022-09/D7.12%20%20UNaLab%20NBS%20Best%20Practices%20Kit_2022-09-08_3935.png"
categories: ['planning', 'local government']
tags: ['governance', 'urban nature', 'water', 'sustainability', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UNaLab**, 2022.
[DOWNLOAD HERE](https://unalab.eu/en/documents/d712-unalab-nbs-best-practices-kit)

> **Abstract** 
>
> In this NBS best practices booklet, recommendations, experiences and best practice examples are collected with the aim of maximising the replication of project results and activities, and ensuring the sustainability and long-term impact of the UNaLab project.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Women Friendly Urban Planning Toolkit"
date: 2022-01-01
summary: "Intended for urban development professionals and city officials, the Toolkit for Women-Friendly Urban Planning addresses the need to know more about how to engage women in participatory processes, with a focus on cities in the Global South. It has been produced by Cities Alliance in collaboration wi ..."
featureImage: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2022-04/cover%20toolkit.PNG.webp?itok=9bR1s0Bv"
thumbnail: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2022-04/cover%20toolkit.PNG.webp?itok=9bR1s0Bv"
categories: ['local government', 'planning', 'urban design']
tags: ['gender', 'participation', 'health', 'public space', 'mobility']  
showDate: false
showReadTime: false
---

**Cities Alliance**, 2022.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/cities-alliance-knowledge/women-friendly-urban-planning-toolkit)

> **Abstract** 
>
> Intended for urban development professionals and city officials, the Toolkit for Women-Friendly Urban Planning addresses the need to know more about how to engage women in participatory processes, with a focus on cities in the Global South. It has been produced by Cities Alliance in collaboration with Womenability and sponsored by SIDA.
The toolkit provides detailed explanations and specific tools and activities for each phase of the project cycle. By catering to all possible biases and problems specific to gender issues, these tools provide solid elements to best mobilize women in urban projects. They have been used in specific development projects and initiatives, especially in informal settlements and communities.
The toolkit is divided into two main parts. The first part presents the various steps for gender mainstreaming within the participatory process throughout the project cycle. The second part explains how to design gender-fair participatory processes, giving insights on the necessary data and tools to choose, how to maintain them and how to communicate in a gender-sensitive way using examples.


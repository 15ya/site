
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for Impact Evaluation of Land Tenure and Governance Interventions"
date: 2019-01-01
summary: "The overall objective of the Guidelines for Impact Evaluation of Land Tenure and Governance Interventions is to inform and strengthen the design and implementation of future land tenure and governance interventions to best support lasting tenure security and achieve related impacts on poverty, food  ..."
featureImage: "https://gltn.net/wp-content/uploads/2019/03/IFAD-GLII-guidelines.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2019/03/IFAD-GLII-guidelines.jpg"
categories: ['local government']
tags: ['equity', 'land use', 'informality', 'governance']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2019.
[DOWNLOAD HERE](https://gltn.net/2019/03/29/guidelines-for-impact-evaluation-of-land-tenure-and-governance-interventions/)

> **Abstract** 
>
> The overall objective of the Guidelines for Impact Evaluation of Land Tenure and Governance Interventions is to inform and strengthen the design and implementation of future land tenure and governance interventions to best support lasting tenure security and achieve related impacts on poverty, food security, gender equality, environmental sustainability and security. The guidelines aim to serve as a tool for both researchers and land sector experts in designing and conducting land impact evaluations and ultimately to broaden the evidence of what works, what does not work and why with regard to measurement to improve land tenure and governance. 
The guidelines are the result of a partnership between IFAD and GLTN, and in consultation with the Global Donor Working Group on Land (GDWGL), to improve access to tools needed to evaluate land tenure and governance interventions. The guidelines are based on a desk review of land evidence and in-depth consultations with evaluation experts, insights from stakeholders from GLTN and GDWGL, and researchers who have conducted land impact evaluations.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Increasing success and effectiveness of mangrove conservation investments : a guide for project developers, donors and investors"
date: 2018-01-01
summary: "Mangroves are among the most important ecosystems on the planet. They provide nursery grounds for a wide variety of marine and landbased species, sustain livelihoods of coastal populations, and protection from ocean swell and extreme weather events. However, mangroves are being cleared, degraded, or ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2018-057-En.JPG?itok=MSdb3o7P"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2018-057-En.JPG?itok=MSdb3o7P"
categories: ['landscape', 'local government', 'planning', 'urban design']
tags: ['urban nature', 'water', 'nbs', 'risk reduction', 'finance', 'climate change']  
showDate: false
showReadTime: false
---

**IUCN**, 2018.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/48357)

> **Abstract** 
>
> Mangroves are among the most important ecosystems on the planet. They provide nursery grounds for a wide variety of marine and landbased species, sustain livelihoods of coastal populations, and protection from ocean swell and extreme weather events. However, mangroves are being cleared, degraded, or lost at a rapid pace. This report is targeted at project developers, donors and investors. It aims to serve as a guide to improve the long-term environmental, social and economic returns from mangrove conservation investments by highlighting the opportunities as well as risks of such endeavours. The report provides a set of recommendations and lessons learned, derived from a literature search, interviews and case studies from Viet Nam, Kenya and Madagascar.


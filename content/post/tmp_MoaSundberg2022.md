
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tunnel Tool"
date: 2022-01-01
summary: "During the modern period, the strategy of creating safe school routes for children and separating them from cars by building tunnels was a popular solution to eliminate the need for children to cross the street. Tunnel crossings for pedestrians and cyclists act as important linkages between neighbou ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'architecture', 'urban design']
tags: ['art', 'streetscape', 'public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2022.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/tunnel-tool/)

> **Abstract** 
>
> During the modern period, the strategy of creating safe school routes for children and separating them from cars by building tunnels was a popular solution to eliminate the need for children to cross the street. Tunnel crossings for pedestrians and cyclists act as important linkages between neighbourhoods, but many people do not feel safe using the tunnels. Nowadays, many tunnels are in need of renovation and improving the feeling of safety is a great challenge, because these places lack ‘eyes on the street’.


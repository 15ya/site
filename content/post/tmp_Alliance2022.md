
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Equitable Development Scorecard"
date: 2022-01-01
summary: "The Equitable Development Principles & Scorecard is a tool to answer the question: “How can development repair past harms and contribute to a stronger, more inclusive and thriving community?”  
The Alliance partnered with the Harrison Neighborhood Association, Umoja Community Development Corporation ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2022/12/ED-Scorecard-cover-232x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2022/12/ED-Scorecard-cover-232x300.png"
categories: ['local government', 'planning']
tags: ['gender', 'age', 'governance', 'urban development', 'equity']  
showDate: false
showReadTime: false
---

**The Alliance**, 2022.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/equitable-development-scorecard/)

> **Abstract** 
>
> The Equitable Development Principles & Scorecard is a tool to answer the question: “How can development repair past harms and contribute to a stronger, more inclusive and thriving community?”  
The Alliance partnered with the Harrison Neighborhood Association, Umoja Community Development Corporation, and members of the Community Engagement Steering Committee to create the 2016 Equitable Development Principles & Scorecard, which helps communities ensure that the principles and practices of equitable development, environmental justice, and affordability are available to all residents. In 2020-2021, the Alliance worked with community partners to update the Scorecard and add a new Livability Principle to the evolving tool.


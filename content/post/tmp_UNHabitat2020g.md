
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Governance Assessment Framework for Metropolitan Territorial and Regional Management"
date: 2020-01-01
summary: "The Governance Assessment Framework for Metropolitan, Territorial and Regional Management (GAF-MTR) and its two-step assessment tools present an analytical and practical vision of governance. They propose the understanding of governance as a complex process in which institutional solutions, decision ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/10/Governance%20Assessment%20Framework%20for%20Metropolitan%20Territorial%20and%20Regional%20Management%20%28GAF-MTR%29.JPG.webp?itok=nTShI2pF"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/10/Governance%20Assessment%20Framework%20for%20Metropolitan%20Territorial%20and%20Regional%20Management%20%28GAF-MTR%29.JPG.webp?itok=nTShI2pF"
categories: ['local government']
tags: ['governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/governance-assessment-framework-for-metropolitan-territorial-and-regional-management-gaf-mtr)

> **Abstract** 
>
> The Governance Assessment Framework for Metropolitan, Territorial and Regional Management (GAF-MTR) and its two-step assessment tools present an analytical and practical vision of governance. They propose the understanding of governance as a complex process in which institutional solutions, decision-making and collective action must work together. Furthermore, the GAF-MTR defines governance factors that, when properly managed and improved, serve to enable and advance territorial management from supra-municipal scales. 
The GAF-MTR draws from inspiring practices on the establishment of institutional and decision-making arrangements for territorial management at supra-municipal scales. Case studies include Valle de Aburrá in Colombia; San Salvador; Montreal in Canada; London; Barcelona in Spain; Bratislava; Johannesburg in South Africa; and Singapore. These examples show how to provide integrative territorial governance frameworks involving fit-for-purpose institutional solutions, representative decision-making processes and collective actions.


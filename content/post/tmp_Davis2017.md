
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Sustainable Square Mile Handbook"
date: 2017-01-01
summary: "The handbook was created from a collaboration between Blacks in Green (BIG) and the National Resources Defense Council (NRDC), making connections between BIG’s programs—the Sustainable Square Mile, Grannynomics, and Green Village Building—and NRDC’s guidelines for Leadership in Energy and Environmen ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/02/Sustainable-SqMi-191x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/02/Sustainable-SqMi-191x300.jpg"
categories: ['planning', 'urban design']
tags: ['urban development', 'equity', 'resilience', 'sustainability', 'urban nature', 'participation', 'services']  
showDate: false
showReadTime: false
---

**Natural Resource Defense Council**, 2017.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/the-sustainable-square-mile-handbook-feb2020/)

> **Abstract** 
>
> The handbook was created from a collaboration between Blacks in Green (BIG) and the National Resources Defense Council (NRDC), making connections between BIG’s programs—the Sustainable Square Mile, Grannynomics, and Green Village Building—and NRDC’s guidelines for Leadership in Energy and Environmental Design for Neighborhood Development (LEED-NC). It is important to make this pairing, as one of the unintended consequences of traditional LEED green-building practices has led to gentrification and displacement. The handbook provides eight fact sheets with information on how to benefit both the community and the environment while providing more sustainable development in underserved neighborhoods. The fact sheets focus on 8 Green Village Building concepts (1) wealth, (2) energy, (3) products, (4) homestead, (5) culture, (6) Organized, (7) Education, and (8) Oasis/Commerce. The fact sheets contain important definitions and background pertaining to each topic as well as solutions, case studies and connections to LEED-NC metrics.


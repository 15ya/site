
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Radius: How to map the 15 Minute City?"
date: 2022-01-01
summary: "The manual motivates you to use the 15-minute city concept with public space analysis in order to get to know your project, on either the block or city scale, more in depth to critically investigate the existence of mixed-use development in both accessibility and human experience perspective. ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2021/03/Case-Study-CPH_THe-Radius-Tool.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2021/03/Case-Study-CPH_THe-Radius-Tool.png"
categories: ['planning', 'urban design']
tags: ['public space', 'mobility', 'sustainability', 'governance']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2022.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/the-radius/)

> **Abstract** 
>
> The manual motivates you to use the 15-minute city concept with public space analysis in order to get to know your project, on either the block or city scale, more in depth to critically investigate the existence of mixed-use development in both accessibility and human experience perspective.


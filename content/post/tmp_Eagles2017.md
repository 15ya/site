
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for tourism partnerships and concessions for protected areas: generating sustainable revenues for conservation and development"
date: 2017-01-01
summary: "ManyPa rties to the CBD underutilise tourism as a means to contribute towards the financial sustainability of protected areas. The development of the present guidelines on tourism partnerships and concessions for protected areas is a response to this under-utilized potential and to recent decisions  ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-Bios-Cons-Nat-Pro-158.JPG?itok=q2OqkYoh"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-Bios-Cons-Nat-Pro-158.JPG?itok=q2OqkYoh"
categories: ['local government']
tags: ['urban nature', 'sustainability', 'finance', 'conservation']  
showDate: false
showReadTime: false
---

**IUCN WCPA**, 2017.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/46956)

> **Abstract** 
>
> ManyPa rties to the CBD underutilise tourism as a means to contribute towards the financial sustainability of protected areas. The development of the present guidelines on tourism partnerships and concessions for protected areas is a response to this under-utilized potential and to recent decisions of the CBD on tourism.


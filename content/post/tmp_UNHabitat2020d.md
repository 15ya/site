
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "City-wide public space assessment toolkit: A guide to community-led digital inventory and assessment of public spaces"
date: 2020-01-01
summary: "The purpose of this document is to guide cities when conducting a city-wide public space assessment. It illustrates the steps that should be followed within the process to ensure that the public space assessment meets the objectives of the city. By applying this tool, cities are able to understand t ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/02/CWPSA.JPG.webp?itok=z_v50w1S"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/02/CWPSA.JPG.webp?itok=z_v50w1S"
categories: ['planning', 'local government', 'urban design']
tags: ['public space']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/city-wide-public-space-assessment-toolkit-a-guide-to-community-led-digital-inventory-and-assessment)

> **Abstract** 
>
> The purpose of this document is to guide cities when conducting a city-wide public space assessment. It illustrates the steps that should be followed within the process to ensure that the public space assessment meets the objectives of the city. By applying this tool, cities are able to understand the state of their public spaces, specifically the network, distribution, accessibility, quantity and quality of their public spaces. This will support the development of a comprehensive evidence-based public space strategy or policy.
This guide provides a flexible framework designed to aid local governments and partners working in public spaces to assess public spaces and develop a prioritized set of interventions – both spatial and non-spatial– that government and private entities can take to address them. The process has been divided into four parts that are progressive with outputs that are as important as the process and social inclusion and human rights being considered at all stages of the process. To ensure that recommendations are implementable, the regulatory framework, urban planning instruments, financing structure and institutional set-up are considered in the process. The tool is also key to monitor and achieve the public space commitments within the New Urban  Agenda and the Agenda 2030.


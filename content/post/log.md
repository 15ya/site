---
title: "Log" # Title of the blog post.
date: 2025-03-14T20:06:49+01:00 # Date of post creation.
description: "Article description." # Description used for search engine.
summary: "Replacement text goes here"
featured: false # Sets if post is a featured post, making appear on the home page side bar.
draft: true # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
usePageBundles: false # Set to true to group assets like images in the same folder as this post.
featureImage: 'https://images.unsplash.com/photo-1447069387593-a5de0862481e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80' #"images/jane-doe.png" # Sets featured image on blog post.
featureImageAlt: 'Description of image' # Alternative text for featured image.
featureImageCap: 'Caption, this is the featured image.' # Caption (optional).
thumbnail: "images/jane-doe.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
tags:
  - logging
# comment: false # Disable comment if false.
showDate: false
showShare: false
showReadTime: false
---


---

Notes.

- Using tips and doc from https://github.com/chipzoller/hugo-clarity
- The site title is set from `languages.toml`, not `config.toml`.
- We can overwrite the card text with key `summary: "Text in tile"`
---

# Cleaning up "examplesite"
- [x] `draft=true` set in all example post headers.

# Adapting config files
- [x] `archetypes/post.md` add default `showDate: false`, `showShare: false`, `showReadTime: false`, `draft: false`.
      This is used when calling `hugo new content/post/...`.
- [x] `config/_default/config.toml` comment "series" in taxonomies.
- [x] `config/_default/menus/menu.en.toml` comment *everything*. We want a minimal header.
- [x] Remove languages, first with the following line from the doc.
    ```
    sed '/^\[pt]$/,$d' -i config/_default/languages.toml && rm config/_default/menus/menu.pt.toml
    ```
    Then in `config/_default/languages.toml`: comment PT and update site name in EN.
    **WARNING**: leave `[pt]` otherwise the searchbar breaks.
- [ ] `config/_default/config.toml`: change or comment "logo", provide "introDescription" change author "name" to "About

# Modifying Clarity for our needs

- [x] copy `theme/hugo-clarity/layouts/partials/sidebar.html` to `layouts/partials`
  then:
  - remove recent posts
  - change the order between "Author", "Tags", searchbar
- [x] copy `theme/hugo-clarity/layouts/partials/logo.html` to `layouts/partials`
    then specify a precise width for the logo in header.
- [x] change colors using `assets/sass/_override.sass`



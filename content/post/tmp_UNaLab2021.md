
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Evaluating the impact of nature-based solutions: A handbook for practitioners"
date: 2021-01-01
summary: "The handbook aims to provide decision-makers with a comprehensive NBS impact assessment framework, and a robust set of indicators and methodologies to assess impacts of nature-based solutions across 12 societal challenge areas: Climate Resilience; Water Management; Natural and Climate Hazards; Green ..."
featureImage: "https://unalab.eu/sites/default/files/2022-11/Evaluating%20the%20impact%20of%20nature-based%20solutions%3A%20A%20handbook%20for%20practitioners_2022-11-30_4059.png"
thumbnail: "https://unalab.eu/sites/default/files/2022-11/Evaluating%20the%20impact%20of%20nature-based%20solutions%3A%20A%20handbook%20for%20practitioners_2022-11-30_4059.png"
categories: ['local government', 'planning']
tags: ['water', 'urban nature', 'sustainability', 'climate change', 'resilience']  
showDate: false
showReadTime: false
---

**UNaLab**, 2021.
[DOWNLOAD HERE](https://unalab.eu/en/documents/evaluating-impact-nature-based-solutions-handbook-practitioners)

> **Abstract** 
>
> The handbook aims to provide decision-makers with a comprehensive NBS impact assessment framework, and a robust set of indicators and methodologies to assess impacts of nature-based solutions across 12 societal challenge areas: Climate Resilience; Water Management; Natural and Climate Hazards; Green Space Management; Biodiversity; Air Quality; Place Regeneration; Knowledge and Social Capacity Building for Sustainable Urban Transformation; Participatory Planning and Governance; Social Justice and Social Cohesion; Health and Well-being; New Economic Opportunities and Green Jobs. 
Indicators have been developed collaboratively by representatives of 17 individual EU-funded NBS projects and collaborating institutions such as the EEA and JRC, as part of the European Taskforce for NBS Impact Assessment, with the four-fold objective of: serving as a reference for relevant EU policies and activities; orient urban practitioners in developing robust impact evaluation frameworks for nature-based solutions at different scales; expand upon the pioneering work of the EKLIPSE framework by providing a comprehensive set of indicators and methodologies; and build the European evidence base regarding NBS impacts. They reflect the state of the art in current scientific research on impacts of nature-based solutions and valid and standardized methods of assessment, as well as the state of play in urban implementation of evaluation frameworks.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Using Minecraft for Youth Participation in Urban Design and Governance"
date: 2015-01-01
summary: "UN-Habitat believes that ICT can be a catalyst to improve governance in towns and cities and help increase levels of participation, efficiency and accountability in public urban policies, provided that the tools are appropriately used, accessible, inclusive and affordable. Research shows that ICT us ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/01/Using-Minecraft-for-Youth-Participation-in-Urban-Design-and-Governance-2.jpg.webp?itok=QrdhsA7y"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/01/Using-Minecraft-for-Youth-Participation-in-Urban-Design-and-Governance-2.jpg.webp?itok=QrdhsA7y"
categories: ['planning', 'landscape', 'architecture', 'urban design']
tags: ['governance', 'participation']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://unhabitat.org/using-minecraft-for-youth-participation-in-urban-design-and-governance)

> **Abstract** 
>
> UN-Habitat believes that ICT can be a catalyst to improve governance in towns and cities and help increase levels of participation, efficiency and accountability in public urban policies, provided that the tools are appropriately used, accessible, inclusive and affordable. Research shows that ICT use by youth can have a direct impact on increasing civic engagement, giving them new avenues through which to become informed, shape opinions, get organized, collaborate and take action. 
UN-Habitat’s experiences of using the video game Minecraft as a community participation tool for public space design confirms this view and shows that providing youth with ICT tools can promote improved civic engagement. Youth are at the center of the ICT revolution, both as drivers and consumers of technological innovation. They are almost twice as networked as the global population as a whole, with the ICT age gap more pronounced in least developed countries where young people are up to three times more likely to be online than the general population. 
The purpose of this paper is to outline UN-Habitat’s approach to using information and communication technology (ICT) as an enabler to encourage youth participation in urban design and governance.


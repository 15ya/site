
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Ownership Rating: A tool for rating sense of ownership in public space"
date: 2022-01-01
summary: "Sense of ownership is the sense of control and responsibility that local residents have over their close living environment. The design and maintenance of public space and the different visions of ownership determine the use of these spaces. Insights into the determinants of sense of ownership, in r ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2022/10/ownership.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2022/10/ownership.png"
categories: ['landscape', 'planning', 'urban design']
tags: ['public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2022.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/ownership-rating/)

> **Abstract** 
>
> Sense of ownership is the sense of control and responsibility that local residents have over their close living environment. The design and maintenance of public space and the different visions of ownership determine the use of these spaces. Insights into the determinants of sense of ownership, in relation to the design and maintenance of public spaces, can contribute to increasing this sense among local residents.


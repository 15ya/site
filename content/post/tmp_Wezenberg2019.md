
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Plinth Rating Manual: How to observe and map ground floors in an area?"
date: 2019-01-01
summary: "Transforming a street, a district, an inner city, or creating a new district with a great city at eye level takes years and usually involves incremental steps forward. Nevertheless, it’s easy to make a quick start. One of the first steps is to involve the community from the beginning. The quality of ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'architecture', 'urban design']
tags: ['public space', 'streetscape', 'infrastructure', 'participation']  
showDate: false
showReadTime: false
---

**City at Eye Level**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/plinth-rating-manual/)

> **Abstract** 
>
> Transforming a street, a district, an inner city, or creating a new district with a great city at eye level takes years and usually involves incremental steps forward. Nevertheless, it’s easy to make a quick start. One of the first steps is to involve the community from the beginning. The quality of public space is not only determined by the street that the city owns and maintains. It includes everything you experience and see around you when walking on the street as a pedestrian. To achieve public space quality, the facades of buildings need to be included, especially the ground floors that we see most when we walk past the buildings.


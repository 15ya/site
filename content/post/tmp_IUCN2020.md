
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "IUCN Global Standard for Nature-based Solutions : first edition"
date: 2020-01-01
summary: "The IUCN Global Standard for Nature-based Solutions lists the Criteria and Indicators, as adopted by the 98th Meeting of the IUCN Council in 2020. ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2020-020-En.PNG?itok=_jIddPxJ"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2020-020-En.PNG?itok=_jIddPxJ"
categories: ['local government', 'planning', 'urban design']
tags: ['nbs', 'climate change', 'resilience', 'sustainability', 'urban nature', 'water']  
showDate: false
showReadTime: false
---

**IUCN**, 2020.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/49070)

> **Abstract** 
>
> The IUCN Global Standard for Nature-based Solutions lists the Criteria and Indicators, as adopted by the 98th Meeting of the IUCN Council in 2020.


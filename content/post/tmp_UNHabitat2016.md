
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The New Urban Agenda"
date: 2016-01-01
summary: "The New Urban Agenda was adopted at the United Nations Conference on Housing and Sustainable Urban Development (Habitat III) in Quito, Ecuador, on 20 October 2016. It was endorsed by the United Nations General Assembly at its sixty-eighth plenary meeting of the seventy-first session on 23 December 2 ..."
featureImage: "https://habitat3.org/wp-content/themes/habitat3/img/nua-en-si.jpg"
thumbnail: "https://habitat3.org/wp-content/themes/habitat3/img/nua-en-si.jpg"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['sdg']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://habitat3.org/the-new-urban-agenda/)

> **Abstract** 
>
> The New Urban Agenda was adopted at the United Nations Conference on Housing and Sustainable Urban Development (Habitat III) in Quito, Ecuador, on 20 October 2016. It was endorsed by the United Nations General Assembly at its sixty-eighth plenary meeting of the seventy-first session on 23 December 2016.
The New Urban Agenda represents a shared vision for a better and more sustainable future. If well-planned and well-managed, urbanization can be a powerful tool for sustainable development for both developing and developed countries.


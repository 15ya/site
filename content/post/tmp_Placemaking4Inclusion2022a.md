
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Placemaking for Inclusion Competences Framework with Ecosystem of Open Badges"
date: 2022-01-01
summary: "The Placemaking 4 Inclusion (PM4I) Competences Framework with Ecosystem of Open Badges (version1) focuses on the conceptual mapping of the benchmarks (aims and objectives for the trainers) and indicators (level of acquisition of competences for the learner) for the Competence Framework-syllabus. ..."
featureImage: "http://placemaking.4learning.eu/wp-content/uploads/sites/13/2022/11/17.png"
thumbnail: "http://placemaking.4learning.eu/wp-content/uploads/sites/13/2022/11/17.png"
categories: ['local government', 'planning']
tags: ['participation', 'governance', 'public space', 'streetscape']  
showDate: false
showReadTime: false
---

**Placemaking4Inclusion**, 2022.
[DOWNLOAD HERE](http://placemaking.4learning.eu/outputs/)

> **Abstract** 
>
> The Placemaking 4 Inclusion (PM4I) Competences Framework with Ecosystem of Open Badges (version1) focuses on the conceptual mapping of the benchmarks (aims and objectives for the trainers) and indicators (level of acquisition of competences for the learner) for the Competence Framework-syllabus.


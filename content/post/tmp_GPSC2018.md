
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Sustainability Framework"
date: 2018-01-01
summary: "The Urban Sustainability Framework (USF) is structured in two parts, along with annexes that explore the good practices of specific cities and organizations and the positive results of their initiatives. Part I: Understanding and Achieving Urban Sustainability lays out a process for, andpractical gu ..."
featureImage: "https://openknowledge.worldbank.org/server/api/core/bitstreams/c66d7865-2256-593e-8f09-5506040fa9b0/content"
thumbnail: "https://openknowledge.worldbank.org/server/api/core/bitstreams/c66d7865-2256-593e-8f09-5506040fa9b0/content"
categories: ['local government', 'planning']
tags: ['sustainability', 'governance']  
showDate: false
showReadTime: false
---

**World Bank**, 2018.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/29364)

> **Abstract** 
>
> The Urban Sustainability Framework (USF) is structured in two parts, along with annexes that explore the good practices of specific cities and organizations and the positive results of their initiatives. Part I: Understanding and Achieving Urban Sustainability lays out a process for, andpractical guidance on, a four-stage approach that includes (1) diagnosis of the city’s current situation; (2) definition of a vision for change and establishment of priorities; (3) an approachto financing of the plan that achieves and demonstrates fiscal sustainability; and (4) monitoring and evaluation. Part II: The GPSC Measuring Framework builds a common understanding of sustainability within the urban context through two “enabling” and four “outcome” dimensions. The enabling dimensions are (1) governance and integrated urban planning, and (2) fiscal sustainability. The outcome dimensions are (1) urban economies, (2) natural environment and resources, (3) climate action and resilience, and (4) inclusivity and quality of life.


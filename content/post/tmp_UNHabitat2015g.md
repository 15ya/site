
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Sustainable Building Design for Tropical Climates"
date: 2015-01-01
summary: "In 2010 the worldwide building sector was responsible for 24% of the total GHG emissions deriving from fossil fuel combustion, second only to the industrial sector; but, if the embodied energy of construction materials is included, the share is far higher and the building sector becomes the prime CH ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/02/Sustainable-Building-Design-for-Tropical-Climates.jpg.webp?itok=Et3yR09h"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/02/Sustainable-Building-Design-for-Tropical-Climates.jpg.webp?itok=Et3yR09h"
categories: ['architecture', 'landscape']
tags: ['housing', 'energy', 'sustainability', 'climate change', 'resilience']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://unhabitat.org/sustainable-building-design-for-tropical-climates)

> **Abstract** 
>
> In 2010 the worldwide building sector was responsible for 24% of the total GHG emissions deriving from fossil fuel combustion, second only to the industrial sector; but, if the embodied energy of construction materials is included, the share is far higher and the building sector becomes the prime CHG emitter. Thus,building design and construction have a significant effect on the chances of meeting the 2 °C target (keeping global temperature increase to 2 °C ). 
Developing countries are going to play a decisive role in the future world energy scenario, as a consequence of their economic development. Industrial energy consumption will grow, and a dramatic increase in energy consumption for transport can be expected, with the growth in the number of vehicles on the roads - if the currently accepted worldwide approach to mobility does not change. The increase in energy consumption in the building sector can be expected to be even more dramatic, not only because air conditioning will spread and the number of domestic electric and electronic appliances will grow, but also because of the increase in the number of buildings.


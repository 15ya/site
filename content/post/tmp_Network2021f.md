
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Implementing Land Policies: A Practical Guide for Assessing Capacity"
date: 2021-01-01
summary: "A lack of capacity in the land management and administration is a key reason that land policies are not effectively implemented in many countries and, indeed, is a reason that those policies are inappropriate or become outdated. This Guide outlines a process for assessing the capacity of the land ma ..."
featureImage: "https://gltn.net/wp-content/uploads/2021/04/Implementing-Land-Policies-863x880.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2021/04/Implementing-Land-Policies-863x880.jpg"
categories: ['local government']
tags: ['governance', 'land use']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2021.
[DOWNLOAD HERE](https://gltn.net/2021/04/21/implementing-land-policies-a-practical-guide-for-assessing-capacity/)

> **Abstract** 
>
> A lack of capacity in the land management and administration is a key reason that land policies are not effectively implemented in many countries and, indeed, is a reason that those policies are inappropriate or become outdated. This Guide outlines a process for assessing the capacity of the land management and administration system in a country as a whole, or just part of it (for example, just the land-valuation system, or the land management and administration system of a particular local authority). The approach is flexible and can be adjusted to the situation and the resources available. The primary output of the assessment is a set of recommendations for planning measures to develop the capacity of the system.


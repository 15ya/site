
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "EPA’s Local Foods, Local Resources Toolkit"
date: 2017-01-01
summary: "The EPA is offering the Local Foods, Local Places Toolkit to help communities interested in using local foods to support downtown and neighborhood revitalization. Developed by EPA’s Office of Sustainable Communities, it provides step-by-step instructions for planning and hosting the type of communit ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2017/11/Local-Foods-Local-Places-Toolkit-A-Guide-to-Help-Communities-Revitalize-Using-Local-Food-Systems-1-300x227.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2017/11/Local-Foods-Local-Places-Toolkit-A-Guide-to-Help-Communities-Revitalize-Using-Local-Food-Systems-1-300x227.png"
categories: ['planning', 'landscape']
tags: ['food', 'urban nature', 'participation']  
showDate: false
showReadTime: false
---

**EPA**, 2017.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/epas-local-foods-local-resources-toolkit/)

> **Abstract** 
>
> The EPA is offering the Local Foods, Local Places Toolkit to help communities interested in using local foods to support downtown and neighborhood revitalization. Developed by EPA’s Office of Sustainable Communities, it provides step-by-step instructions for planning and hosting the type of community workshop offered through the Local Foods, Local Places assistance program. The program helps communities create action plans that chart a course for using local foods to help meet a broad range of community goals. The program and its predecessor, Livable Communities in Appalachia, has worked with more than 80 communities since 2014. 
These efforts can help communities achieve multiple goals, such as: 
- Keeping food dollars within the local economy and supporting creation of new jobs in the region.
- Diversifying the local economy and sustaining or reinvigorating a region’s agricultural heritage.
- Increasing the vitality of a historic Main Street or an existing neighborhood, helping to attract reinvestment and growth to these areas.
- Revitalizing already-developed areas to reap environmental benefits.
- Reducing food insecurity and shrinking food deserts.


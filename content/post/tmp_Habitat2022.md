
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Master Tool on Climate Change: Flagship 3 RISE UP - Resilient Settlements for the Urban Poor"
date: 2022-01-01
summary: "Over recent years, UN-Habitat has developed various guides and tools in the context of climate change and urban/human settlements development; however a stocktaking and managing mechanism was lacking. To efficiently and effectively utilize the guides and tools and to maximize the use of limited reso ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/12/master%20tool%20on%20climate%20change.JPG.webp?itok=SOefeBge"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/12/master%20tool%20on%20climate%20change.JPG.webp?itok=SOefeBge"
categories: ['planning', 'local government']
tags: ['land use', 'informality', 'sustainability', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2022.
[DOWNLOAD HERE](https://unhabitat.org/master-tool-on-climate-change-flagship-3-rise-up-resilient-settlements-for-the-urban-poor)

> **Abstract** 
>
> Over recent years, UN-Habitat has developed various guides and tools in the context of climate change and urban/human settlements development; however a stocktaking and managing mechanism was lacking. To efficiently and effectively utilize the guides and tools and to maximize the use of limited resources, the importance of a master tool has been pointed out. This master tool is a living document, and it will be regularly updated as and when new guides and tools related to the Flagship Programme “Resilient Settlements for the Urban Poor” RISE UP are published. In the January 2022 version, 18 guides and tools are presented.


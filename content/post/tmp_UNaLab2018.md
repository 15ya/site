
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Municipal Governance Guidelines for Nature-Based Solutions"
date: 2018-01-01
summary: "The Municipal Governance Guidelines aim to support cities in overcoming governance-related challenges in relation to the effective development of nature-based solutions (NBS). ..."
featureImage: "https://unalab.eu/sites/default/files/2019-10/D6.2%20Municipal%20Governance%20Guidelines%20_2019-10-24_1728.jpg"
thumbnail: "https://unalab.eu/sites/default/files/2019-10/D6.2%20Municipal%20Governance%20Guidelines%20_2019-10-24_1728.jpg"
categories: ['local government']
tags: ['water', 'urban nature', 'sustainability', 'infrastructure', 'nbs', 'governance', 'climate change']  
showDate: false
showReadTime: false
---

**UNaLab**, 2018.
[DOWNLOAD HERE](https://unalab.eu/en/documents/d62-municipal-governance-guidelines)

> **Abstract** 
>
> The Municipal Governance Guidelines aim to support cities in overcoming governance-related challenges in relation to the effective development of nature-based solutions (NBS).


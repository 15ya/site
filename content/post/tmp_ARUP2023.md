
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Net-zero buildings: halving construction emissions today"
date: 2023-01-01
summary: "The built environment accounts for nearly 40% of global energy-related carbon emissions. Global decarbonization trajectories indicate that the industry needs to reduce these emissions by 50% by 2030 if it is to reach net zero by mid-century and achieve the climate goals of the Paris Agreement.  
Thi ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/n/wbcsd-nzb-halving-construction-emissions-today-1.jpg?h=485&mw=340&w=340&hash=E86752B079D977B236DA938B8DE66377"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/n/wbcsd-nzb-halving-construction-emissions-today-1.jpg?h=485&mw=340&w=340&hash=E86752B079D977B236DA938B8DE66377"
categories: ['architecture', 'planning']
tags: ['energy', 'housing', 'sustainability']  
showDate: false
showReadTime: false
---

**ARUP**, 2023.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/net-zero-buildings-halving-construction-emissions-today)

> **Abstract** 
>
> The built environment accounts for nearly 40% of global energy-related carbon emissions. Global decarbonization trajectories indicate that the industry needs to reduce these emissions by 50% by 2030 if it is to reach net zero by mid-century and achieve the climate goals of the Paris Agreement.  
This new report – the next instalment in a series of net-zero buildings publications by the World Business Council for Sustainable Development (WBCSD) and Arup – sets out we set out how we can halve the construction emissions in buildings starting today, adopting the primary principle of doing more with less.
The report provides clear strategies and actions which point to how we can achieve the rapid systemic changes required.


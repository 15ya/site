
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tools for Equitable Climate Resilience: Fostering Community Leadership"
date: 2020-01-01
summary: "River Network is developing Tools for Equitable Climate Resilience with two methods for addressing climate risks: (1) Leadership Development, and (2) Community Based Participatory Research. In this toolkit – Fostering Community Leadership – methods  for creating successful leadership programs are de ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/12/fostering-community-leadership-237x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/12/fostering-community-leadership-237x300.png"
categories: ['planning']
tags: ['water', 'equity', 'climate change', 'resilience', 'participation', 'urban nature']  
showDate: false
showReadTime: false
---

**River Network**, 2020.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/tools-for-equitable-climate-resilience-fostering-community-leadership-dec-2020/)

> **Abstract** 
>
> River Network is developing Tools for Equitable Climate Resilience with two methods for addressing climate risks: (1) Leadership Development, and (2) Community Based Participatory Research. In this toolkit – Fostering Community Leadership – methods  for creating successful leadership programs are described and case studies from organizations implementing such programs are provided. Informed by “on-the-ground practitioners of leadership development,” the guide highlights that collaborative approaches for equitable resilience are key.


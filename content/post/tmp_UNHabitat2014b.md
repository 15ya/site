
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Assessing the Impact of Eviction: Handbook"
date: 2014-01-01
summary: "Every year, millions of people around the world are either threatened by evictions or are actually unlawfully evicted. Forced evictions result in severe trauma and in serious declines in the standard of living of those that are already marginalized or vulnerable within their respective societies, of ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/01/Cover-page-AssessingTheImpactOfEviction-Handbook.jpg.webp?itok=pVSFd0yM"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/01/Cover-page-AssessingTheImpactOfEviction-Handbook.jpg.webp?itok=pVSFd0yM"
categories: ['local government', 'planning']
tags: ['housing', 'informality']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2014.
[DOWNLOAD HERE](https://unhabitat.org/assessing-the-impact-of-eviction-handbook)

> **Abstract** 
>
> Every year, millions of people around the world are either threatened by evictions or are actually unlawfully evicted. Forced evictions result in severe trauma and in serious declines in the standard of living of those that are already marginalized or vulnerable within their respective societies, often leaving them homeless, landless, and living in extreme poverty and destitution. Even if the phenomenon of forced evictions constitutes a distinct phenomenon under international law, the consequences arising therefrom are similar to those arising from arbitrary and/or involuntary displacement of people from their homes, lands and communities.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tools for measuring, modelling, and valuing ecosystem services"
date: 2018-01-01
summary: "Increasing interest in measuring, modelling and valuing ecosystem services (ES), the benefits that ecosystems provide to people, has resulted in the development of an array of ES assessment tools in recent years. Selecting an appropriate tool for measuring and modelling ES can be challenging. This d ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-028-En.PNG?itok=iAm_CiEy"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-028-En.PNG?itok=iAm_CiEy"
categories: ['planning', 'local government', 'landscape', 'urban design']
tags: ['urban nature', 'sustainability', 'climate change']  
showDate: false
showReadTime: false
---

**IUCN**, 2018.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/47778)

> **Abstract** 
>
> Increasing interest in measuring, modelling and valuing ecosystem services (ES), the benefits that ecosystems provide to people, has resulted in the development of an array of ES assessment tools in recent years. Selecting an appropriate tool for measuring and modelling ES can be challenging. This document provides guidance for practitioners on existing tools that can be applied to measure or model ES provided by important sites for biodiversity and nature conservation, including Key Biodiversity Areas (KBAs), natural World Heritage sites (WHS), and protected areas (PAs). Selecting an appropriate tool requires identifying the specific question being addressed, what sorts of results or outputs are required, and consideration of practical factors such as the level of expertise, time and data required for applying any given tool. This guide builds on existing reviews of ES assessment tools, but has an explicit focus on assessing ES for sites of importance for biodiversity and nature conservation.


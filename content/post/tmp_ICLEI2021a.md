
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Policy and planning tools for urban green justice"
date: 2021-01-01
summary: "A new toolkit provides planners and policymakers with 50 tools that fight displacement and gentrification while also improving the accessibility and inclusiveness of green spaces in cities. ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/06/BCNUEJ-policy-planning-tools-cover-211x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/06/BCNUEJ-policy-planning-tools-cover-211x300.jpg"
categories: ['planning', 'local government']
tags: ['urban nature', 'participation', 'sustainability', 'governance', 'urban development']  
showDate: false
showReadTime: false
---

**ICLEI**, 2021.
[DOWNLOAD HERE](https://www.bcnuej.org/2021/04/08/policy-and-planning-toolkit-for-urban-green-justice/)

> **Abstract** 
>
> A new toolkit provides planners and policymakers with 50 tools that fight displacement and gentrification while also improving the accessibility and inclusiveness of green spaces in cities.


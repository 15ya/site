
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Imparting City"
date: 2023-01-01
summary: "This practice orientated handbook aims at all urban actors wishing to develop and realise complex urban planning concepts. It sets out a series of techniques, methods and process models that range from analytical approaches and concept strategies to the creation of participatory projects. 
Creative  ..."
featureImage: "https://image.isu.pub/230117161611-49c2ac2f73d8bcc78cee1cb9b59cbe6e/jpg/page_1_thumb_large.jpg"
thumbnail: "https://image.isu.pub/230117161611-49c2ac2f73d8bcc78cee1cb9b59cbe6e/jpg/page_1_thumb_large.jpg"
categories: ['planning', 'urban design']
tags: ['participation', 'governance']  
showDate: false
showReadTime: false
---

**Tanja Siems**, 2023.
[DOWNLOAD HERE](https://issuu.com/birkhauser.ch/docs/imparting_city_look_inside)

> **Abstract** 
>
> This practice orientated handbook aims at all urban actors wishing to develop and realise complex urban planning concepts. It sets out a series of techniques, methods and process models that range from analytical approaches and concept strategies to the creation of participatory projects. 
Creative open-ended experiments have been proven as effective academic practice driven methods within applied participatory urban mediation. The book proposes a method-catalogue of immediately realisable approaches for experimental urban research as part of a design and planning procedure within education and practice.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Comprehensive Framework for NBS Assessment"
date: 2019-01-01
summary: "A product of Workpackage 4 of the EU-H2020 project PHUSICOS is a comprehensive framework to verify the performances of NBSs in risk management processes from both technical and socio-economic points of view. The comprehensive framework assesses the beneficial role of NBSs in ecosystem services, whic ..."
featureImage: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/framework.png?itok=9g-E3Kl3"
thumbnail: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/framework.png?itok=9g-E3Kl3"
categories: ['local government', 'planning']
tags: ['nbs']  
showDate: false
showReadTime: false
---

**PHUSICOS**, 2019.
[DOWNLOAD HERE](https://networknature.eu/product/29397)

> **Abstract** 
>
> A product of Workpackage 4 of the EU-H2020 project PHUSICOS is a comprehensive framework to verify the performances of NBSs in risk management processes from both technical and socio-economic points of view. The comprehensive framework assesses the beneficial role of NBSs in ecosystem services, which is a crucial metric for the overall evaluation of the implemented intervention and solutions. In addition to ecosystem services, environmental, economic and social indicators are coupled with the above-mentioned risk management indicators, defining positive co-benefits, as well as potentially undesirable side effects and social perceptions.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Our City Plans"
date: 2022-01-01
summary: "Our City Plans is a global toolbox that guides and supports local governments and urban actors to develop inclusive and integrated urban planning processes. 
It is organised in four phases: assessment, plan, operationalisation, and implementation. It includes 15 thematic blocks and 54 activities tha ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['participation', 'governance', 'land use', 'sustainability']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2022.
[DOWNLOAD HERE](https://ourcityplans.unhabitat.org/toolbox)

> **Abstract** 
>
> Our City Plans is a global toolbox that guides and supports local governments and urban actors to develop inclusive and integrated urban planning processes. 
It is organised in four phases: assessment, plan, operationalisation, and implementation. It includes 15 thematic blocks and 54 activities that include step-by-step instructions, adaptable tools and templates, and additional resources from UN-Habitat and external partners.


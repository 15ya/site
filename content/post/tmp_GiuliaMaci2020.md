
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Cities for Women: Urban Assessment Framework Through a Gender Lens"
date: 2020-01-01
summary: "Cities Alliance has developed a Cities for Women Framework as a first step to help local stakeholders to gain an understanding of the current engagement of women in the various dimensions of their environments. It is with this understanding that efforts can be made to formulate policy and engagement ..."
featureImage: "https://www.ukesa.info/images/CgNQlBmuPDtj8i1UZHYE9dXWRhkF67pS/Cities-for-women.JPG"
thumbnail: "https://www.ukesa.info/images/CgNQlBmuPDtj8i1UZHYE9dXWRhkF67pS/Cities-for-women.JPG"
categories: ['local government', 'planning', 'urban design']
tags: ['gender', 'participation', 'governance', 'resilience']  
showDate: false
showReadTime: false
---

**Cities Alliance**, 2020.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/publications/cities-women-urban-assessment-framework-through-gender-lens)

> **Abstract** 
>
> Cities Alliance has developed a Cities for Women Framework as a first step to help local stakeholders to gain an understanding of the current engagement of women in the various dimensions of their environments. It is with this understanding that efforts can be made to formulate policy and engagement techniques to improve women’s input in shaping their cities. 
This framework is also a tool to allow participatory processes whereby women can be active participants of the analysis and improvement of their cities.


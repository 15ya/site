
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guide to Measuring Neighborhood Change to Understand and Prevent Displacement"
date: 2019-01-01
summary: "The guide–developed by the National Neighborhood Indicators Partnership based on the Turning the Corner project–is designed to support the analysis of neighborhood change by government, university, and non-profit organizations. The guide starts by highlighting the importance of data to understand ne ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/04/NNIP_guide_neighborhood_change-236x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/04/NNIP_guide_neighborhood_change-236x300.jpg"
categories: ['local government', 'planning', 'urban design']
tags: ['urban development', 'equity', 'governance']  
showDate: false
showReadTime: false
---

**National Neighborhood Indicators Partnership**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/guide-to-measuring-neighborhood-change-to-understand-and-prevent-displacement-apr-2019/)

> **Abstract** 
>
> The guide–developed by the National Neighborhood Indicators Partnership based on the Turning the Corner project–is designed to support the analysis of neighborhood change by government, university, and non-profit organizations. The guide starts by highlighting the importance of data to understand neighborhood dynamics, create effective policy solutions, and promote informed discussions. Then, the guide provides practical steps for defining an analytical approach and communicating results. Also included is more information on indicators and data sources.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "EcoLogistics Low Carbon Action Plan for Urban Freight"
date: 2021-01-01
summary: "EcoLogistics: Low carbon freight for sustainable cities, a project led by ICLEI – Local Governments for Sustainability, is playing a critical role in helping develop road maps for efficient and sustainable freight transport. This LCAP-UF guidebook was created to provide local governments with a road ..."
featureImage: "https://static.e-library.iclei.org/covers/EcoLogistics_LCAP_guidebook-cover.png"
thumbnail: "https://static.e-library.iclei.org/covers/EcoLogistics_LCAP_guidebook-cover.png"
categories: ['local government', 'planning']
tags: ['mobility', 'sustainability']  
showDate: false
showReadTime: false
---

**ICLEI**, 2021.
[DOWNLOAD HERE](https://iclei.org/e-library/ecologistics-low-carbon-action-plan-for-urban-freight-lcap-uf-10/)

> **Abstract** 
>
> EcoLogistics: Low carbon freight for sustainable cities, a project led by ICLEI – Local Governments for Sustainability, is playing a critical role in helping develop road maps for efficient and sustainable freight transport. This LCAP-UF guidebook was created to provide local governments with a roadmap in enhancing the freight movement and operations with a low carbon approach aiming to reduce GHG emissions. It facilitates a safe, socially inclusive, accessible, reliable, affordable, fuel-efficient, environmentally friendly, low-carbon, and resilient freight movement in the city. The main objective of this guidebook is to help cities develop realistic and usable localized action plans that move away from the usual “one size fits all” model.


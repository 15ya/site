
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Maximizing the impact of partnerships for the SDGs"
date: 2019-01-01
summary: "The purpose of this guidebook is to support organizations and partnerships to maximize the value created by collaboration towards the Sustainable Development Goals. 
The guidebook deconstructs what “value” mean and the types of value that partnerships can create It also explores the range of partner ..."
featureImage: "https://sustainabledevelopment.un.org/content/images/imagemain400_2564.jpg"
thumbnail: "https://sustainabledevelopment.un.org/content/images/imagemain400_2564.jpg"
categories: ['planning', 'local government']
tags: ['governance', 'participation', 'sdg']  
showDate: false
showReadTime: false
---

**UNDESA**, 2019.
[DOWNLOAD HERE](https://sustainabledevelopment.un.org/index.php?page=view&type=400&nr=2564&menu=35)

> **Abstract** 
>
> The purpose of this guidebook is to support organizations and partnerships to maximize the value created by collaboration towards the Sustainable Development Goals. 
The guidebook deconstructs what “value” mean and the types of value that partnerships can create It also explores the range of partnerships that ca be established and how the nature of the partnership influences the type of value created for the partner and for beneficiaries. 
Understanding these issues in greater detail should support organizations in the process of identifying designing and managing effective partnership that deliver the greatest value, and to think more ambitiously and creatively about how they partner. 
This is a practical guide focused on maximizin the value of partnership and not a comprehensiv exploration of cross-sector partnerships. It is intende to complement and extend other more detaile accounts of the principles and practice of cross-secto partnership.


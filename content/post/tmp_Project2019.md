
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A guide to plan and promote ecotourism activities and measure their impacts in Mediterranean protected areas following the MEET approach"
date: 2019-01-01
summary: "This manual is based on, and represents an expanded and updated edition of the first MEET manual. The purpose of this manual is to provide protected area managing bodies and the local ecotourism sector with a clear pathway to plan and enhance engaged conservation-focused ecotourism in their areas. ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-Bios-Cons-Nat-Pro-262-002.JPG?itok=1pmOgWI_"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-Bios-Cons-Nat-Pro-262-002.JPG?itok=1pmOgWI_"
categories: ['local government', 'landscape']
tags: ['urban nature', 'sustainability', 'conservation']  
showDate: false
showReadTime: false
---

**DestiMED Project**, 2019.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/49089)

> **Abstract** 
>
> This manual is based on, and represents an expanded and updated edition of the first MEET manual. The purpose of this manual is to provide protected area managing bodies and the local ecotourism sector with a clear pathway to plan and enhance engaged conservation-focused ecotourism in their areas.


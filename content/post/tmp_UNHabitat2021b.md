
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Environmental and Social Safeguards System Version 3"
date: 2021-01-01
summary: "The ESSS 3.1 describes UN-Habitat’s commitments, roles, and responsibilities towards environmental and social (E&S) risks and impacts associated with its activities. The ESSS 3.1 ensures that that E&S risks and impacts are managed from the conceptualization to the implementation of projects and prog ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/ESSS3.0.JPG.webp?itok=Qmw7BmiZ"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/ESSS3.0.JPG.webp?itok=Qmw7BmiZ"
categories: ['local government']
tags: ['governance', 'sustainability', 'urban nature', 'resilience']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/environmental-and-social-safeguards-system-version-3-esss-31)

> **Abstract** 
>
> The ESSS 3.1 describes UN-Habitat’s commitments, roles, and responsibilities towards environmental and social (E&S) risks and impacts associated with its activities. The ESSS 3.1 ensures that that E&S risks and impacts are managed from the conceptualization to the implementation of projects and programmes, in accordance with local regulations and international standards.
The ESSS 3.1 establishes the standard that UN-Habitat will use to manage and improve its environmental and social performance and its activities consistently over time. The ESSS sets out UN-Habitat’s responsibilities to continuously develop, govern and control the identification, assessment, management, monitoring and reporting of all risks and impacts that may arise throughout the lifecycle of UN-Habitat’s projects and programmes.


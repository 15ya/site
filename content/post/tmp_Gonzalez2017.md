
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Community-Driven Climate Resilience Planning: A Framework"
date: 2017-01-01
summary: "The guide was developed by community-based organizations for community-based organizations working to develop and implement climate solutions. The information is also useful to philanthropic institutions funding climate resilience strategies as well as to government officials developing climate acti ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/07/NACRP-231x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/07/NACRP-231x300.jpg"
categories: ['planning', 'local government']
tags: ['participation', 'resilience', 'climate change', 'governance', 'equity']  
showDate: false
showReadTime: false
---

**National Association of Climate Resilience Planners**, 2017.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/community-driven-climate-resilience-planning-a-framework-july-2019/)

> **Abstract** 
>
> The guide was developed by community-based organizations for community-based organizations working to develop and implement climate solutions. The information is also useful to philanthropic institutions funding climate resilience strategies as well as to government officials developing climate action plans. The framework “1) advocates deepening democratic practices at the local and regional levels, 2) seeks to put forth the principles and practices defining this emergent field, and 3) outlines resources for community-based institutions implementing community-driven planning processes.”


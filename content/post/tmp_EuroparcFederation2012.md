
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Practical, profitable, protected : a starter guide to developing sustainable tourism in protected areas"
date: 2012-01-01
summary: "This book is a practical manual on how to develop and manage tourism in protected areas. It is for all those responsible for the management of protected areas as tourism destinations. These include not only protected-area managers but also local authorities, tourism promotion agencies and tourism bu ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['conservation', 'urban nature']  
showDate: false
showReadTime: false
---

**Europarc Federation, DE**, 2012.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/28972)

> **Abstract** 
>
> This book is a practical manual on how to develop and manage tourism in protected areas. It is for all those responsible for the management of protected areas as tourism destinations. These include not only protected-area managers but also local authorities, tourism promotion agencies and tourism business associations. It focuses on protected areas that are in an early stage of their tourism development and wish to do this in a well-planned and sustainable manner.


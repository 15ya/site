
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guide on Connecting Green Recovery and Resilience in Cities"
date: 2022-01-01
summary: "This Guide on Connecting Green Recovery and Resilience in Cities stems from a collaboration between Cities Alliance, ICLEI, the Resilient City Network and GIZ, under the Resilient Cities Action Package 2021 (ReCAP21). It outlines three key entry points for local governments to move towards an urban  ..."
featureImage: "https://www.citiesalliance.org/sites/default/files/styles/social_large/public/2022-02/KP%20cover.jpg?h=c74750f6&itok=syhBlYKX"
thumbnail: "https://www.citiesalliance.org/sites/default/files/styles/social_large/public/2022-02/KP%20cover.jpg?h=c74750f6&itok=syhBlYKX"
categories: ['local government', 'planning']
tags: ['sustainability', 'governance', 'risk reduction', 'resilience', 'urban nature', 'climate change']  
showDate: false
showReadTime: false
---

**ICLEI**, 2022.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/cities-alliance-knowledge/guide-connecting-green-recovery-and-resilience)

> **Abstract** 
>
> This Guide on Connecting Green Recovery and Resilience in Cities stems from a collaboration between Cities Alliance, ICLEI, the Resilient City Network and GIZ, under the Resilient Cities Action Package 2021 (ReCAP21). It outlines three key entry points for local governments to move towards an urban transformation and a resilient recovery process, which is also green in nature. Together, these entry points provide local governments with the practical support to identify comprehensive solutions that range from finance and physical interventions to cultural practices and governance arrangements that may contribute to achieving more resilient societies.


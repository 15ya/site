
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Drinking Water Guide"
date: 2019-01-01
summary: "River Network’s Drinking Water Guide is a first step in galvanizing a national network of advocates for safe, clean, affordable, and sustainable drinking water and drinking water systems. Supported by the C.S. Mott Foundation, and guided by a Great Lakes–based Advisory Committee, the Drinking Water  ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/05/Drinking-Water-Guide-A-Resource-for-Advocates-%E2%80%93-River-Network-300x231.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/05/Drinking-Water-Guide-A-Resource-for-Advocates-%E2%80%93-River-Network-300x231.png"
categories: ['planning', 'urban design', 'landscape']
tags: ['water', 'climate change', 'health', 'sustainability']  
showDate: false
showReadTime: false
---

**River Network**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/drinking-water-guide/)

> **Abstract** 
>
> River Network’s Drinking Water Guide is a first step in galvanizing a national network of advocates for safe, clean, affordable, and sustainable drinking water and drinking water systems. Supported by the C.S. Mott Foundation, and guided by a Great Lakes–based Advisory Committee, the Drinking Water Guide provides in-depth information, resources, and case studies organized into sections that answer these fundamental questions: 
Where does drinking water come from? How can we protect our drinking water?
What does a drinking water system do?
How do we ensure water is safe to drink?
How is drinking water cost calculated? What do water bills pay for?
How will climate change affect our water? What can we do about it?
How can we support community advocacy and engagement on drinking water issues?


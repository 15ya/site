
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Handling land: Innovative Tools for Land Governance and Secure Tenure"
date: 2012-01-01
summary: "Everyone has a relationship to land. It is an asset that, with its associated resources, allows its owner access to loans, to build their houses and to set up small businesses in cities. In rural areas, land is essential for livelihoods, subsistence and food security. However, land is a scarce resou ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2012/06/Handling-land-Innovative-Tools-for-Land-Governance-and-Secure-Tenure-1.jpg.webp?itok=rh12V9GJ"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2012/06/Handling-land-Innovative-Tools-for-Land-Governance-and-Secure-Tenure-1.jpg.webp?itok=rh12V9GJ"
categories: ['local government']
tags: ['urban development', 'land use', 'housing', 'governance', 'equity']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2012.
[DOWNLOAD HERE](https://unhabitat.org/handling-land-innovative-tools-for-land-governance-and-secure-tenure)

> **Abstract** 
>
> Everyone has a relationship to land. It is an asset that, with its associated resources, allows its owner access to loans, to build their houses and to set up small businesses in cities. In rural areas, land is essential for livelihoods, subsistence and food security. However, land is a scarce resource governed by a wide range of rights and responsibilities. And not everyone's right to land is secure. Mounting pressure and competition mean that improving land governance - the rules, processes and organizations through which decisions are made about land - is more urgent than ever. This book shows how the Global Land Tool Network is addressing these problems by setting an international agenda on land. It features the land tools that the Network has developed.
The Global Land Tool Network is a partnership of a wide range of organizations involved in land issues. Established in 2006, it has just completed its first phase of operations. This book celebrates the work of the Network so far and illustrates how all land stakeholders play a role in handling the critical social change needed towards achieving equitable access to land for all.


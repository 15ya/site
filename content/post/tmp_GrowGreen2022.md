
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "GrowGreen nature-based solutions co-design guide"
date: 2022-01-01
summary: "This guide is developed for practitioners responsible for the design and implementation of Naturebased Solutions (NbS) projects. It will provide a comprehensive guidance tool to develop climate adaptation projects based on NbS. The design and implementation of NbS is considered as a collaborative ef ..."
featureImage: "https://growgreenproject.eu/wp-content/uploads/2017/11/logo-2.jpg"
thumbnail: "https://growgreenproject.eu/wp-content/uploads/2017/11/logo-2.jpg"
categories: ['urban design', 'landscape', 'planning']
tags: ['urban nature', 'participation', 'nbs', 'streetscape']  
showDate: false
showReadTime: false
---

**GrowGreen**, 2022.
[DOWNLOAD HERE](https://growgreenproject.eu/nbs-co-design-guide-bipolaire/)

> **Abstract** 
>
> This guide is developed for practitioners responsible for the design and implementation of Naturebased Solutions (NbS) projects. It will provide a comprehensive guidance tool to develop climate adaptation projects based on NbS. The design and implementation of NbS is considered as a collaborative effort in which scientists, experts, policy makers, practitioners, citizens and other stakeholders work together on the planning and implementation of NbS. 
There are lots of tools and resources available that can sometimes be overwhelming and complicated. This guide will help the reader to understand a co-design process for NbS and provide further reading materials, tools and case studies that illustrate how the GG Codesign process has been implemented by GrowGreen partners. The aim of the guide is to help delivering successful NbS projects in order to have better cities for people and for the environment, ensuring it is done collaboratively and it is economically, technically, environmentally and socially acceptable. The guide is accompanied by a digital tool, that helps keep track of the process steps.


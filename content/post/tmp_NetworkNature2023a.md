
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Biodiversity-positive Design in Urban Areas with NBS: Wildlife-friendly Areas, Conservation Sites, the Public Realm - Design Brief 2"
date: 2023-01-01
summary: "This NetworkNature design brief series, the first of its kind, comprises three design briefs on biodiversity-positive design recommendations for urban and peri-urban areas with nature-based solutions. The series, developed with support of IFLA Europe, presents simple design suggestions for renaturin ..."
featureImage: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/Design%20brief%202.PNG?itok=WlI1Fwhz"
thumbnail: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/Design%20brief%202.PNG?itok=WlI1Fwhz"
categories: ['urban design', 'landscape', 'planning']
tags: ['water', 'urban nature', 'sustainability', 'nbs', 'risk reduction', 'resilience', 'public space', 'streetscape', 'climate change']  
showDate: false
showReadTime: false
---

**NetworkNature**, 2023.
[DOWNLOAD HERE](https://networknature.eu/product/29463)

> **Abstract** 
>
> This NetworkNature design brief series, the first of its kind, comprises three design briefs on biodiversity-positive design recommendations for urban and peri-urban areas with nature-based solutions. The series, developed with support of IFLA Europe, presents simple design suggestions for renaturing in built environments to restore or provide habitat for nature. It is not meant to replace professional ecological or landscape guidance, but rather to encourage designers to intentionally consider how they can adopt an interdisciplinary approach to make projects more biodiverse. Specifically, it encourages professionals to adapt to and achieve biodiversity positive design in urban areas via nature-based solutions (NBS).


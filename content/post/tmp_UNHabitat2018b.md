
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guide to the City Resilience Profiling Tool"
date: 2018-01-01
summary: "UN-Habitat defines a resilient city as one that is able to absorb, adapt and recover from the shocks and stresses that are likely to happen, transforming itself in a positive way toward sustainability. This guiding document explores the concept and various elements of urban resilience and explains t ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/01/CRPP%20Guide.png.webp?itok=2wiUYcAV"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/01/CRPP%20Guide.png.webp?itok=2wiUYcAV"
categories: ['local government']
tags: ['governance', 'sustainability', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2018.
[DOWNLOAD HERE](https://unhabitat.org/guide-to-the-city-resilience-profiling-tool)

> **Abstract** 
>
> UN-Habitat defines a resilient city as one that is able to absorb, adapt and recover from the shocks and stresses that are likely to happen, transforming itself in a positive way toward sustainability. This guiding document explores the concept and various elements of urban resilience and explains the various phases of the City Resilience Profiling Tool, which supports local actors to make a plan for resilience-based urban development in their city.


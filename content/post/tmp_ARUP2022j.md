
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tomorrow’s public transport system"
date: 2022-01-01
summary: "The future of public transport is characterised by uncertainty. While the climate crisis requires a fundamental shift in travel behaviour to secure a transition to net-zero, exponential technological progress is beginning to significantly impact transport business models and has the potential to res ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/t/tomorrows-public-transport-system-cover.jpg?h=190&mw=340&w=340&hash=11A80C8D047B74BD4C078FEEB869E22C"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/t/tomorrows-public-transport-system-cover.jpg?h=190&mw=340&w=340&hash=11A80C8D047B74BD4C078FEEB869E22C"
categories: ['planning', 'local government', 'urban design']
tags: ['mobility', 'infrastructure', 'streetscape', 'services']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/tomorrows-public-transport-system)

> **Abstract** 
>
> The future of public transport is characterised by uncertainty. While the climate crisis requires a fundamental shift in travel behaviour to secure a transition to net-zero, exponential technological progress is beginning to significantly impact transport business models and has the potential to reshape how we think about personal mobility.  
The benefits of an effective, multi-modal, integrated public transport system go beyond getting people from A to B but provide an opportunity to transform passengers' experience and contribute to a more sustainable future in our cities and neighbourhoods. 
This paper aims to enable transport authorities across the UK to shape a bold integrated transport vision for their area. We identify practical steps for authorities that move beyond reactive measures to create a public transport system capable of delivering significant modal shift, reducing car ownership and associated widespread benefits for cities.


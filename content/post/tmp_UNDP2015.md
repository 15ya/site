
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Catalyzing Climate Finance"
date: 2015-01-01
summary: "This guidebook is offered as a primer to countries to enable them to better assess the level and nature of assistance they will require to catalyse climate capital based on their unique set of national, regional and local circumstances. ..."
featureImage: "https://www.undp.org/sites/g/files/zskgke326/files/styles/publication_cover_image_mobile/public/publications/cover-catalyzing-climate-finance.gif?itok=dBwSNQQa"
thumbnail: "https://www.undp.org/sites/g/files/zskgke326/files/styles/publication_cover_image_mobile/public/publications/cover-catalyzing-climate-finance.gif?itok=dBwSNQQa"
categories: ['local government']
tags: ['governance', 'finance', 'climate change']  
showDate: false
showReadTime: false
---

**UNDP**, 2015.
[DOWNLOAD HERE](https://www.undp.org/publications/catalyzing-climate-finance)

> **Abstract** 
>
> This guidebook is offered as a primer to countries to enable them to better assess the level and nature of assistance they will require to catalyse climate capital based on their unique set of national, regional and local circumstances.


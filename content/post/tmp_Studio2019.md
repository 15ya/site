
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Neighbourhoods Made by Neighbours"
date: 2019-01-01
summary: "It all begins with an idea. Maybe you want to launch a business. Maybe you want to turn a hobby into something more. Or maybe you have a creative project to share with the world. Whatever it is, the way you tell your story online can make all the difference. ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'urban design']
tags: ['participation', 'public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://www.codesignstudio.com.au/free-guides)

> **Abstract** 
>
> It all begins with an idea. Maybe you want to launch a business. Maybe you want to turn a hobby into something more. Or maybe you have a creative project to share with the world. Whatever it is, the way you tell your story online can make all the difference.


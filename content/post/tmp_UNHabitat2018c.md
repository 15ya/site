
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate Change and National Urban Policies in Asia and the Pacific"
date: 2018-01-01
summary: "Climate Change and National Urban Policies in Asia and the Pacific – A regional guide for mainstreaming climate change into urban related urban-related policy, legislative, financial and institutional frameworks provides policy makers with a flexible and non-prescriptive approach that can help with  ..."
featureImage: "https://fukuoka.unhabitat.org/wp-content/uploads/2021/12/RegionalGuide_Print_NEWVersion-page-001-1-150x150.jpg"
thumbnail: "https://fukuoka.unhabitat.org/wp-content/uploads/2021/12/RegionalGuide_Print_NEWVersion-page-001-1-150x150.jpg"
categories: ['local government']
tags: ['climate change', 'governance', 'sustainability', 'resilience']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2018.
[DOWNLOAD HERE](https://fukuoka.unhabitat.org/en/related-publications/2842/?lang=en)

> **Abstract** 
>
> Climate Change and National Urban Policies in Asia and the Pacific – A regional guide for mainstreaming climate change into urban related urban-related policy, legislative, financial and institutional frameworks provides policy makers with a flexible and non-prescriptive approach that can help with the integration of climate change into urban policy at any point of the policy cycle.
This Guide suggests methods and steps for mainstreaming Climate Change into National Urban Policies. Government stakeholders can select the methods and tools needed based on their respective circumstances, under a framework of “Phases” and “Elements” that serve as the building blocks of the mainstreaming process. Within each of the Elements, which are aligned and consistent with phases of UN-Habitat’s National Urban Policy process, concrete actions covering various policy aspects are proposed to ensure effective mainstreaming; ranging from the substantive planning process, capacity development, vertical and horizontal policy alignment to multi-stakeholder participation.


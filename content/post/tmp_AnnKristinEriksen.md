
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Light Interventions Handbook"
date: 2019-01-01
summary: "Creating a light intervention can be a great way to enliven a place even in winter. Additionally, used by the community, it can increase the sense of ownership of a place and let people experience a sense of belonging. It is a fairly easy process. The projector is a simple, accessible and flexible t ..."
featureImage: ""
thumbnail: ""
categories: ['architecture', 'landscape', 'urban design']
tags: ['public space', 'infrastructure', 'art']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/light-interventions/)

> **Abstract** 
>
> Creating a light intervention can be a great way to enliven a place even in winter. Additionally, used by the community, it can increase the sense of ownership of a place and let people experience a sense of belonging. It is a fairly easy process. The projector is a simple, accessible and flexible technology. Projection onto a wall or fabric is relatively easy and very eye-catching. In this tutorial you will learn how to make light installations using a projector and simple materials.


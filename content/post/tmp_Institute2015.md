
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Building healthy places toolkit"
date: 2015-01-01
summary: "Strategies for Enhancing Health in the Built Environment ..."
featureImage: "https://m.media-amazon.com/images/I/512nd96+AhS._AC_UF1000,1000_QL80_.jpg"
thumbnail: "https://m.media-amazon.com/images/I/512nd96+AhS._AC_UF1000,1000_QL80_.jpg"
categories: ['landscape', 'planning', 'urban design']
tags: ['health', 'public space', 'food', 'mobility', 'sustainability', 'urban nature', 'land use', 'services']  
showDate: false
showReadTime: false
---

**Urban Land Institute**, 2015.
[DOWNLOAD HERE](https://bhptoolkit.uli.org/#about)

> **Abstract** 
>
> Strategies for Enhancing Health in the Built Environment


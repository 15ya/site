
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Port energy supply for green shipping corridors"
date: 2022-01-01
summary: "International shipping is a vital industry, facilitating global trade and transporting people around the world. At the same time, it produces greenhouse gas emissions – comparable in scale to industrialised nations such as Germany or Japan - and is a significant source of air pollution. 
Urgent acti ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/p/green-shipping-report_cover-image.jpg?h=480&mw=340&w=340&hash=95D823A228A799C775F6EC96DDDF5C27"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/p/green-shipping-report_cover-image.jpg?h=480&mw=340&w=340&hash=95D823A228A799C775F6EC96DDDF5C27"
categories: ['local government']
tags: ['mobility', 'energy']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/port-energy-supply-for-green-shipping-corridors)

> **Abstract** 
>
> International shipping is a vital industry, facilitating global trade and transporting people around the world. At the same time, it produces greenhouse gas emissions – comparable in scale to industrialised nations such as Germany or Japan - and is a significant source of air pollution. 
Urgent action is required to reduce emissions in a sustainable manner. The production, supply, and use of alternative fuels – many of which are linked to the hydrogen economy - are essential to this aim. 
The Arup, Lloyd's Register and The Resilience Shift report explores the opportunities and challenges associated with developing infrastructure for alternative fuels. Focusing on a case study of a green shipping triangle in the Atlantic Ocean, it outlines the critical role of ports as a bridge between low carbon energy infrastructure and decarbonised vessel fleets. 
We explore the infrastructure required for low carbon fuel supply, demonstrating the significant scale required even for initial projects, highlighting the need for an integrated approach to their development. We frame how a Total Value approach to these initial projects can unlock significant co-benefits, strengthening their case for investment.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Port Resilience Framework for Action"
date: 2022-01-01
summary: "Ports connect people and supply chains, protect and enhance the environment, and provide opportunities for people to thrive. Yet ports have continually faced a diversity of shocks and stresses - from extreme weather and cyber-attacks to aging infrastructure and the need to decarbonise. 
The disrupti ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/p/port-resilience-cover.jpg?h=481&mw=340&w=340&hash=A0A6426DA9D0E3F529688A310D154BB2"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/p/port-resilience-cover.jpg?h=481&mw=340&w=340&hash=A0A6426DA9D0E3F529688A310D154BB2"
categories: ['local government', 'planning']
tags: ['services', 'resilience', 'sustainability', 'infrastructure', 'water']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/port-resilience-framework-for-action)

> **Abstract** 
>
> Ports connect people and supply chains, protect and enhance the environment, and provide opportunities for people to thrive. Yet ports have continually faced a diversity of shocks and stresses - from extreme weather and cyber-attacks to aging infrastructure and the need to decarbonise. 
The disruptive forces that ports are exposed to and must respond to will increase in frequency, severity and complexity over time. Although we can draw lessons from recent events, the past is no longer a good predictor of the future. The nature of and response to these interrelated disruptors means that ports must be prepared for the threats we can anticipate and be able to respond to those we cannot predict or avoid – a shift to resilience. 
Together Arup and Resilience Rising are exploring opportunities for resilient, system-wide, transformation - creating a framework for action. 
This framework for action describes ten goals - across three dimensions: economy and society; leadership and strategy; and infrastructure and ecosystems – that together can transform port performance. It provides a line of sight for resilience from a policy level through to implementation at a port asset level.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Wetlands Protection and Restoration Guide"
date: 2021-01-01
summary: "The Urban Wetlands Protection and Restoration Guide summarizes the findings of a two-year project carried out by the Association of State Wetland Managers, with assistance from a workgroup of experts, to identify ways to enhance, protect, and restore wetlands within and surrounding urban areas to ma ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/08/Screen-Shot-2021-08-06-at-10.19.29-AM-294x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/08/Screen-Shot-2021-08-06-at-10.19.29-AM-294x300.png"
categories: ['landscape', 'planning']
tags: ['conservation', 'urban nature', 'sustainability']  
showDate: false
showReadTime: false
---

**Association of State Wetland Managers**, 2021.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/urban-wetlands-protection-and-restoration-guide/)

> **Abstract** 
>
> The Urban Wetlands Protection and Restoration Guide summarizes the findings of a two-year project carried out by the Association of State Wetland Managers, with assistance from a workgroup of experts, to identify ways to enhance, protect, and restore wetlands within and surrounding urban areas to maximize economic, ecological, and social benefits for urban communities. It also provides a road map for future efforts to improve policies, programs, and actions that restore and protect urban wetlands. Of particular interest to UWLN members, Chapter 4 has a good list of grant and financial assistance programs available.


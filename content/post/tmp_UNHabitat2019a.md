
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "City-Wide Public Space Strategies: A Compendium of Inspiring Practices"
date: 2019-01-01
summary: "Public space is more than well designed physical places. It is an arena for social interaction and active citizenship that can spark social and economic development and drive environmental sustainability. The design, provision and maintenance of well connected systems of public space are integral to ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/03/City-Wide%20Public%20Space%20Strategies%20A%20Compendium%20of%20Inspiring%20Practices%20-%20cover.png.webp?itok=ezBlIHrb"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/03/City-Wide%20Public%20Space%20Strategies%20A%20Compendium%20of%20Inspiring%20Practices%20-%20cover.png.webp?itok=ezBlIHrb"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['public space', 'streetscape', 'governance', 'participation']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2019.
[DOWNLOAD HERE](https://unhabitat.org/city-wide-public-space-strategies-a-compendium-of-inspiring-practices)

> **Abstract** 
>
> Public space is more than well designed physical places. It is an arena for social interaction and active citizenship that can spark social and economic development and drive environmental sustainability. The design, provision and maintenance of well connected systems of public space are integral to achieving a safe and accessible city. However, cities must move beyond typically site-specific approaches to addressing public space if sustainable and longer lasting benefits are to be achieved. Establishing and implementing a city-wide strategy that approaches a city as a multi-functional and connected urban system can ensure the best chances of proactively driving good urban development. 
A thorough strategy offers cities an action-oriented approach encompassing not only spatial goals, but governance arrangements, implementation plans, budgetary needs and measurable indicators. It should be formulated to overcome common obstacles to the successful provision of public spaces throughout a city. With adequate political support and funding, a city-wide public space strategy can deliver a well-distributed, accessible and inclusive public space system.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Equitable Water Infrastructure Toolkit"
date: 2021-01-01
summary: "This River Network Equitable Infrastructure Toolkit is a one-stop shop for community stakeholders, advocates, and leaders to: 
Identify the factors that affect water affordability; 
Become familiar with water infrastructure funding and financing mechanisms; and 
Understand the role and impact of loc ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/06/RN-EWIT-230x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/06/RN-EWIT-230x300.jpg"
categories: ['urban design']
tags: ['water', 'infrastructure', 'services', 'equity']  
showDate: false
showReadTime: false
---

**River Network**, 2021.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/equitable-water-infrastructure-toolkit-june-2021/)

> **Abstract** 
>
> This River Network Equitable Infrastructure Toolkit is a one-stop shop for community stakeholders, advocates, and leaders to: 
Identify the factors that affect water affordability; 
Become familiar with water infrastructure funding and financing mechanisms; and 
Understand the role and impact of local, state, and federal entities and community organizations in addressing affordability and sustainability.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Climate Adaptation Actions for Urban Forests and Human Health"
date: 2021-01-01
summary: "The report provides an Urban Forest and Health Menu that compiles nature-based adaptation actions from peer-reviewed research. The menu includes nine adaptation strategies that are designed to be used with the Adaptation Workbook–a step-by-step process that helps users consider potential impacts of  ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/09/urban-forest-climate-and-health-menu-230x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/09/urban-forest-climate-and-health-menu-230x300.jpg"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['urban nature', 'sustainability', 'health', 'climate change']  
showDate: false
showReadTime: false
---

**Forest Service, U.S. Department of Agriculture**, 2021.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/climate-adaptation-actions-for-urban-forests-and-human-health/)

> **Abstract** 
>
> The report provides an Urban Forest and Health Menu that compiles nature-based adaptation actions from peer-reviewed research. The menu includes nine adaptation strategies that are designed to be used with the Adaptation Workbook–a step-by-step process that helps users consider potential impacts of climate change and restoration actions.


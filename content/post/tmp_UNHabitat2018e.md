
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "International Guidelines on Urban and Territorial Planning Handbook"
date: 2018-01-01
summary: "In 2015, the Governing Council of UN-Habitat approved the International Guidelines on Urban and Territorial Planning as a set of universal principles for the improvement of policies, plans, designs and implementation processes that lead to more compact, socially inclusive, better integrated and conn ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2018/05/Handbook-cover.jpg.webp?itok=HpNtm0BC"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2018/05/Handbook-cover.jpg.webp?itok=HpNtm0BC"
categories: ['local government', 'planning']
tags: ['governance', 'land use', 'mobility']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2018.
[DOWNLOAD HERE](https://unhabitat.org/international-guidelines-on-urban-and-territorial-planning-ig-utp-handbook)

> **Abstract** 
>
> In 2015, the Governing Council of UN-Habitat approved the International Guidelines on Urban and Territorial Planning as a set of universal principles for the improvement of policies, plans, designs and implementation processes that lead to more compact, socially inclusive, better integrated and connected cities and territories that foster sustainable urban development and are resilient to climate change. Still, while the Guidelines serve as a compass to guide decision-makers, a practical companion is required to operationalize their principles and recommendations. This handbook for the Guidelines is that companion, and part of a series of tools designed by UN-Habitat to improve planning practice.
The approach of the Guidelines requires that planning is examined not only as a technical tool for urban and territorial change, but is also tested for the quality of the decision-making processes. Thus, the handbook provides an overview of the scope of topics covered by the Guidelines and puts special emphasis on planning processes, products and outcomes while making reference to additional tools, literature and resources for planners, civil society actors and policy-makers at the national and local levels.


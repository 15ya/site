
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "New strategy for Renaturing Cities through Nature Based Solutions - Good Practices Kit"
date: 2022-01-01
summary: "This kit will help you in using Nature-Based Solutions (NBS) to make your city more liveable and resilient to climate change. It offers a set of best practices and recommendations that cover all the stages of NBS implementation: how to select the right NBS, how to set one up and how to monitor it. 
 ..."
featureImage: "https://www.urbangreenup.eu/imgpub/2043804/900/0/05.jpg"
thumbnail: "https://www.urbangreenup.eu/imgpub/2043804/900/0/05.jpg"
categories: ['planning', 'urban design', 'landscape', 'architecture']
tags: ['urban nature', 'sustainability', 'nbs', 'climate change']  
showDate: false
showReadTime: false
---

**URBAN GreenUP**, 2022.
[DOWNLOAD HERE](https://www.urbangreenup.eu/resources/good-practices-kit/good-practices-kit.kl)

> **Abstract** 
>
> This kit will help you in using Nature-Based Solutions (NBS) to make your city more liveable and resilient to climate change. It offers a set of best practices and recommendations that cover all the stages of NBS implementation: how to select the right NBS, how to set one up and how to monitor it. 
The advice in this kit draws on the insights from the European project URBAN GreenUP, that has developed and applied “Renaturing Urban Plans” in various cities across the world. The goal is to use pioneering NBS to mitigate the effects of climate change, improve air quality and water management, and to make cities more sustainable.


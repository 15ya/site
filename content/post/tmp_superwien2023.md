
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Walk’n’Roll Booklet"
date: 2023-01-01
summary: "One of the key challenges many cities across Europe face is the physical separation of the different components of everyday life, leading to significant mobility demand. As the current mobility systems in most cities can still be characterized by the dominance of the individual car, this demand is m ..."
featureImage: "https://superwien.com/wp-content/uploads/2023/02/WalknRoll-cover.jpg"
thumbnail: "https://superwien.com/wp-content/uploads/2023/02/WalknRoll-cover.jpg"
categories: ['local government', 'planning', 'urban design']
tags: ['mobility', 'infrastructure', 'streetscape', 'public space']  
showDate: false
showReadTime: false
---

**superwien**, 2023.
[DOWNLOAD HERE](https://superwien.com/portfolio/urbact-walknroll/)

> **Abstract** 
>
> One of the key challenges many cities across Europe face is the physical separation of the different components of everyday life, leading to significant mobility demand. As the current mobility systems in most cities can still be characterized by the dominance of the individual car, this demand is met to a large extent by car use. People drive cars to improve their quality of life by significantly shortening the time needed for moving between different parts of the city – to work or to use various services. In reality, however, car-oriented local mobility has a wide range of adverse consequences, many of which negatively affect the quality of life already in the short run. 
While most cities understand the problem, its likely consequences and are committed to implement a shift towards more sustainable urban mobility and public space use, this is easier said than done. That’s why 28 European cities of different sizes from 16 countries have come together to face today’s mobility challenges. Partners of the RiConnect, Space4People and Thriving Streets URBACT Action Planning Networks decided to establish a long term cooperation and created Walk’n’Roll Cities – a platform to exchange ideas, inspirations and learn from each other. 
Together, these cities explored visions and interventions that could contribute to massive reduction of car use in our cities. And, thanks to the Knowledge Hub initiative of the URBACT programme, an online publication has also been created that presents these visions and interventions. It is an accessible, practical and concise resource for local politicians, decision-makers, professionals, city practitioners – and even for citizens – who are interested in urban mobility – and committed to make their city a better place for people.


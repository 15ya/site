
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Getting it right from planning to reporting: A guidance tool for women’s land rights data and statistics"
date: 2021-01-01
summary: "This guidance tool aims to explain the practical steps towards enhancing the quality of women’s land rights data and statistics for data producers, analysts, and researchers. In doing so, it addresses critical gaps in the quality of the design, collection, analysis, management, and dissemination of  ..."
featureImage: "https://www.unwomen.org/sites/default/files/Headquarters/Images/Sections/Digital%20Library/2021/Guidance-tool-Getting-it-right-from-planning-to-reporting-en.jpg"
thumbnail: "https://www.unwomen.org/sites/default/files/Headquarters/Images/Sections/Digital%20Library/2021/Guidance-tool-Getting-it-right-from-planning-to-reporting-en.jpg"
categories: ['local government']
tags: ['data', 'equity', 'land use', 'informality', 'gender']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2021.
[DOWNLOAD HERE](https://www.unwomen.org/en/digital-library/publications/2021/05/guidance-tool-getting-it-right-from-planning-to-reporting)

> **Abstract** 
>
> This guidance tool aims to explain the practical steps towards enhancing the quality of women’s land rights data and statistics for data producers, analysts, and researchers. In doing so, it addresses critical gaps in the quality of the design, collection, analysis, management, and dissemination of data and statistics on women’s land rights. 
The tool recognizes the diverse relationships between women and land with respect to their land rights, which vary from country to country and even context to context, based on legal, policy, and institutional regimes. 
In particular, this tool provides guidance on survey design, data collection, statistics development process, analysis, and reporting on women's land rights data. It suggests good practices for enhancing the quality of data and statistics on women’s land rights in the context of measuring, monitoring, and reporting on Sustainable Development Goals indicators 1.4.2, 5.a.1, and 5.a.2. 
This tool is for use by data producers and data users alike. It is a useful tool for government data and statistics authorities, international organizations, the private sector, civil society, academia, research, and grassroots organizations that generate and/or use women’s land rights data and statistics. Building on the global methodologies for monitoring these SDG indicators, this tool profiles and strengthens responsiveness to women’s land rights issues in the generation of data and statistics to ensure quality and reliable data for evidence-based decision making.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Inclusive Community Engagement Playbook"
date: 2019-01-01
summary: "Community engagement is the process of involving the people that live and interact with your city in its development, including anyone with an interest or influence in, or who is impacted by, a local plan, policy or action. Engagement strategies help cities to develop a better working relationship w ..."
featureImage: "https://www.thegpsc.org/sites/gpsc/files/playbook.png"
thumbnail: "https://www.thegpsc.org/sites/gpsc/files/playbook.png"
categories: ['planning', 'urban design']
tags: ['sustainability', 'participation', 'governance']  
showDate: false
showReadTime: false
---

**C40 Cities Climate Leadership Group**, 2019.
[DOWNLOAD HERE](https://www.c40knowledgehub.org/s/article/Inclusive-Community-Engagement-Playbook?language=en_US)

> **Abstract** 
>
> Community engagement is the process of involving the people that live and interact with your city in its development, including anyone with an interest or influence in, or who is impacted by, a local plan, policy or action. Engagement strategies help cities to develop a better working relationship with the community to ensure that the needs and issues of all parties are understood, and can be addressed to achieve positive change. 
This Playbook is a detailed practitioners’ guide on everything cities need to know about how to deliver inclusive community engagement. It includes an innovative and diverse selection of tools of varying complexity to cater to cities with different needs and capacity, and case studies from cities around the world. 
The Playbook is intended to provide practical support to cities to develop a comprehensive and effective engagement strategy, and can be used by any official across city departments involved in the process of developing and implementing climate mitigation or adaptation action. It can be used by cities at different points of climate action planning: at the scoping stage, during planning, design and implementation. It should be used alongside national, regional and local regulation, as well as the project objectives, to create a tailored engagement approach.


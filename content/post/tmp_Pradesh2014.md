
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Land Governance Assessment Framework"
date: 2014-01-01
summary: "The Land Governance Assessment Framework (LGAF) is a diagnostic tool to assess the status of land governance at country level using a participatory process that draws systematically on existing evidence and local expertise rather than on outsiders. The analysis covers nine themes: land tenure recogn ..."
featureImage: "https://openknowledge.worldbank.org/server/api/core/bitstreams/4b55689a-d66a-5a05-9c75-dfe634455e04/content"
thumbnail: "https://openknowledge.worldbank.org/server/api/core/bitstreams/4b55689a-d66a-5a05-9c75-dfe634455e04/content"
categories: ['local government', 'planning']
tags: ['governance', 'land use']  
showDate: false
showReadTime: false
---

**Centre for Good Governance**, 2014.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/28518)

> **Abstract** 
>
> The Land Governance Assessment Framework (LGAF) is a diagnostic tool to assess the status of land governance at country level using a participatory process that draws systematically on existing evidence and local expertise rather than on outsiders. The analysis covers nine themes: land tenure recognition; rights to forest and common lands and rural land use regulations; urban land use, planning, and development; public land management; process for transfer of public land to private use; public provision of land information (land administration and information systems); land valuation and taxation; dispute resolution and review of institutional arrangements and policies. The assessment follows a scorecard approach and produces a matrix of policy priorities matrix. The LGAF process helps to establish a consensus on (i) gaps in existing evidence; (ii) areas for regulatory or institutional change, piloting of new approaches, and interventions to improve land governance on a broader scale (e.g. by strengthening land rights and improving their enforcement); and (iii) criteria to assess the effectiveness of these measures. This report presents the result for Andhra Pradesh.


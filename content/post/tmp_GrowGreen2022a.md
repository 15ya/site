
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Green Cities Framework (GCF) Handbook"
date: 2022-01-01
summary: "Cities of all sizes and types can develop effective strategies for nature-based solutions, allowing them to create innovative and inspiring ways to tackle major urban challenges, such as flooding and heat stress. Embedding nature-based solutions in this way in long-term city planning, development an ..."
featureImage: "https://growgreenproject.eu/wp-content/uploads/2017/11/logo-2.jpg"
thumbnail: "https://growgreenproject.eu/wp-content/uploads/2017/11/logo-2.jpg"
categories: ['local government', 'planning', 'urban design']
tags: ['sustainability', 'nbs', 'urban nature', 'water']  
showDate: false
showReadTime: false
---

**GrowGreen**, 2022.
[DOWNLOAD HERE](https://networknature.eu/product/29002)

> **Abstract** 
>
> Cities of all sizes and types can develop effective strategies for nature-based solutions, allowing them to create innovative and inspiring ways to tackle major urban challenges, such as flooding and heat stress. Embedding nature-based solutions in this way in long-term city planning, development and management will reduce climate impacts, support health and create an attractive environment for citizens, visitors and investments. The Green Cities Framework takes cities through the different elements of developing a successful strategy for nature-based solutions, ensuring the final outcome is grounded in evidence, co-developed with stakeholders and easy to put into practice. It is a replicable tool and is specifically designed for the distinctive characteristics of nature-based solutions, drawing on the outcomes of GrowGreen. The framework is supported by the GrowGreen Training Programme, which provides tools and resources from GrowGreen, examples of how they have been used in GrowGreen cities, and a selection of existing online resources for each element of strategy development.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Land tenure and sustainable agri-food systems"
date: 2021-01-01
summary: "World hunger increased in 2020, propelled by the COVID-19 pandemic. The prevalence of undernourishment rose from 8.4% to around 9.9% between 2019 and 2020. Between 720 and 811 million people in the world faced hunger in 2020 according to the Food and Agriculture Organization, IFAD, UNICEF, WFP and W ..."
featureImage: "https://gltn.net/wp-content/uploads/2022/02/fao-unh.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2022/02/fao-unh.jpg"
categories: ['local government', 'planning']
tags: ['food', 'equity', 'informality']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2021.
[DOWNLOAD HERE](https://gltn.net/2022/02/17/land-tenure-and-sustainable-agri-food-systems/)

> **Abstract** 
>
> World hunger increased in 2020, propelled by the COVID-19 pandemic. The prevalence of undernourishment rose from 8.4% to around 9.9% between 2019 and 2020. Between 720 and 811 million people in the world faced hunger in 2020 according to the Food and Agriculture Organization, IFAD, UNICEF, WFP and WHO (FAO et al., 2021). In addition, when using the Food Insecurity Experience Scale, it is estimated that nearly one in three people in the world (2.37 billion) did not have access to adequate food in 2020 – a growth of almost 320 million people in just one year (FAO et al., 2021).
These numbers reveal the intense pressure placed on land and related natural resources and are a reminder of the challenges facing our agri-food systems. When access to land and tenure rights fail to comply with human rights and social, economic and environmental sustainability, agri-food systems will be compromised and may lead to the exclusion of vulnerable groups, as well as unsustainable patterns of food production and consumption. Protecting the land rights of the poor, including women and youth, whether in written or non-written forms, can be crucial for vulnerable groups, who are disproportionately more prone to suffer evictions, disinheritance and displacements.
In response to these challenges, FAO in collaboration with UN-Habitat – Global Land Tool Network (GLTN) and the International Land Coalition (ILC) developed a policy paper on Land Tenure and Sustainable Agri-Food Systems highlighting the need for securing land rights beyond the farming community and for the array of actors, including micro, small and medium sized agri-food enterprises. Secure access to land and tenure rights needs to be considered part of a broader investment and enabling policy environment to support the sustainability of agri-food systems in the long term.
This policy paper was developed through a consultative process involving a wide range of stakeholders in civil society, academia, grassroots organizations and private sector from different parts of the world and was supported by several partners, including bilateral and multilateral donors within the Global Donor Working Group on Land (GDWGL).
This paper makes the case for the priority of reforming and securing access and tenure rights to land and natural resources. In doing so, it identifies key actions and recommendations concerning tenure security and equitable access to land that may contribute to a broader policy agenda to improve food and nutrition security and agri-food systems transformation. The emphasis is on tenure security and equitable access to land as critical contributing factors towards sustainable agri-food systems, poverty reduction and the overall achievement of the SDGs.


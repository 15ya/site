
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The New Urban Agenda Illustrated Handbook"
date: 2020-01-01
summary: "The New Urban Agenda, adopted at Habitat III in Quito, Ecuador, on 20 October 2016, presents a paradigm shift based on the science of cities and lays out standards and principles for the planning, construction, development, management and improvement of urban areas. The New Urban Agenda is intended  ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/12/NUA%20Handbook_FINAL4.png.webp?itok=JpFer0o8"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/12/NUA%20Handbook_FINAL4.png.webp?itok=JpFer0o8"
categories: ['local government', 'planning', 'landscape', 'architecture', 'urban design']
tags: ['sdg']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/the-new-urban-agenda-illustrated)

> **Abstract** 
>
> The New Urban Agenda, adopted at Habitat III in Quito, Ecuador, on 20 October 2016, presents a paradigm shift based on the science of cities and lays out standards and principles for the planning, construction, development, management and improvement of urban areas. The New Urban Agenda is intended as a resource for different actors in multiple levels of government and for civil society organizations, the private sector and all who reside in urban spaces of the world. The New Urban Agenda highlights linkages between sustainable urbanization and job creation, livelihood opportunities and improved quality of life, and it insists on incorporation of all these sectors in every urban development or renewal policy and strategy.


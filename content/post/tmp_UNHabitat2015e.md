
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A Practical Guide to Designing, Planning, and Executing Citywide Slum Upgrading Programmes"
date: 2015-01-01
summary: "While 220 million people have been lifted out of slum conditions over the past 10 years, the number of people living in slum conditions is likely to grow by six million every year, to reach a total of 889 million by 2020. It is necessary to equip cities and their practitioners with the tools and cap ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/07/Untitled-1-1.jpg.webp?itok=bB9Wnxib"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2015/07/Untitled-1-1.jpg.webp?itok=bB9Wnxib"
categories: ['local government', 'planning']
tags: ['land use', 'informality', 'housing', 'infrastructure', 'governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://unhabitat.org/a-practical-guide-to-designing-planning-and-executing-citywide-slum-upgrading-programmes)

> **Abstract** 
>
> While 220 million people have been lifted out of slum conditions over the past 10 years, the number of people living in slum conditions is likely to grow by six million every year, to reach a total of 889 million by 2020. It is necessary to equip cities and their practitioners with the tools and capacities to anticipate and control urban growth and city officials will require knowledge, skills and methodologies that will allow them not only to upgrade existing slums but also prevent the appearance of new ones.  
This Guide advocates for a citywide approach to slum upgrading, which represents a fundamental shift from piecemeal project interventions to a citywide programme approach. This Practical Guide is part of a trilogy on citywide slum upgrading that includes Streets as Tools for Urban Transformation in Slums: A Street-led Approach to Citywide Slum Upgrading and A Training Module for Designing and Implementing Citywide Slum Upgrading. With the other two partner publications, the Practical Guide provides an accessible tool for practitioners, leading them through UN-Habitat steps towards a successful citywide slum-upgrading program.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Performance and Impact Monitoring of Nature-Based Solutions"
date: 2019-01-01
summary: "Report which summarises the classification and mode of action of nature-based solutions (NBS), the selection of key indicators of NBS performance and impact, design of an NBS monitoring scheme, and baseline establishment along with a suite of measurement/monitoring methods for key indicators and met ..."
featureImage: "https://unalab.eu/sites/default/files/2019-10/D3.1%20NBS%20Performance%20and%20Impact%20Monitoring%20Report_2019-10-24_1714.jpg"
thumbnail: "https://unalab.eu/sites/default/files/2019-10/D3.1%20NBS%20Performance%20and%20Impact%20Monitoring%20Report_2019-10-24_1714.jpg"
categories: ['local government']
tags: ['governance', 'sustainability', 'nbs']  
showDate: false
showReadTime: false
---

**UNaLab**, 2019.
[DOWNLOAD HERE](https://unalab.eu/en/documents/d31-nbs-performance-and-impact-monitoring-report)

> **Abstract** 
>
> Report which summarises the classification and mode of action of nature-based solutions (NBS), the selection of key indicators of NBS performance and impact, design of an NBS monitoring scheme, and baseline establishment along with a suite of measurement/monitoring methods for key indicators and metrics.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "River Access Planning Guide"
date: 2019-01-01
summary: "The River Access Planning Guide is a publication resulting from a collaboration between American Whitewater, the River Management Society, the Bureau of Land Management, the US Forest Service, and the National Park Service. The guide is designed for planners, river managers, and private entities who ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/12/river-access-planning-guide-nps-234x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/12/river-access-planning-guide-nps-234x300.jpg"
categories: ['planning', 'urban design', 'landscape']
tags: ['water', 'urban nature', 'sustainability', 'public space']  
showDate: false
showReadTime: false
---

**American Whitewater**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/river-access-planning-guide/)

> **Abstract** 
>
> The River Access Planning Guide is a publication resulting from a collaboration between American Whitewater, the River Management Society, the Bureau of Land Management, the US Forest Service, and the National Park Service. The guide is designed for planners, river managers, and private entities who are establish new river access points or update existing ones. The authors state: 
“Access points along rivers are gateways to river recreation. They can serve as launch facilities for boats or other watercraft and allow opportunities for visitors to enjoy and experience activities around the water. Providing for these diverse visitor uses while protecting natural resources and sustaining desired recreation outcomes can be challenging. The River Access Planning Guide addresses these challenges by providing a step-by-step procedure to evaluate existing and anticipated uses, select appropriate sites, and design facilities that support desired recreation experiences.” 
The guide includes a step-by-step framework for design as well as appendices with case studies, background information, publications and activity sheets.


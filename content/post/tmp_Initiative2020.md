
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Sharing in the Benefits of a Greening City"
date: 2020-01-01
summary: "The guidelines in this toolkit created by The CREATE Initiative partners comes out of the question: “What does it look like to envision green spaces as sites through which to build a more equitable and just world?” 
Sharing in the Benefits of a Greening City includes sections including, (1) Concepts ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/03/sharing-in-the-benefits-of-a-greening-city-230x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/03/sharing-in-the-benefits-of-a-greening-city-230x300.jpg"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['urban nature', 'sustainability', 'equity']  
showDate: false
showReadTime: false
---

**The CREATE Initiative**, 2020.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/sharing-in-the-benefits-of-a-greening-city-mar-2020/)

> **Abstract** 
>
> The guidelines in this toolkit created by The CREATE Initiative partners comes out of the question: “What does it look like to envision green spaces as sites through which to build a more equitable and just world?” 
Sharing in the Benefits of a Greening City includes sections including, (1) Concepts and Content, (2) Policy Tools, and (3) From Toolkit to Action as well as a glossary and a wealth of additional resources in the Appendices. 
Individual handouts for topics within each section are available at the toolkit website.


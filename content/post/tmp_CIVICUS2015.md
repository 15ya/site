
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Participatory Governance Toolkit"
date: 2015-01-01
summary: "Participatory governance is embodied in processes that empower citizens to participate in public decision-making, and it has been gaining increasing acceptance as an effective means to tackle ‘democracy deficits’ and improve public accountability.  
Around the world, a growing number of governments  ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['participation', 'governance']  
showDate: false
showReadTime: false
---

**CIVICUS**, 2015.
[DOWNLOAD HERE](https://www.civicus.org/index.php/media-resources/resources/toolkits/611-participatory-governance-toolkit)

> **Abstract** 
>
> Participatory governance is embodied in processes that empower citizens to participate in public decision-making, and it has been gaining increasing acceptance as an effective means to tackle ‘democracy deficits’ and improve public accountability.  
Around the world, a growing number of governments and their partners in civil society are experimenting with innovative practices that seek to expand the space and mechanisms for citizen participation in governance processes beyond elections. There is evidence that participatory governance practices are contributing to stronger government transparency, accountability and responsiveness, and improved public policies and services.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "What About Housing: A Policy Toolkit for Inclusive Growth"
date: 2019-01-01
summary: "Housing is one of the most important solutions to preventing displacement and creating equitable development. As quoted within the document, “Grounded Solutions Network created this toolkit to help communities understand their housing policy options and the approach that will work best for them. Com ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/08/why-housing-grounded-solutions-toolkit-233x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/08/why-housing-grounded-solutions-toolkit-233x300.jpg"
categories: ['local government', 'planning']
tags: ['housing', 'urban development', 'equity']  
showDate: false
showReadTime: false
---

**Grounded Solutions Network**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/what-about-housing-a-policy-toolkit-for-inclusive-growth-aug-2019/)

> **Abstract** 
>
> Housing is one of the most important solutions to preventing displacement and creating equitable development. As quoted within the document, “Grounded Solutions Network created this toolkit to help communities understand their housing policy options and the approach that will work best for them. Community leaders and policymakers can start with local dynamics—their community’s housing situation and the outcomes they want to achieve—and determine which policy tools best suit their needs.” The toolkit includes chapters such as: 
How do we help lower-income families become homeowners and build equity?
How can market activity help generate revenue for affordable housing investments?
How else can we fund affordable housing investments?


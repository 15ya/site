
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Financing Urban Shelter - Global Report on Human Settlements"
date: 2005-01-01
summary: "Financing Urban Shelter presents the first global assessment of housing finance systems, placing shelter and urban development challenges within the overall context of macroeconomic policies. The report describes and analyses housing finance conditions and trends in all regions of the world, includi ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2005/11/GRHS2005.jpg.webp?itok=frEaTl6f"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2005/11/GRHS2005.jpg.webp?itok=frEaTl6f"
categories: ['local government']
tags: ['finance', 'governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2005.
[DOWNLOAD HERE](https://unhabitat.org/financing-urban-shelter-global-report-on-human-settlements)

> **Abstract** 
>
> Financing Urban Shelter presents the first global assessment of housing finance systems, placing shelter and urban development challenges within the overall context of macroeconomic policies. The report describes and analyses housing finance conditions and trends in all regions of the world, including formal housing finance mechanisms, microfinance and community funding, highlighting their relevance to the upgrading of slums. Recent policy developments in the area of shelter finance are discussed at the international and national levels. The report also examines policy directions that could be taken to strengthen shelter finance systems, particularly with respect to realizing the Millennium Declaration target of improving the lives of slum dwellers.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "City Resilience Action Planning Tool (CityRAP)"
date: 2018-01-01
summary: "The CityRAP tool is used for training city managers and municipal technicians in small to intermediate sized cities in sub-Saharan Africa. It enables communities to understand and plan actions aimed at reducing risk and building resilience through the development of a Resilience Framework for Action ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/05/CityRap.JPG.webp?itok=ch4NIbCb"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/05/CityRap.JPG.webp?itok=ch4NIbCb"
categories: ['local government']
tags: ['participation', 'governance', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2018.
[DOWNLOAD HERE](https://unhabitat.org/city-resilience-action-planning-tool-cityrap)

> **Abstract** 
>
> The CityRAP tool is used for training city managers and municipal technicians in small to intermediate sized cities in sub-Saharan Africa. It enables communities to understand and plan actions aimed at reducing risk and building resilience through the development of a Resilience Framework for Action.
The CityRAP tool is a step-by-step participatory resilience planning methodology that includes a set of training exercises and activities targeting municipal authorities, communities and local stakeholders. The implementation of the tool lasts approximately two to three months that are divided into four phases, as described below. A team of external trainers kicks-off the process and supports it throughout each phase, at different levels- sometimes directly on-site and at other times by being available as a remote resource. A small group of at least three people should be trained to lead the process at the city level, hereafter referred as the Municipal Focal Points. They play a very important role as they lead the CityRAP Tool roll-out process, thus collecting data, supporting data analysis, facilitating discussions, ensuring effective communication with partners/stakeholders, actively engaging with communities through participatory approach, and drafting the City RFA.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Locally-led neighbourhoods: A Community-led Placemaking Manual"
date: 2019-01-01
summary: "This step-by-step methodology provides guidance, worksheets, and tips to help your community, council or organization support the implementation of community-led placemaking. This guide outlines the CoDesign Studio framework for how councils and communities can deliver placemaking projects. Step by  ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-Locally-led-neighborhood.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-Locally-led-neighborhood.png"
categories: ['landscape', 'urban design']
tags: ['participation', 'public space']  
showDate: false
showReadTime: false
---

**Co-Design Studio**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/locally-led-neighbourhoods/)

> **Abstract** 
>
> This step-by-step methodology provides guidance, worksheets, and tips to help your community, council or organization support the implementation of community-led placemaking. This guide outlines the CoDesign Studio framework for how councils and communities can deliver placemaking projects. Step by step, it will take you from exploration of community ideas through to implementation and evaluation of the project. The process differs for community members and councils. It is also full of important knowledge and insights for other stakeholders of place such as industry and larger government agencies.


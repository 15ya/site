
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Achieving biodiversity restoration"
date: 2022-01-01
summary: "Biodiversity is the variety of life on Earth in all its forms. It is the key resource upon which all communities and future generations depend, and it underpins the planet’s health. 
Healthy ecosystems are the planet’s life support system, the foundation of societies, economies, food production and  ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/b/biodiversity-cards-cover.jpg?h=239&mw=340&w=340&hash=BE14B6C75E18223AFA14F01CD8A97AB5"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/b/biodiversity-cards-cover.jpg?h=239&mw=340&w=340&hash=BE14B6C75E18223AFA14F01CD8A97AB5"
categories: ['landscape']
tags: ['urban nature', 'sustainability', 'nbs', 'climate change', 'resilience']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/biodiversity-trend-cards)

> **Abstract** 
>
> Biodiversity is the variety of life on Earth in all its forms. It is the key resource upon which all communities and future generations depend, and it underpins the planet’s health. 
Healthy ecosystems are the planet’s life support system, the foundation of societies, economies, food production and human health. The interrelationship between climate change, biodiversity loss and human wellbeing is indisputable. Loss of biodiversity is directly linked to a decrease in the function of ecosystems, and in turn the reduction of the life-sustaining services from nature. 
Our Ecology experts at Arup have created a set of cards “Achieving Biodiversity Restoration” that highlight key challenges but also their potential solutions around the five primary drivers of biodiversity loss: Land & Sea Use Change, Direct Exploitation, Climate Change, Pollution and Invasive Species. 
The cards both reflect the United Nation’s Global Biodiversity Framework and the Sustainable Development Goals; they are a thought-provoking tool to help shape discussion, create awareness and above all, signpost the road to recovery.


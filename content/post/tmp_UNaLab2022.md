
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Nature-based Solutions: Technical Handbook Factsheets"
date: 2022-01-01
summary: "Handbook describing different types of NBS and their technical specifications. ..."
featureImage: "https://unalab.eu/sites/default/files/2022-11/NBSTechnicalHandbook.png"
thumbnail: "https://unalab.eu/sites/default/files/2022-11/NBSTechnicalHandbook.png"
categories: ['architecture', 'landscape', 'urban design']
tags: ['water', 'urban nature', 'sustainability', 'nbs', 'risk reduction', 'resilience', 'streetscape', 'public space', 'climate change', 'infrastructure']  
showDate: false
showReadTime: false
---

**UNaLab**, 2022.
[DOWNLOAD HERE](https://unalab.eu/system/files/2022-11/unalab-nbs-technical-handbook-factsheets2022-11-17.pdf)

> **Abstract** 
>
> Handbook describing different types of NBS and their technical specifications.


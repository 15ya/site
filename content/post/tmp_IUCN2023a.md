
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "IUCN SSC guidelines on human-wildlife conflict and coexistence : first edition"
date: 2023-01-01
summary: "As human-wildlife conflicts become more frequent, serious and widespread worldwide, they are notoriously challenging to resolve, and many efforts to address these conflicts struggle to make progress. These Guidelines provide an essential guide to understanding and resolving human-wildlife conflict.  ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2023-009-En.png?itok=sspqR_sY"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2023-009-En.png?itok=sspqR_sY"
categories: ['planning', 'local government']
tags: ['urban nature']  
showDate: false
showReadTime: false
---

**IUCN**, 2023.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/50756)

> **Abstract** 
>
> As human-wildlife conflicts become more frequent, serious and widespread worldwide, they are notoriously challenging to resolve, and many efforts to address these conflicts struggle to make progress. These Guidelines provide an essential guide to understanding and resolving human-wildlife conflict. The Guidelines aim to provide foundations and principles for good practice, with clear, practical guidance on how best to tackle conflicts and enable coexistence with wildlife. They have been developed for use by conservation practitioners, community leaders, decision-makers, researchers, government officers and others. Focusing on approaches and tools for analysis and decision-making, they are not limited to any particular species or region of the world.


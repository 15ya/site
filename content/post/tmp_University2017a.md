
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for Urban Labs"
date: 2017-01-01
summary: "The Urban Lab Kit guidelines are intended for team members and managers of urban labs and, more generally, for civil servants and facilitators in cities working with experimental urban governance processes to tackle complex challenges. They aim to support the everyday practice of collaboratively exp ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'urban design']
tags: ['governance', 'participation']  
showDate: false
showReadTime: false
---

**Maastricht University**, 2017.
[DOWNLOAD HERE](https://www.maastrichtuniversity.nl/research/msi/research-output/guidelines-urban-labs)

> **Abstract** 
>
> The Urban Lab Kit guidelines are intended for team members and managers of urban labs and, more generally, for civil servants and facilitators in cities working with experimental urban governance processes to tackle complex challenges. They aim to support the everyday practice of collaboratively experimenting and learning how to create more sustainable and inclusive cities. 
These guidelines do not provide a single definitive answer on ways to organize and run an urban lab or its experimental activities, but rather they offer, through frameworks and examples, guidance for ways to act in relation to, and reflect on, key issues. 
It is the outcome of a collaborative effort between researchers and practitioners and has been tested with various potential user groups to improve the final version. Overall, the LAB kit consists of three types of questions to be answered in a single day interactive workshop.


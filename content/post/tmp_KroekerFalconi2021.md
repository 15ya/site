
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Compendium of Best Practices for Housing in Africa"
date: 2021-01-01
summary: "This compendium from Cities Alliance's member, Habitat for Humanity International (HFHI), documents positive stories of change in the areas of adequate and affordable housing, including basic services, land tenure and slum upgrading in Africa. ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning', 'architecture']
tags: ['housing', 'land use', 'informality', 'governance', 'water', 'infrastructure', 'services']  
showDate: false
showReadTime: false
---

**Habitat for Humanity International**, 2021.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/global-knowledge/compendium-best-practices-slum-upgrading)

> **Abstract** 
>
> This compendium from Cities Alliance's member, Habitat for Humanity International (HFHI), documents positive stories of change in the areas of adequate and affordable housing, including basic services, land tenure and slum upgrading in Africa.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Developing Local Climate Change Plans"
date: 2012-01-01
summary: "This tool provides local policy-makers and major stakeholders with a methodology to plan for climate change. These plans must address both mitigation (e.g., reducing the concentration of greenhouse gases in the atmosphere) and adaptation (responding to the impacts of climate change). If they are to  ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Developing-Local-Climate-Change-Plans-1.jpg.webp?itok=JEedqxQX"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Developing-Local-Climate-Change-Plans-1.jpg.webp?itok=JEedqxQX"
categories: ['local government', 'planning']
tags: ['governance', 'sustainability', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2012.
[DOWNLOAD HERE](https://unhabitat.org/developing-local-climate-change-plans)

> **Abstract** 
>
> This tool provides local policy-makers and major stakeholders with a methodology to plan for climate change. These plans must address both mitigation (e.g., reducing the concentration of greenhouse gases in the atmosphere) and adaptation (responding to the impacts of climate change). If they are to be effective, local plans for climate change (both adaptation and mitigation) require the involvement of a variety of stakeholders and a specific focus on the most vulnerable groups.


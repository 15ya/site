
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Ecological restoration for protected areas : principles, guidelines and best practices"
date: 2012-01-01
summary: "This publication provides guidance for terrestrial, marine, and freshwater protected area managers at both system and site levels on the restoration of natural and associated values of protected areas. As this sometimes necessitates restoration beyond protected area borders (e.g., to address ecosyst ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-018.jpg?itok=kEoXYvMO"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-018.jpg?itok=kEoXYvMO"
categories: ['local government', 'landscape']
tags: ['water', 'urban nature', 'conservation']  
showDate: false
showReadTime: false
---

**IUCN**, 2012.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/10205)

> **Abstract** 
>
> This publication provides guidance for terrestrial, marine, and freshwater protected area managers at both system and site levels on the restoration of natural and associated values of protected areas. As this sometimes necessitates restoration beyond protected area borders (e.g., to address ecosystem fragmentation and maintain well-connected protected area systems), this guide uses the term "restoration for protected areas" for activities within protected areas and for activities in connecting or surrounding lands and waters that influence protected area values. It provides information on principles and best practice, with examples, and advice on the process of restoration, but is not a comprehensive restoration manual and does not give detailed methodologies and techniques.


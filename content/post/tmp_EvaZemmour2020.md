
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Suitcase"
date: 2020-01-01
summary: "Behind the enigmatic term “the suitcase” lies a simple DIY (Do It Yourself) concept : an easily transportable, attractive and readable ensemble that enables the visualisation of a project, evolves with it, and facilitates the involvement of people (co-makers, stakeholders) in the project’s developme ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_The-Suitcase.png"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_The-Suitcase.png"
categories: ['planning', 'urban design']
tags: ['participation']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2020.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/suitcase/)

> **Abstract** 
>
> Behind the enigmatic term “the suitcase” lies a simple DIY (Do It Yourself) concept : an easily transportable, attractive and readable ensemble that enables the visualisation of a project, evolves with it, and facilitates the involvement of people (co-makers, stakeholders) in the project’s development. All of this, in nothing more than a box with handles. To be more accurate, we should rather say the suitcaseS. As a matter of fact, there is not one ultimate suitcase. There is a potential infinity of suitcases to be built, unique for each project. It is a concept and blueprint of a suitcase, like a box that can be filled and designed according to the needs, capacity and goals of a particular project. It is a versatile tool that can be at the same time a branding showcase for your project, or can be turned into a participatory mapping tool where communities can be involved in the design phase of the project.


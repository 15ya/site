
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Principles for delivering urban Nature-based Solutions"
date: 2021-01-01
summary: "This report aims to enable more ambitious targets related to NBS, climate resilience and ENG, and ultimately increase the application of NBS, both wild and cultivated, in urban areas. To achieve this, the report sets out six principles to assist organisations and individuals in the design, delivery, ..."
featureImage: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/Capture_80.PNG?itok=KLafr_Yl"
thumbnail: "https://networknature.eu/sites/default/files/styles/flexslider_enhanced/public/images/products/Capture_80.PNG?itok=KLafr_Yl"
categories: ['urban design', 'landscape', 'planning', 'local government']
tags: ['urban nature', 'water', 'sustainability', 'governance', 'nbs']  
showDate: false
showReadTime: false
---

**GBC**, 2021.
[DOWNLOAD HERE](https://networknature.eu/product/26381)

> **Abstract** 
>
> This report aims to enable more ambitious targets related to NBS, climate resilience and ENG, and ultimately increase the application of NBS, both wild and cultivated, in urban areas. To achieve this, the report sets out six principles to assist organisations and individuals in the design, delivery, and operation of urban NBS, along with the methods that can be used to achieve them, and case studies of real-world application. Ranging from strategic inception to considerations for short-term funding, long- term management and future research and innovation, these principles aim to provide an overview of methods that can be utilised to further drive the consideration of NBS both conceptually and practically within a range of urban development contexts.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Municipal Governance for Nature-Based Solutions: Executive Summary"
date: 2020-01-01
summary: "This executive summary of the UNaLab municipal governance guidelines provides an overview of the guidelines, touching on the key findings and takeaways from the report. ..."
featureImage: "https://unalab.eu/sites/default/files/2022-11/Municipal%20governance%20for%20nature-based%20solutions_2022-11-30_4061.png"
thumbnail: "https://unalab.eu/sites/default/files/2022-11/Municipal%20governance%20for%20nature-based%20solutions_2022-11-30_4061.png"
categories: ['local government']
tags: ['water', 'urban nature', 'sustainability', 'governance', 'nbs', 'infrastructure', 'climate change']  
showDate: false
showReadTime: false
---

**UNaLab**, 2020.
[DOWNLOAD HERE](https://unalab.eu/en/documents/municipal-governance-nature-based-solutions)

> **Abstract** 
>
> This executive summary of the UNaLab municipal governance guidelines provides an overview of the guidelines, touching on the key findings and takeaways from the report.


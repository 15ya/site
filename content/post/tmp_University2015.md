
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Multi-Stakeholder Partnerships Guide"
date: 2015-01-01
summary: "The guide links the underlying rationale for multi-stakeholder partnerships, with a clear four phase process model, a set of seven core principles, key ideas for facilitation and 60 participatory tools for analysis, planning and decision making.  
The guide has been written for those directly involv ..."
featureImage: "https://mspguideorg.files.wordpress.com/2021/12/guide-1.png"
thumbnail: "https://mspguideorg.files.wordpress.com/2021/12/guide-1.png"
categories: ['local government', 'planning', 'urban design']
tags: ['participation', 'governance']  
showDate: false
showReadTime: false
---

**Wageningen University**, 2015.
[DOWNLOAD HERE](https://mspguide.org/the-msp-guide/)

> **Abstract** 
>
> The guide links the underlying rationale for multi-stakeholder partnerships, with a clear four phase process model, a set of seven core principles, key ideas for facilitation and 60 participatory tools for analysis, planning and decision making.  
The guide has been written for those directly involved in MSPs – as a stakeholder, leader, facilitator or funder – to provide both the conceptual foundations and practical tools that underpin successful partnerships. 
What’s inside draws on the direct experience of staff from CDI in supporting MSP processes in many countries around the world. The guide also compiles the ideas and materials behind CDI’s annual three week international course on facilitating MSPs and social learning. 
This work has been inspired by the motivation and passion that comes when people dare to “walk in each other’s shoes” to find new paths toward shared ambitions for the future.


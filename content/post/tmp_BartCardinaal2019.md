
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Place Led Development – Creating vibrant communities & human scale areas"
date: 2019-01-01
summary: "Although placemaking and real estate development often seem to be categorised in separate worlds, or even labeled as adversaries, we believe they should not be. Placemaking should be intertwined into the real estate development process. This is what over the last decade we started to call “Place Led ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'landscape', 'urban design']
tags: ['public space', 'streetscape', 'participation']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/place-led-development/)

> **Abstract** 
>
> Although placemaking and real estate development often seem to be categorised in separate worlds, or even labeled as adversaries, we believe they should not be. Placemaking should be intertwined into the real estate development process. This is what over the last decade we started to call “Place Led Development”. Place led development transforms placemaking from one time/temporary interventions into more structural and fundamental urban development.


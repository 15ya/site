
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Transforming Existing Hotels to Net Zero Carbon"
date: 2021-01-01
summary: "Approximately 80% of the buildings we’ll be using in 2050 already exist. And with hotels responsible for approximately 1% of global greenhouse gas emissions, it’s therefore essential that decarbonising our existing hotel stock is part of the action we take to achieve net zero carbon and help avert c ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/n/net-zero-carbon-hotels/transforming-existing-hotels-to-net-zero-carbon-cover.jpg?h=481&mw=340&w=340&hash=B6F5E16F9FA0F698575B7A022F211EDF"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/n/net-zero-carbon-hotels/transforming-existing-hotels-to-net-zero-carbon-cover.jpg?h=481&mw=340&w=340&hash=B6F5E16F9FA0F698575B7A022F211EDF"
categories: ['architecture']
tags: ['energy', 'resilience', 'housing']  
showDate: false
showReadTime: false
---

**ARUP**, 2021.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/transforming-existing-hotels-to-net-zero-carbon)

> **Abstract** 
>
> Approximately 80% of the buildings we’ll be using in 2050 already exist. And with hotels responsible for approximately 1% of global greenhouse gas emissions, it’s therefore essential that decarbonising our existing hotel stock is part of the action we take to achieve net zero carbon and help avert climate catastrophe. 
We were able to capitalise on the unique opportunity presented by a year of lockdowns, examining a typical UK business/leisure hotel’s carbon footprint with negligible human influence. This has enabled in-depth analysis that will improve the industry’s understanding of how we can transform existing hotels to be net zero. 
As well as examining the carbon impact of potential interventions, the paper also looks at cost, refurbishment cycle and materials/systems interdependencies. The result is a route map for hotels to achieve net zero carbon that plots the optimal phasing of steps. This includes changes to operations that reduce energy consumption, passive measures such as improving the thermal performance of the building, active measures that improve the efficiency of systems, and generating renewable energy on site. 
Setting this within the broader industry context – including legislative change and the value customers place on sustainability – has enabled the risks of creating stranded assets to be surfaced and the asset value case for net zero carbon investment to be made.


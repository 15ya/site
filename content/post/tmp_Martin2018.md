
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "From Projects to Portfolios: Mainstreaming Large-Scale Investment in Integrated Infrastructure"
date: 2018-01-01
summary: "In order to get green infrastructure integration and implementation to the scale needed to maximize its many economic, social, and environmental benefits, we will need to rethink not only how government delivers services, but how communities receive and benefit from them. In this Blueprint for Incre ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/03/Earth_Economics-232x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/03/Earth_Economics-232x300.jpg"
categories: ['local government']
tags: ['finance', 'land use', 'infrastructure']  
showDate: false
showReadTime: false
---

**Earth Economics**, 2018.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/from-projects-to-portfolios-mainstreaming-large-scale-investment-in-integrated-infrastructure-2019/)

> **Abstract** 
>
> In order to get green infrastructure integration and implementation to the scale needed to maximize its many economic, social, and environmental benefits, we will need to rethink not only how government delivers services, but how communities receive and benefit from them. In this Blueprint for Increased Investment in Integrated Infrastructure, Earth Economics presents five major cultural and institutional shifts required at the societal level along with a comprehensive set of actions that can be taken immediately at the municipal level.


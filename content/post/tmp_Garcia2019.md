
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Conservation Toolkit: A Guide to Land, Water and Climate Issues and the Impact on Latino Communities"
date: 2019-01-01
summary: "The report examines the importance of environmental issues–like public lands, land and water conservation, climate change, heat and drought, and wildfires–for Latino voters. Each section includes an explanation of the environmental issue and why it is important to Latino communities, including infor ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/09/HAF-toolkit-233x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/09/HAF-toolkit-233x300.jpg"
categories: ['planning']
tags: ['urban nature', 'governance', 'climate change', 'equity', 'conservation', 'water', 'land use']  
showDate: false
showReadTime: false
---

**Hispanic Access Foundation**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/conservation-toolkit-a-guide-to-land-water-and-climate-issues-and-the-impact-on-latino-communities-sept-2019/)

> **Abstract** 
>
> The report examines the importance of environmental issues–like public lands, land and water conservation, climate change, heat and drought, and wildfires–for Latino voters. Each section includes an explanation of the environmental issue and why it is important to Latino communities, including information on access and health, cultural heritage and history, jobs and economic impacts, and public opinion. The guide serves as a resource to Latino voters, who demonstrate overwhelming concerns for the environment.


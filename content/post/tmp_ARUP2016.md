
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Cities Alive: Green Building Envelope"
date: 2016-01-01
summary: "Through a multi-disciplinary approach, experts from eight skill networks around the world cross-examine how green building envelopes can reduce energy consumption, improve air quality and the health and well-being of city inhabitant’s. ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/c/cities_alive_green_building_envelope.jpg?h=482&mw=340&w=340&hash=BD3E4877201C98B1FB3612324D2E0482"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/c/cities_alive_green_building_envelope.jpg?h=482&mw=340&w=340&hash=BD3E4877201C98B1FB3612324D2E0482"
categories: ['architecture', 'landscape', 'urban design']
tags: ['urban nature', 'infrastructure', 'health', 'energy', 'nbs', 'sustainability']  
showDate: false
showReadTime: false
---

**ARUP**, 2016.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/cities-alive-green-building-envelope)

> **Abstract** 
>
> Through a multi-disciplinary approach, experts from eight skill networks around the world cross-examine how green building envelopes can reduce energy consumption, improve air quality and the health and well-being of city inhabitant’s.


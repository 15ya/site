
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Creating a digital communal dinner"
date: 2020-01-01
summary: "In 2019 CREWS invited all residents of Oslo to join their communal dinner with Oslo’s longest dining table. They served dinner made from ingredients harvested in the park and other urban gardens in Oslo. In 2020, due to the Covid-19 pandemic, they developed a concept to create a free digital communa ..."
featureImage: "https://nabolagshager.files.wordpress.com/2020/12/cover-digital-communal-dinner.jpg?w=486"
thumbnail: "https://nabolagshager.files.wordpress.com/2020/12/cover-digital-communal-dinner.jpg?w=486"
categories: ['urban design']
tags: ['participation']  
showDate: false
showReadTime: false
---

**CREWS**, 2020.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/digital-communal-dinner/)

> **Abstract** 
>
> In 2019 CREWS invited all residents of Oslo to join their communal dinner with Oslo’s longest dining table. They served dinner made from ingredients harvested in the park and other urban gardens in Oslo. In 2020, due to the Covid-19 pandemic, they developed a concept to create a free digital communal dinner inviting people to receive a bag of locally grown vegetables, make soup at home, and post pictures to social media using the hashtag #OslosLengsteLangbord. The tool will walk you through all steps of how to create your digital communal dinner with locally grown vegetables. It’s a Covid-19 proof placemaking tool that brings some joy into your community.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Reclaiming Play in Cities"
date: 2020-01-01
summary: "Reclaiming Play in Cities was developed by Arup and the LEGO Foundation for the Real Play Coalition, and reviews the evidence around learning through play and the impact that urban environments have on children’s access to play and ultimately, their overall development. ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/r/real-play-cover.jpg?h=487&mw=340&w=340&hash=7D00546FFB053DF1DB4BB5B200F28B3B"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/r/real-play-cover.jpg?h=487&mw=340&w=340&hash=7D00546FFB053DF1DB4BB5B200F28B3B"
categories: ['planning', 'landscape', 'urban design']
tags: ['age', 'streetscape', 'public space', 'urban nature', 'participation', 'health']  
showDate: false
showReadTime: false
---

**ARUP**, 2020.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/reclaiming-play-in-cities)

> **Abstract** 
>
> Reclaiming Play in Cities was developed by Arup and the LEGO Foundation for the Real Play Coalition, and reviews the evidence around learning through play and the impact that urban environments have on children’s access to play and ultimately, their overall development.


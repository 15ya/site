
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Addressing Urban and Human Settlement Issues in National Adaptation Plans"
date: 2019-01-01
summary: "We live in an urban world: more than 55 per cent of the world population lives in urban areas today and this number will grow to 68 per cent by 2050. Cities are particularly vulnerable to Climate Change as the concentrate large populations and a centres for the national economy and social-economic d ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/04/NAP-HS.jpg.webp?itok=dwrH5BwC"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/04/NAP-HS.jpg.webp?itok=dwrH5BwC"
categories: ['local government', 'planning']
tags: ['climate change', 'sustainability']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2019.
[DOWNLOAD HERE](https://unhabitat.org/nap-human-settlement)

> **Abstract** 
>
> We live in an urban world: more than 55 per cent of the world population lives in urban areas today and this number will grow to 68 per cent by 2050. Cities are particularly vulnerable to Climate Change as the concentrate large populations and a centres for the national economy and social-economic development. In order to build the climate resilience of the national population and economy, building resilient cities and human settlements is essential. National Adaptation Plans are the most important process to articulate the adaptation needs and priorities of countries, and therefore countries should comprehensively address urban and human settlement issues in National Adaptation Plans (NAPs) at the formulation and implementation stage.
The guide primarily targets decision-makers at the national level working on NAPs, both within and outside UNFCCC focal point ministries, while it also targets a broader set of stakeholders at the national and sub-national levels who are interested in NAPs or who may be involved in their implementation. This guide has been developed to address the support countries require to enhance the coverage of human settlement/urban issues within the broader national effort to formulate and implement NAPs. The supplement also offers advice on how adaptation efforts at the urban level can be scaled up and better integrated into national efforts.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Block by Block Playbook: Using Minecraft as a participatory design tool in urban design and governance"
date: 2021-01-01
summary: "The purpose of this playbook is to outline UN-Habitat’s approach to using Minecraft as an enabler to encourage community participation in urban design and governance.
UN-Habitat has integrated the Block by Block methodology in its Global Public Space Programme, a programme launched in 2012 to develo ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/cover.jpg.webp?itok=xcL-uM2t"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/cover.jpg.webp?itok=xcL-uM2t"
categories: ['landscape', 'planning', 'architecture', 'urban design']
tags: ['governance', 'participation']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/the-block-by-block-playbook-using-minecraft-as-a-participatory-design-tool-in-urban-design-and)

> **Abstract** 
>
> The purpose of this playbook is to outline UN-Habitat’s approach to using Minecraft as an enabler to encourage community participation in urban design and governance.
UN-Habitat has integrated the Block by Block methodology in its Global Public Space Programme, a programme launched in 2012 to develop local policies, plans and designs for safe, inclusive, and accessible public spaces for all to support cities to become more compact, integrated, connected, socially inclusive, and resilient to climate change. The Programme provides technical support in developing city-wide public space strategies, conducting participatory design/visioning workshops, setting up public space management frameworks, conducting city-wide public space assessments, developing indicators to monitor implementation and assess impact, amongst others.


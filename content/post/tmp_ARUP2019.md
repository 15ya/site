
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Cities Alive: Designing for ageing communities"
date: 2019-01-01
summary: "The ageing of the world population will be one of the defining megatrends of this century. These changes will have a profound effect on our cities and the lives of the people who live in them, creating new challenges and also opening up new opportunities. 
Cities Alive: Designing for Ageing Communit ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/d/ageingcommunitiescover.jpg?h=482&mw=340&w=340&hash=C482E043BE3D1316F95D2FE5BE57CA82"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/d/ageingcommunitiescover.jpg?h=482&mw=340&w=340&hash=C482E043BE3D1316F95D2FE5BE57CA82"
categories: ['landscape', 'urban design']
tags: ['age', 'streetscape', 'infrastructure', 'public space', 'mobility', 'urban nature', 'health', 'housing']  
showDate: false
showReadTime: false
---

**ARUP**, 2019.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/cities-alive-designing-for-ageing-communities)

> **Abstract** 
>
> The ageing of the world population will be one of the defining megatrends of this century. These changes will have a profound effect on our cities and the lives of the people who live in them, creating new challenges and also opening up new opportunities. 
Cities Alive: Designing for Ageing Communities identifies the specific needs of older people and proposes strategies and actions that cities can take to make communities more age-friendly. The report synthesises these strategies into a vision for the future, showing how communities around the word can achieve this vision and empower their older residents to live happy and fulfilling lives.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Social Value Toolkit for Architecture"
date: 2020-01-01
summary: "The Social Value Toolkit for Architecture has been developed to make it simple to evaluate and demonstrate the social impact of design on people and communities. 
Social value outcomes are increasingly being considered necessary benefits in public and private procurement through quality scores of bi ..."
featureImage: "https://kb.goodhomes.org.uk/wp-content/uploads/2020/07/RIBAUoR-Social-Value-Toolkit-2020pdf-1-1200x1697.jpg"
thumbnail: "https://kb.goodhomes.org.uk/wp-content/uploads/2020/07/RIBAUoR-Social-Value-Toolkit-2020pdf-1-1200x1697.jpg"
categories: ['architecture']
tags: ['gender', 'age', 'participation']  
showDate: false
showReadTime: false
---

**RIBA**, 2020.
[DOWNLOAD HERE](https://www.architecture.com/knowledge-and-resources/resources-landing-page/social-value-toolkit-for-architecture)

> **Abstract** 
>
> The Social Value Toolkit for Architecture has been developed to make it simple to evaluate and demonstrate the social impact of design on people and communities. 
Social value outcomes are increasingly being considered necessary benefits in public and private procurement through quality scores of bids and tenders. To provide evidence that meets these key performance targets and metrics, practices need to demonstrate value quantitatively and this toolkit provides a post occupancy evaluation survey and methodology for reporting social value as a financial return on investment. 
The Social Value Toolkit was developed through a research project led by the University of Reading and included representatives from the RIBA and research leaders in architectural practice. Download the guidance below to hear from some of these researchers on how their practice is building social value into their projects and design processes.


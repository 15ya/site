
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Measuring, Reporting, Verification of Urban Low Emission Development"
date: 2016-01-01
summary: "Paris Agreement led onto the beginning of a new era where climate action is accelerated, coordinated and properly ﬁnanced. It is clear that climate action at local level is critical to ensure bottom-up results are achieved, that in turn support accomplishment of international goals and target set in ..."
featureImage: "https://www.thegpsc.org/sites/gpsc/files/iclei_0.png"
thumbnail: "https://www.thegpsc.org/sites/gpsc/files/iclei_0.png"
categories: ['local government']
tags: ['sustainability', 'energy']  
showDate: false
showReadTime: false
---

**ICLEI**, 2016.
[DOWNLOAD HERE](https://www.thegpsc.org/knowledge-products/climate-change/measuring-reporting-verification-mrv-urban-low-emission)

> **Abstract** 
>
> Paris Agreement led onto the beginning of a new era where climate action is accelerated, coordinated and properly ﬁnanced. It is clear that climate action at local level is critical to ensure bottom-up results are achieved, that in turn support accomplishment of international goals and target set in Paris Agreement.  Yet we also know that local governments cannot do this alone, they need support and engagement from all levels of government. Thus, this is the perfect time to (re)explore effective vertical and horizontal integration between all levels of government, discovering together how to better coordinate, communicate, plan, implement, monitor and report with and to each other. This directly connects to the process for Measuring, Reporting, Veriﬁcation (MRV) of local climate action, as outlined in this handbook. This handbook, GreenClimateCities MRV guidance, can freely be used by local governments in any country, helping to track developments – ideally easily docking onto national processes and reporting. This handbook aims to guide the local government’s approach to effectively address climate change, and also for integrated sustainable development, stimulating the local green economy, and many other topics, using climate action as an entry point.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "An activity trail: Encouraging exploration and play in underused public spaces"
date: 2020-01-01
summary: "At Nabolagshager we created a fun and engaging tool that is winter and pandemic proof. The manual on how to create an activity trail aims at creating a place people can be active and playful in. It’s a tool to engage people in exploring a place and was developed under the Covid-19 pandemic. It descr ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_Encouraging_exploration_play_underused_spaces-1024x497.jpg"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_Encouraging_exploration_play_underused_spaces-1024x497.jpg"
categories: ['landscape', 'urban design']
tags: ['public space', 'urban nature', 'participation']  
showDate: false
showReadTime: false
---

**Nabolagshager**, 2020.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/activity-trail/)

> **Abstract** 
>
> At Nabolagshager we created a fun and engaging tool that is winter and pandemic proof. The manual on how to create an activity trail aims at creating a place people can be active and playful in. It’s a tool to engage people in exploring a place and was developed under the Covid-19 pandemic. It describes all the steps it takes for you to realize your activity trail in your neighbourhood. It also includes some background on why Nabolagshager and Oslo Living Lab decided on developing the tool in Oslo.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Co-creating Climate Smart Cities: A Practical Guide"
date: 2018-01-01
summary: "Co-creating Climate Smart Cities: A Practical Guide ..."
featureImage: "https://www.smart-city-dialog.de/wp-content/uploads/2021/03/2020_GIZ_ICT-A_Co-Creating_Climate_Smart_Cities_Practical_Guide-787x1024.jpg"
thumbnail: "https://www.smart-city-dialog.de/wp-content/uploads/2021/03/2020_GIZ_ICT-A_Co-Creating_Climate_Smart_Cities_Practical_Guide-787x1024.jpg"
categories: ['planning', 'local government']
tags: ['data', 'participation']  
showDate: false
showReadTime: false
---

**GIZ**, 2018.
[DOWNLOAD HERE](https://www.giz.de/en/downloads/2020_GIZ_ICT-A_Co-Creating_Climate_Smart_Cities_Practical_Guide_web.pdf)

> **Abstract** 
>
> Co-creating Climate Smart Cities: A Practical Guide


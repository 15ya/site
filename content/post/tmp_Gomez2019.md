
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Playful neighbourhood process methodology"
date: 2019-01-01
summary: "Cities are developing and implementing new methods that involve citizens in the creation of urban spaces, but more radically, emphasis has been put on participation throughout the entire process: from the generation of ideas, through design all the way to implementation. The methodology highlights t ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'urban design']
tags: ['participation']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/playful-neighbourhood/)

> **Abstract** 
>
> Cities are developing and implementing new methods that involve citizens in the creation of urban spaces, but more radically, emphasis has been put on participation throughout the entire process: from the generation of ideas, through design all the way to implementation. The methodology highlights tactics and strategies of Playful Actions that allow people to make decisions about their cities, while also inspiring greater interest through inclusive recreational actions in the public space. This approach, originally published in the Our City? book, is both a tool and an example of how to implement the various steps and what the results were. From neighbourhood diagnosis to implementation, the tool offers short and sweet keypoints to be aware of.


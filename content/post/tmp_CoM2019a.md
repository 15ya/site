
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Sustainable Energy Access and Climate Action Plan (SEACAP)"
date: 2019-01-01
summary: "The European Commission’s Joint Research Centre (JRC) has developed a guidebook entitled “How to develop a Sustainable Energy Access and Climate Action Plan (SEACAP) in Sub-Saharan Africa” as a tool for CoM SSA signatories. 
CoM SSA signatories commit to produce and implement a strategic and operati ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['governance', 'sustainability', 'energy', 'climate change']  
showDate: false
showReadTime: false
---

**CoM SSA**, 2019.
[DOWNLOAD HERE](https://comssa.org/en/guidebook)

> **Abstract** 
>
> The European Commission’s Joint Research Centre (JRC) has developed a guidebook entitled “How to develop a Sustainable Energy Access and Climate Action Plan (SEACAP) in Sub-Saharan Africa” as a tool for CoM SSA signatories. 
CoM SSA signatories commit to produce and implement a strategic and operational document called a Sustainable Energy Access and Climate Action Plan (SEACAP). The guidebook provides directions and guidance to local governments on the steps to follow when developing their SEACAP.


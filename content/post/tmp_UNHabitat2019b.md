
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban-rural linkages"
date: 2019-01-01
summary: "The goal of these Guiding Principles is to inform pragmatic strategies and propose a Framework for Action to build an enabling environment for more inclusive and functional urban-rural linkages. The principles are flexible and can be applied by all levels of stakeholders at all scales. 
While the pr ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/05/cover_2.png.webp?itok=FJ3Lr1pv"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/05/cover_2.png.webp?itok=FJ3Lr1pv"
categories: ['local government', 'planning']
tags: ['urban nature', 'sustainability', 'land use', 'urban development', 'mobility', 'governance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2019.
[DOWNLOAD HERE](https://unhabitat.org/urban-rural-linkages-guiding-principles)

> **Abstract** 
>
> The goal of these Guiding Principles is to inform pragmatic strategies and propose a Framework for Action to build an enabling environment for more inclusive and functional urban-rural linkages. The principles are flexible and can be applied by all levels of stakeholders at all scales. 
While the principles are designed for universal application, there are distinct roles and actions appropriate for national or local governments, civil society, the private sector and international organizations. In addition, they can be applied in varying national contexts; for example, where there is a concern about the rate of urbanization and rural transformation, or the degree of diversity in the population. 
The Guiding Principles are to help address the complexity of aligning different levels of governance (national, territorial and local) while recognizing unique local contexts and multiple possibilities for implementation. Urban-rural linkages that advance integrated territorial development are not only about a collection of separate subnational regions, but are also about systems of cities at national and even across national to regional levels.


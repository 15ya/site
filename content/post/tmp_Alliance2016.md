
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "One Water Roadmap: The Sustainable Management of Life’s Most Essential Resource"
date: 2016-01-01
summary: "“The idea of an integrated systems approach to water is not new. Its full-scale implementation, however, has yet to be realized. There are many signs that water management in the US is entering another great era of change and innovation. All around the country we are seeing silo-busting examples of  ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2017/07/Roadmap2017-300x194.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2017/07/Roadmap2017-300x194.png"
categories: ['local government']
tags: ['water', 'governance', 'climate change', 'equity']  
showDate: false
showReadTime: false
---

**US Water Alliance**, 2016.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/water-alliance-one-water-roadmap-sustainable-management2017/)

> **Abstract** 
>
> “The idea of an integrated systems approach to water is not new. Its full-scale implementation, however, has yet to be realized. There are many signs that water management in the US is entering another great era of change and innovation. All around the country we are seeing silo-busting examples of integrated and inclusive approaches to water resource management. These approaches exemplify the view that all water has value and should be managed in a sustainable, inclusive, integrated way. We call this perspective One Water. And while our focus is water, our goals are thriving local economies, community vitality, and healthy ecosystems.”



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Facade Garden Manual"
date: 2021-01-01
summary: "By placing plants, flowerpots and even benches in front of your house, you claim your space in public. This place is still public, but you create a small area that will improve the transition between public and private space. This is a hybrid space. This space creates a smooth transition between pri ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'architecture']
tags: ['urban nature', 'sustainability', 'participation', 'streetscape', 'public space', 'nbs']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2021.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/facade-garden/)

> **Abstract** 
>
> By placing plants, flowerpots and even benches in front of your house, you claim your space in public. This place is still public, but you create a small area that will improve the transition between public and private space. This is a hybrid space. This space creates a smooth transition between private property and the public. A good hybrid zone removes the hard edges and creates opportunities to interact. More and more cities are starting to realise the value and impact in improving hybrid spaces on a larger scale. Municipalities – usually one of the main stakeholders of this process – have discovered that when you empower a community to take ownership of their public space, sustainable, social and human scale improvements happen. In The Netherlands, many municipalities and organisations have tried to come up with initiatives and policies to support and regulate the concept of the facade garden.


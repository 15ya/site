
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Her4Climate"
date: 2022-01-01
summary: "A women-centered tool for assessing responses to climate impacts in cities. 
Her4Climate is a participatory assessment tool developed by Cities Alliance in collaboration with  Arup to mainstream gender considerations in climate adaptation. Designed primarily for international development practitione ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/h/her4climate-tool-cover.jpg?h=481&mw=340&w=340&hash=3E7B34CED59DA72D2329DB45CB624256"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/h/her4climate-tool-cover.jpg?h=481&mw=340&w=340&hash=3E7B34CED59DA72D2329DB45CB624256"
categories: ['planning', 'local government', 'urban design']
tags: ['gender', 'climate change', 'sustainability', 'participation', 'equity']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/her-4-climate)

> **Abstract** 
>
> A women-centered tool for assessing responses to climate impacts in cities. 
Her4Climate is a participatory assessment tool developed by Cities Alliance in collaboration with  Arup to mainstream gender considerations in climate adaptation. Designed primarily for international development practitioners, in-country facilitators, government representatives and urban professionals, this tool is centred on the knowledge and capacities of women in urban areas, providing a framework for understanding women's exposure and their capacity to respond to climate change in urban areas, including  the identification of key climate impacts that require action, and the level of climate adaptability in the future.


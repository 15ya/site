
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Handbook of Sustainable Urban Development Strategies"
date: 2020-01-01
summary: "The Handbook of Sustainable Urban Development Strategies aims to develop methodological support to augment knowledge on how to implement integrated and place-based urban strategies under cohesion policy. In particular, it refers to Sustainable Urban Development (SUD) as supported by the European Reg ..."
featureImage: "https://publications.jrc.ec.europa.eu/repository/cover/JRC118841_cover.jpg"
thumbnail: "https://publications.jrc.ec.europa.eu/repository/cover/JRC118841_cover.jpg"
categories: ['local government', 'planning']
tags: ['governance', 'sustainability', 'urban development']  
showDate: false
showReadTime: false
---

**European Commission**, 2020.
[DOWNLOAD HERE](https://urban.jrc.ec.europa.eu/urbanstrategies/full-report#the-chapter)

> **Abstract** 
>
> The Handbook of Sustainable Urban Development Strategies aims to develop methodological support to augment knowledge on how to implement integrated and place-based urban strategies under cohesion policy. In particular, it refers to Sustainable Urban Development (SUD) as supported by the European Regional Development Fund during the current programming period (2014-2020) and the upcoming one (2021-2027). 
In this context, the Handbook contains recommendations intended to complement official regulations, without being prescriptive. In fact, it is conceived as a policy learning tool, which should be flexible and adaptable to the needs arising from different territorial and administrative contexts. The Handbook addresses SUD strategies as bridges between cohesion policy on the one hand (with its rationale, rules and actors) and local territorial governance systems (with their rationale, rules and actors) on the other.


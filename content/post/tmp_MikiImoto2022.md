
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Yokohama Urban Design Sketchbook : Translating a Community-Led Vision into Practice"
date: 2022-01-01
summary: "Pioneered in Yokohama City, Japan, the Yokohama Urban Design Sketchbook (YUDS) is a citizen engagement and co-creation methodology for urban design at the neighborhood level. It leverages cross-sectional sketches and drawings to translate citizens' visions and ideas of urban areas into concrete prop ..."
featureImage: "https://openknowledge.worldbank.org/server/api/core/bitstreams/9c5cb803-62ec-5435-a687-7b1e888db7d7/content"
thumbnail: "https://openknowledge.worldbank.org/server/api/core/bitstreams/9c5cb803-62ec-5435-a687-7b1e888db7d7/content"
categories: ['planning', 'urban design']
tags: ['participation', 'governance']  
showDate: false
showReadTime: false
---

**World Bank**, 2022.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/37135)

> **Abstract** 
>
> Pioneered in Yokohama City, Japan, the Yokohama Urban Design Sketchbook (YUDS) is a citizen engagement and co-creation methodology for urban design at the neighborhood level. It leverages cross-sectional sketches and drawings to translate citizens' visions and ideas of urban areas into concrete proposals of urban design. YUDS also develops interest among the community on urban areas and it enhances community engagement in broader urban planning and municipal processes. Based on the experience of Yokohama City, the YUDS methodology has been successfully piloted and tested in two distinct urban contexts: in Panama City, Panama, in April 2019, and in Barranquilla, Colombia, in February 2020. The YUDS methodology consists of structured participatory workshops, in which participants collaboratively produce sketches that reflect their urban vision. The uniqueness of the methodology derives from the use of cross-sectional sketches, a simple yet powerful tool that overcomes barriers of communication and encourages consensus among participants regard-less of their language, generation, or social position. The most significant feature of YUDS is the use of schematic representations by the use of cross-sectional sketches. This approach is different from traditional urban design methodologies, which typically rely on photography, maps, and bird's-eye view illustrations.The implementation of the YUDS methodology requires careful planning and dedication of time and resources. Workshops require the preparation of urban design materials and their simplification for nonprofessional participants. The methodology also works best when a municipal champion is committed to the process, and when university researchers and students are closely engaged in the preparation and implementation of each workshop.


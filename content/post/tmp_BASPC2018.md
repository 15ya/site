
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Asphalt Art Guide: How to reclaim city roadways and public infrastructure with art?"
date: 2018-01-01
summary: "Bloomberg Philanthropies’ Asphalt Art Initiative responds to the growing number of cities around the world embracing art as an effective and relatively low-cost strategy to activate their streets. The Asphalt Art Guide documents a wide variety of project types and champions – from formal city-sancti ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_Ashalt_art_Guide-1024x683.jpg"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2022/11/PE_Toolbox_-_Ashalt_art_Guide-1024x683.jpg"
categories: ['landscape', 'urban design']
tags: ['art', 'infrastructure', 'streetscape', 'public space', 'mobility']  
showDate: false
showReadTime: false
---

**Bloomberg Philanthropies**, 2018.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/asphalt-art/)

> **Abstract** 
>
> Bloomberg Philanthropies’ Asphalt Art Initiative responds to the growing number of cities around the world embracing art as an effective and relatively low-cost strategy to activate their streets. The Asphalt Art Guide documents a wide variety of project types and champions – from formal city-sanctioned programs to citizen-driven interventions. It also identifies key considerations, including liability and permitting, community engagement, artist curation and installation methods. This guide includes 26 example projects and tools and tactics on doing “asphalt art”.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Green Values Strategy Guide: Linking Green Infrastructure Benefits to Community Priorities"
date: 2020-01-01
summary: "The Green Values Strategy Guide from the Center for Neighborhood Technology describes how community benefits are related to Green Stormwater Infrastructure, defines different green stormwater infrastructure strategies, and includes information about (1) Health Benefits, (2) Economic Benefits, (3) Cl ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/12/cnt-green-strategies-guide-231x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2020/12/cnt-green-strategies-guide-231x300.png"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['urban nature', 'participation', 'sustainability', 'governance', 'climate change', 'risk reduction']  
showDate: false
showReadTime: false
---

**Center for Neighborhood Technology**, 2020.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/green-values-strategies-guide-linking-green-infrastructure-benefits-to-community-priorities-dec-2020/)

> **Abstract** 
>
> The Green Values Strategy Guide from the Center for Neighborhood Technology describes how community benefits are related to Green Stormwater Infrastructure, defines different green stormwater infrastructure strategies, and includes information about (1) Health Benefits, (2) Economic Benefits, (3) Climate Benefits, and (4) Transportation Benefits. The guide also offers tips for getting started. 
The authors state: “Residents win when local infrastructure investments give equal or higher importance to environmental and societal outcomes, rather than solely prioritizing economic outcomes. Municipal leaders can use this Guide to inform a holistic, triple-bottom line approach to infrastructure improvements in pursuit of healthy, equitable, and resilient communities.”


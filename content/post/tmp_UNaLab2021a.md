
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Handbook on implementation and adoption barriers of Urban Living Labs developing nature-based solutions"
date: 2021-01-01
summary: "Effective collaboration is recognised as essential for the successful adoption of nature-based solutions (NBS) and achieving multifunctionality. Developing NBS requires a flexible governance structure that includes experimentation, learning, reflexivity, and reversibility. Urban Living Labs (ULLs) a ..."
featureImage: "https://unalab.eu/sites/default/files/2021-09/Handbook%20on%20implementation%20and%20adoption%20barriers%20of%20Urban%20Living%20Labs%20developing%20nature-based%20solutions_2021-09-01_3477.png"
thumbnail: "https://unalab.eu/sites/default/files/2021-09/Handbook%20on%20implementation%20and%20adoption%20barriers%20of%20Urban%20Living%20Labs%20developing%20nature-based%20solutions_2021-09-01_3477.png"
categories: ['landscape', 'planning', 'urban design']
tags: ['urban nature', 'sustainability', 'nbs', 'resilience', 'participation', 'governance']  
showDate: false
showReadTime: false
---

**UNaLab**, 2021.
[DOWNLOAD HERE](https://unalab.eu/en/documents/handbook-implementation-and-adoption-barriers-urban-living-labs-developing-nature-based)

> **Abstract** 
>
> Effective collaboration is recognised as essential for the successful adoption of nature-based solutions (NBS) and achieving multifunctionality. Developing NBS requires a flexible governance structure that includes experimentation, learning, reflexivity, and reversibility. Urban Living Labs (ULLs) are introduced as the orchestrators of the collaborations between different stakeholders – including companies, research communities, public sector, and citizens - through co-creation. 
Developing Urban Living Labs is a relatively novel approach in urban areas, and consequently, there are many challenges and barriers that cities face during ULL adoption. This handbook aims to identify and explore some common barriers to the adoption of Urban Living Labs, and provide cities with a tool and an understanding of how to strategically address these barriers.


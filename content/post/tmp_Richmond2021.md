
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "An Analysis of Urban Climate Adaptation Finance"
date: 2021-01-01
summary: "This analysis aims to assess the state of urban climate adaptation finance and to prototype analysis methods to address current data and methodology limitations. The purpose of high-quality urban adaptation finance tracking is to identify gaps and barriers to financing resilience solutions in global ..."
featureImage: "https://d1bf23g64f8xve.cloudfront.net/sites/default/files/styles/research_medium/public/research/An-Analysis-of-Urban-Climate-Adaptation-Finance.png?itok=1vfEXYtE"
thumbnail: "https://d1bf23g64f8xve.cloudfront.net/sites/default/files/styles/research_medium/public/research/An-Analysis-of-Urban-Climate-Adaptation-Finance.png?itok=1vfEXYtE"
categories: ['local government']
tags: ['governance', 'climate change', 'resilience', 'sustainability']  
showDate: false
showReadTime: false
---

**Cities Climate Finance Leadership Alliance**, 2021.
[DOWNLOAD HERE](https://www.climatepolicyinitiative.org/publication/an-analysis-of-urban-climate-adaptation-finance/)

> **Abstract** 
>
> This analysis aims to assess the state of urban climate adaptation finance and to prototype analysis methods to address current data and methodology limitations. The purpose of high-quality urban adaptation finance tracking is to identify gaps and barriers to financing resilience solutions in global urban areas and to drive action by investors, cities, national governments, and other stakeholders to increase urban adaptation finance.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Self-Assessment Tool for Sustainable Urban Development strategies"
date: 2021-01-01
summary: "The Self-Assessment Tool for Sustainable Urban Development strategies (SAT4SUD) is designed for Local Authorities and national and regional Managing Authorities of EU Cohesion Policy, in charge of building or updating sustainable urban development strategies. 
The tool aims to support Local Authorit ..."
featureImage: "https://urban.jrc.ec.europa.eu/sat4sud/img/Strategies-illustration_Banner-SAT4SUD.c21e2c87.svg"
thumbnail: "https://urban.jrc.ec.europa.eu/sat4sud/img/Strategies-illustration_Banner-SAT4SUD.c21e2c87.svg"
categories: ['local government', 'planning']
tags: ['governance', 'infrastructure', 'mobility', 'housing', 'land use', 'urban development']  
showDate: false
showReadTime: false
---

**European Commission**, 2021.
[DOWNLOAD HERE](https://urban.jrc.ec.europa.eu/sat4sud/en)

> **Abstract** 
>
> The Self-Assessment Tool for Sustainable Urban Development strategies (SAT4SUD) is designed for Local Authorities and national and regional Managing Authorities of EU Cohesion Policy, in charge of building or updating sustainable urban development strategies. 
The tool aims to support Local Authorities and relevant actors to assess to what extent the strategy builds on an integrated and participatory approach as set in the New Leipzig Charter and supported in Cohesion Policy 2021-2027. Furthermore, it provides guidance when evaluating the strategy’s completeness and quality, from its design and implementation, to its monitoring and evaluation. The tool can also be used by Managing Authorities when assessing the strategies and providing feedback to Local Authorities during the strategy design and implementation processes. 
The tool focuses at promoting self-assessment as an important learning practice to critically reflect on the strategy, recognise strengths and identify opportunities for improvement. Moreover, the tool can usefully support and provide valuable information for other activities, such as discussion and working sessions between Local Authorities and relevant stakeholders, peer-reviews, benchmarking, dissemination activities as well as external expert involvement (e.g. TAIEX-REGIO PEER 2 PEER exchanges).


---
title: "A Catalogue of Nature-based Solutions for Urban Resilience"
featureImage: "https://openknowledge.worldbank.org/bitstream/handle/10986/36507/A-Catalogue-of-Nature-based-Solutions-for-Urban-Resilience.pdf.jpg?sequence=5&isAllowed=y" # Sets featured image on blog post.
draft: true
thumbnail: "https://openknowledge.worldbank.org/bitstream/handle/10986/36507/A-Catalogue-of-Nature-based-Solutions-for-Urban-Resilience.pdf.jpg?sequence=5&isAllowed=y"
#categories: ['test_cat',]
#tags: ['test_tag', ]
# comment: false # Disable comment if false.
showDate: false
#showShare: false
showReadTime: false
---


**World Bank**, 2021.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/36507)

{{% notice info "Abstract" %}}

This is a standard "note" style.

{{% /notice %}}

> **Abstract** 
>
> Cities worldwide are facing resilience challenges as climate risks interact with urbanization, loss of biodiversity and ecosystem services, poverty, and rising socioeconomic inequality. Extreme precipitation events, flooding, heatwaves, and droughts are causing economic losses, social insecurity, and affecting wellbeing. Over time, urban resilience challenges are expected to grow, driven by processes such as urbanization, land use, and climate change. Whereas climate change is expected to increase the frequency and intensity of some natural hazards, urbanization can also lead to higher exposure of people and assets in cities. More than half of the global population lives in cities, and more than seventy percent are expected to do so by 2050. Nature-based solutions are approaches that use nature and natural processes for delivering infrastructure, services, and integrative solutions to meet the rising challenge of urban resilience. The catalogue of Nature-based solutions for urban resilience has been developed as a guidance document to support the growing demand for NBS by enabling an initial identification of potential investments in nature-based solutions. The document is structured as follows: Chapter 2 describes generic principles for integrating NBS into urban environments. Chapter 3 provides a reader’s guide and holds the Catalogue of the fourteen NBS families.


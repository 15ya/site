
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Asian American and Pacific Islander Anti-Displacement Strategies"
date: 2016-01-01
summary: "This report counters the notion that “gentrification is inevitable” by offering an alternative approach to development, one that directly involves and benefits the community in which the development is taking place. Focusing on the history and statistics of displacement affecting Asian American, Nat ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/04/anti-displacement-strategies-300x196.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/04/anti-displacement-strategies-300x196.png"
categories: ['local government']
tags: ['equity', 'housing', 'governance']  
showDate: false
showReadTime: false
---

**National Coalition for Asian Pacific American Community Development**, 2016.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/anti-displacement-strategies-may2016/)

> **Abstract** 
>
> This report counters the notion that “gentrification is inevitable” by offering an alternative approach to development, one that directly involves and benefits the community in which the development is taking place. Focusing on the history and statistics of displacement affecting Asian American, Native Hawaiian and Pacific Islander communities, the report details strategies that can be applied to a wide range of communities. Case studies from cities throughout the Continental U.S. and Hawaii document various strategies implemented by and with communities of color to invest in humane, equitable development. These strategies include: preventing home foreclosures, preserving farmlands, addressing homelessness, developing city-wide anti-displacement plans, arts-driven place-making, loan programs for small businesses, and revision of landlord-tenant laws. In the Summary of Findings, the report recommends policy measures and best practices that would address displacement issues at the national level, including but not limited to: transit-oriented development, meaningful community engagement, and taking measures to stabilize housing affordability.


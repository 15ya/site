
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "National Urban Policies Driving Public Space Led Urban Development: A Quick Thematic Guide for Mainstreaming Safe, Inclusive and Accessible Public Spaces into National Urban Policies"
date: 2020-01-01
summary: "This document is addressed primarily to policy makers and stakeholders involved in formulating, implementing, monitoring and evaluating National Urban Policy (NUP) and public and private actors undertaking public space activities. It offers guidance on how national policies for urbanization should a ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/National%20Urban%20Policies%20Driving%20Public%20Space%20Led%20Urban%20Development.png.webp?itok=6vIBVysR"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/National%20Urban%20Policies%20Driving%20Public%20Space%20Led%20Urban%20Development.png.webp?itok=6vIBVysR"
categories: ['local government']
tags: ['public space']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/national-urban-policies-driving-public-space-led-urban-development-a-quick-thematic-guide-for)

> **Abstract** 
>
> This document is addressed primarily to policy makers and stakeholders involved in formulating, implementing, monitoring and evaluating National Urban Policy (NUP) and public and private actors undertaking public space activities. It offers guidance on how national policies for urbanization should address public space and strengthen involvement of different stakeholders in that effort, 
UN-Habitat has been mandated by Member states to address the issue of public space and its contribution to sustainable urban development. This includes developing and widely disseminating policy approaches on the role of public spaces in meeting the challenges of the rapidly urbanizing world. The agency’s research has already shown that the most prosperous cities are those that recognize public spaces with proper design layout, and allocate sufficient land to their development. However, public spaces in most urban areas largely remain ignored in national urban policy discourse. Where they exist, they are ambiguous, fragmented and embedded in other policies. Drawing experiences and practices from recent national urban policies and public space programme, this guide demonstrates how to mainstream public and open space into the National Urban Policy (NUP).
The guide is divided into two sections. Section one gives an overview of public space as generators of prosperous cities, the national urban policy process and the need for integrating the two. It also highlights streets as vital ingredient for cities and how their designs and management facilitate sustainable urbanization. Insights on NUPs development process; pillars and principles in support of implementation and monitoring of the Sustainable Development Goals (SDGs), the New Urban Agenda (NUA) among other international frameworks highlighted.
Section two discusses how NUP can support local government policy on public space design, formulation, implementation, monitoring and evaluation strategies through strong leadership and stakeholder participation. Clear recommendations on how to integrate public space in each of the NUP’s development phases: feasibility, diagnosis, formulation, implementation, monitoring and evaluation are given. Further, analysis on how NUP can support national and local capacity development for public space has also been discussed.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Our Communities, Our Power: Advancing Resistance and Resilience in Climate Change Adaptation"
date: 2019-01-01
summary: "This toolkit was created to guide NAACP units through the process of establishing an Environmental and Climate Justice Committee in order to develop a climate adaptation plan. The action toolkit provides 19 modules ranging from Developing a Community Climate Action Plan to Economic Justice and Water ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/06/NAACP-our-communities-our-power-232x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/06/NAACP-our-communities-our-power-232x300.jpg"
categories: ['planning']
tags: ['participation', 'resilience', 'equity', 'climate change', 'sustainability', 'governance']  
showDate: false
showReadTime: false
---

**National Association for the Advancement of Colored People**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/our-communities-our-power-advancing-resistance-and-resilience-in-climate-change-adaptation-june-2019/)

> **Abstract** 
>
> This toolkit was created to guide NAACP units through the process of establishing an Environmental and Climate Justice Committee in order to develop a climate adaptation plan. The action toolkit provides 19 modules ranging from Developing a Community Climate Action Plan to Economic Justice and Water Resources Management. Because communities are different, some modules might apply in one location and not another. The guide is designed so that modules can be used by themselves or together with the other modules.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Planning for City Leaders"
date: 2012-01-01
summary: "Urban Planning for City Leaders is a valuable source of information, inspiration and ideas on urban planning that is designed for city leaders and decision makers at a critical moment in human history. Predicted human population growth over the next 50 years will have immense consequences for all ci ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/UN-Habitat-UPCL-14-02624-Combine.jpg.webp?itok=sVahMuF-"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/UN-Habitat-UPCL-14-02624-Combine.jpg.webp?itok=sVahMuF-"
categories: ['local government', 'planning']
tags: ['infrastructure', 'mobility', 'land use', 'housing', 'governance', 'sustainability', 'services', 'urban development']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2012.
[DOWNLOAD HERE](https://unhabitat.org/urban-planning-for-city-leaders-0)

> **Abstract** 
>
> Urban Planning for City Leaders is a valuable source of information, inspiration and ideas on urban planning that is designed for city leaders and decision makers at a critical moment in human history. Predicted human population growth over the next 50 years will have immense consequences for all cities, in particular intermediate cities with populations of up to two million people. 
Developed countries will need to double the amount of urban space they have by 2050 to accommodate the expected numbers of people, whereas developing countries will need to expand their urban space by more than 300 per cent.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tenure responsive land use planning - A Guide for Country Level Implementation"
date: 2021-01-01
summary: "Tenure Responsive Land Use Planning is a GLTN tool for solving land use planning and tenure security challenges simultaneously. It enables local people to take charge of their development vision in a more participatory, gender-sensitive, and tenure responsive fashion, using practical, local processe ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/10/thumbnail.png.webp?itok=4lGChrkN"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/10/thumbnail.png.webp?itok=4lGChrkN"
categories: ['local government', 'planning']
tags: ['land use', 'informality', 'housing']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/tenure-responsive-land-use-planning-a-practical-guide-for-country-level-intervention)

> **Abstract** 
>
> Tenure Responsive Land Use Planning is a GLTN tool for solving land use planning and tenure security challenges simultaneously. It enables local people to take charge of their development vision in a more participatory, gender-sensitive, and tenure responsive fashion, using practical, local processes and fit-for-purpose approaches to strengthen their knowledge, capacity, and development through land use planning. It offers improved decision-making and resource allocation skills based on coherent, evidence-based planning using relevant land management skills and land information database applications and analysis. 
The Practical Guide explains how to implement tenure-responsive land use planning in conjunction with other tools at the country level and presents the steps involved to ensure that land use planning improves tenure security. Though it is not a blueprint for tenure-responsive land use planning, it outlines procedures that can be adapted to land use and tenure situations in various developing countries.


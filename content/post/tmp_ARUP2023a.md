
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Delivering ambitious decarbonisation targets in the higher education sector"
date: 2023-01-01
summary: "How does the higher education sector move forward to achieve its ambition to deliver successful decarbonisation? The need to decarbonise our towns and cities has never been greater, with the UK Government revising their target to be net zero by 2050. 
Across the UK, universities and other higher edu ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/d/decarbonising-the-higher-education-sector-cover.jpg?h=483&mw=340&w=340&hash=82F489512424884E8B158551852BD332"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/d/decarbonising-the-higher-education-sector-cover.jpg?h=483&mw=340&w=340&hash=82F489512424884E8B158551852BD332"
categories: ['architecture', 'local government']
tags: ['energy', 'housing', 'sustainability']  
showDate: false
showReadTime: false
---

**ARUP**, 2023.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/delivering-ambitious-decarbonisation-targets-in-the-higher-education-sector)

> **Abstract** 
>
> How does the higher education sector move forward to achieve its ambition to deliver successful decarbonisation? The need to decarbonise our towns and cities has never been greater, with the UK Government revising their target to be net zero by 2050. 
Across the UK, universities and other higher education institutions are tackling decarbonisation head on, setting ambitious targets that not only incorporate indirect scope 3 emissions, but include targets that are decades ahead of the UK’s 2050 net zero ambition. 
As global sustainability consultants, our engineers, planners and designers are committed to working with Estates teams across the UK to support the sector transition to net zero, addressing the pressing issues facing universities today, whilst also future proofing to mitigate against the challenges of tomorrow. 
In this report, we bring together our specialist advisory expertise across property, sustainability and decarbonisation to outline a route forward for the sector on the journey towards net zero, turning their commitments into reality.


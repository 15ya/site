
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guideline to Participatory Approach for Disability-Inclusive City"
date: 2018-01-01
summary: "The Guideline to Participatory Approach for Disability-Inclusive City is an educational tool designed to demonstrate the process of participatory data collection for advocacy and policy purposes, to support disability-inclusive cities.
It is a manual that can be used as guidance for city governments ..."
featureImage: "https://kotakita.org/cfind/source/images/kk-test/tools/inclusive-particpatory.jpg"
thumbnail: "https://kotakita.org/cfind/source/images/kk-test/tools/inclusive-particpatory.jpg"
categories: ['local government', 'planning', 'urban design']
tags: ['participation', 'gender', 'age', 'infrastructure', 'public space', 'governance']  
showDate: false
showReadTime: false
---

**Kota Kita**, 2018.
[DOWNLOAD HERE](https://kotakita.org/tools/guideline-to-participatory-approach-for-disability-inclusive-city)

> **Abstract** 
>
> The Guideline to Participatory Approach for Disability-Inclusive City is an educational tool designed to demonstrate the process of participatory data collection for advocacy and policy purposes, to support disability-inclusive cities.
It is a manual that can be used as guidance for city governments, NGOs, or the public in general who wants to create a Disability-Inclusive City Profile and provide options of methodology that can be implemented to collect disability data at a city level. The guideline synthesizes the process, learning, and experience of Kota Kita’s participatory data collection initiative for a Disability-Inclusive City Profile in Solo, Central Java, Indonesia, together with UNESCO. It has also been applied in Banjarmasin, South Kalimantan, Indonesia.
How does this work?
This manual provides step-by-step activities that can be followed by any city looking to create a Disability-Inclusive City Profile. While this process pertains to the one held in Solo, the issues and methods involved are relevant to other cities in Indonesia as well.
Who can use this?
This manual is intended for city governments, planners, activists, civil society organizations, non-governmental organizations, and anyone who has concerns with disability issues and wishes to make their city a more inclusive place.


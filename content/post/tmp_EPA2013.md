
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Sustainable Design and Green Building Toolkit for Local Governments"
date: 2013-01-01
summary: "EPA Region 4 developed the Sustainable Design and Green Building Toolkit (2013) to help local governments identify and remove barriers to sustainable design and green building in their permitting processes. The toolkit can also be used by members of the development community, local government green  ..."
featureImage: ""
thumbnail: ""
categories: ['local government']
tags: ['sustainability', 'housing', 'infrastructure']  
showDate: false
showReadTime: false
---

**EPA**, 2013.
[DOWNLOAD HERE](https://www.epa.gov/smartgrowth/sustainable-design-and-green-building-toolkit-local-governments)

> **Abstract** 
>
> EPA Region 4 developed the Sustainable Design and Green Building Toolkit (2013) to help local governments identify and remove barriers to sustainable design and green building in their permitting processes. The toolkit can also be used by members of the development community, local government green teams, and other building professionals. 
The toolkit addresses local codes and ordinances that affect the design, construction, renovation, and operation and maintenance of a building and its immediate site. It contains an assessment tool, a resource guide, and a guide to developing an action plan for implementing changes to the permitting process.


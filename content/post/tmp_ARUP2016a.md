
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Cities Alive: Towards a walking world"
date: 2016-01-01
summary: "Cities Alive: Towards a walking world highlights the significant social, economic, environmental and political benefits of walking. Informed by specialist insight and multidisciplinary expertise from across our global offices, we highlight 50 benefits of walking explored through 16 distinct indicati ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/c/cities_alive_towards_a_walking_world.jpg?h=481&mw=340&w=340&hash=FAE9BF793BEC13BD5601FBAB6185FF8D"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/c/cities_alive_towards_a_walking_world.jpg?h=481&mw=340&w=340&hash=FAE9BF793BEC13BD5601FBAB6185FF8D"
categories: ['planning', 'landscape', 'urban design']
tags: ['mobility', 'public space', 'urban nature', 'streetscape', 'infrastructure']  
showDate: false
showReadTime: false
---

**ARUP**, 2016.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/cities-alive-towards-a-walking-world)

> **Abstract** 
>
> Cities Alive: Towards a walking world highlights the significant social, economic, environmental and political benefits of walking. Informed by specialist insight and multidisciplinary expertise from across our global offices, we highlight 50 benefits of walking explored through 16 distinct indicative themes, and list 40 actions that city leaders can consider to inform walking policy, strategy and design. These are informed by a catalogue of 80 international case studies that will inspire action, and further aid cities in identifying and evaluating opportunities.


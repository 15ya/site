
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Sustainable Urban Energy Planning: A Handbook for Cities and Towns in Developing Countries"
date: 2009-01-01
summary: "The main purpose of this handbook is to assist people who are working in or with local government to develop sustainable energy and climate action plans and implementation programmes. There can be no single recipe for all cities ? so it is up to each local government to develop its own innovative an ..."
featureImage: "https://sustainabledevelopment.un.org/content/images/imagemain400_177.jpg"
thumbnail: "https://sustainabledevelopment.un.org/content/images/imagemain400_177.jpg"
categories: ['local government', 'planning']
tags: ['energy', 'sustainability', 'land use', 'housing', 'informality', 'mobility', 'services']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2009.
[DOWNLOAD HERE](https://sustainabledevelopment.un.org/index.php?page=view&type=400&nr=293&menu=1515)

> **Abstract** 
>
> The main purpose of this handbook is to assist people who are working in or with local government to develop sustainable energy and climate action plans and implementation programmes. There can be no single recipe for all cities ? so it is up to each local government to develop its own innovative and appropriate plans based on local resources and needs. We, at UN-Habitat and ICLEI, hope this handbook will go a long way to helping you to do this and allow you to take full advantage of the opportunities inherent in such planning. This handbook is for you if you are working in an urban context with or as part of a local government, and your primary areas of concern include: Delivery of services to citizens Economic development Strategic development planning Land use planning, zoning, building plans approval Housing and poverty issues Environmental management Management of local government resources and systems Fiscal responsibility and risk management Water resource and waste management Public health Transportation management The Introduction addresses the challenges of energy consumption, climate change and development in developing countries. It deals with the role of urban centres and local governments in defining a sustainable development path and a new energy future in their countries. It includes an explanation of the greenhouse effect and a mini-history on climate change. Chapter 1 is an explanation of why it is important for urban centres in developing countries to engage in sustainable energy planning. Chapter 2 provides in some detail a step-bystep process to developing and implementing a sustainable energy plan, illustrated by relevant case studies. Chapter 3 offers a range of case studies from developed and developing urban centres covering all the major areas of local government responsibility. Chapter 4 provides an extensive list and information on support organisations and resources.


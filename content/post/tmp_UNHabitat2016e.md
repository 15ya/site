
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Using Minecraft for Community Participation"
date: 2016-01-01
summary: "Minecraft is a ‘sandbox’ computer game originally launched in 2011. The game has sold nearly 100 million copies worldwide, making it one of the world’s best-selling computer games. UN-Habitat has been working with Mojang, the makers of Minecraft, since 2012 on an innovative programme called Block by ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/08/Pages-from-USING-MINECRAFT-FOR-COMMUNITY-PARTICIPATION-MANUAL.jpg.webp?itok=xsPlEWvf"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/08/Pages-from-USING-MINECRAFT-FOR-COMMUNITY-PARTICIPATION-MANUAL.jpg.webp?itok=xsPlEWvf"
categories: ['planning', 'landscape', 'architecture', 'urban design']
tags: ['governance', 'participation']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://unhabitat.org/manual-using-minecraft-for-community-participation)

> **Abstract** 
>
> Minecraft is a ‘sandbox’ computer game originally launched in 2011. The game has sold nearly 100 million copies worldwide, making it one of the world’s best-selling computer games. UN-Habitat has been working with Mojang, the makers of Minecraft, since 2012 on an innovative programme called Block by Block.
Through Block by Block, UN-Habitat uses Minecraft as a community participation tool in the design of urban public spaces. UN-Habitat's experiences of impementing these projects all over the world shows that Minecraft is a good way of involving hard to reach groups such as youth, women and slum dwellers in urban design processes. Read this manual to find out how it's done.


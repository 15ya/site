
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guide to Equitable Community-Driven Climate Preparedness Planning"
date: 2017-01-01
summary: "While many municipalities are planning for climate resilience, the process isn’t necessarily inclusive of the most vulnerable communities. This guide provides direction to local governments to design more equitable planning processes. The document includes: 
- a primer on social inequities and the r ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/07/USDN-232x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/07/USDN-232x300.jpg"
categories: ['local government', 'planning']
tags: ['participation', 'climate change', 'equity', 'resilience', 'risk reduction']  
showDate: false
showReadTime: false
---

**Urban Sustainability Directors Network**, 2017.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/guide-to-equitable-community-driven-climate-preparedness-planning-july-2019/)

> **Abstract** 
>
> While many municipalities are planning for climate resilience, the process isn’t necessarily inclusive of the most vulnerable communities. This guide provides direction to local governments to design more equitable planning processes. The document includes: 
- a primer on social inequities and the role of government 
- an equitable, community-driven climate preparedness planning framework, and
- equitable climate resilience solutions.


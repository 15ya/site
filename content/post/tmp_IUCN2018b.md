
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for privately protected areas"
date: 2018-01-01
summary: "These guidelines address planning and management of privately protected areas (or PPAs) and the guidance is aimed principally at practitioners and policy makers, who are or may be involved with PPAs. Guidance is given on all aspects of PPA establishment, management and reporting, and information is  ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-029-En.PNG?itok=uCnYZR9W"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-029-En.PNG?itok=uCnYZR9W"
categories: ['landscape', 'planning', 'local government']
tags: ['urban nature', 'sustainability', 'governance', 'land use', 'finance']  
showDate: false
showReadTime: false
---

**IUCN**, 2018.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/47916)

> **Abstract** 
>
> These guidelines address planning and management of privately protected areas (or PPAs) and the guidance is aimed principally at practitioners and policy makers, who are or may be involved with PPAs. Guidance is given on all aspects of PPA establishment, management and reporting, and information is provided on principles and best practices, with examples drawn from many different parts of the world. The aim of these guidelines is to shape the application of IUCN policy and principles towards enhanced effectiveness and conservation outcomes, focused on PPA managers and administrators. Not all the guidance will necessarily apply in all social, political and economic contexts. However, learning from best practices around the world and considering how these can be incorporated at site or national level may improve the likelihood of success in private conservation and suggest how conditions might be improved to favour PPAs and thus capitalise on the opportunities they present.


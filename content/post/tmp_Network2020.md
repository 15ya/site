
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Toolkit for a Resilient Recovery"
date: 2020-01-01
summary: "The Resilient Cities Network worked with Chief Resilience Officers to co-design and identify the best methodologies to enable cities to plan a resilient recovery from the impacts of the Covid-19 pandemic. 
The toolkit for a resilient recovery is based on best practices from R-Cities member cities. C ..."
featureImage: "https://www.urbanet.info/wp-content/uploads/2022/02/Resilient-recovery-Framework-400x394.png"
thumbnail: "https://www.urbanet.info/wp-content/uploads/2022/02/Resilient-recovery-Framework-400x394.png"
categories: ['local government', 'planning']
tags: ['governance', 'sustainability', 'resilience']  
showDate: false
showReadTime: false
---

**Resilient Cities Network**, 2020.
[DOWNLOAD HERE](https://resilientcitiesnetwork.org/resilient-recovery-toolkit/)

> **Abstract** 
>
> The Resilient Cities Network worked with Chief Resilience Officers to co-design and identify the best methodologies to enable cities to plan a resilient recovery from the impacts of the Covid-19 pandemic. 
The toolkit for a resilient recovery is based on best practices from R-Cities member cities. Cities can use it independently, as well as in collaboration with R-Cities. 
The toolkit enables each city to define its own path to recovery by considering four iterative activities: assessing and analyzing the situation; defining a portfolio of actions; improving the proposals; and deepening learning. 
Characterized by a place-based approach, the toolkit helps local governments to identify opportunities that include not only physical solutions – such as rethinking street layouts to optimize pedestrian and cycling mobility – but also cultural practices, cross-cutting issues, and governance arrangements to achieve carbon neutrality in the long term.


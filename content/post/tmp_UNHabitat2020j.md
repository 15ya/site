
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Settlement Profiling Tool"
date: 2020-01-01
summary: "In partnership with UNHCR who funded the project, UN-Habitat has prepared the Settlement Profiling Tool to guide field personnel in creating cross-sectoral Settlement profiles intended to help inform future urban development plans and policies in displacement affected contexts.
The user-friendly Too ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/08/Settlement%20Profiling%20Tool.png.webp?itok=uryEfvoA"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/08/Settlement%20Profiling%20Tool.png.webp?itok=uryEfvoA"
categories: ['local government', 'planning']
tags: ['governance', 'housing', 'land use', 'informality', 'services']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/settlement-profiling-tool#:~:text=InpartnershipwithUNHCRwho,policiesindisplacementaffectedcontexts.)

> **Abstract** 
>
> In partnership with UNHCR who funded the project, UN-Habitat has prepared the Settlement Profiling Tool to guide field personnel in creating cross-sectoral Settlement profiles intended to help inform future urban development plans and policies in displacement affected contexts.
The user-friendly Tool guides local authorities and humanitarian-development actors through an iterative process of investigation with the aim of developing a common baseline of information to support coordination and planning, particularly in situations where protracted displacement has occurred. The Tool enables officials, UN Agencies, Donors, NGOs, local authorities and other stakeholders to more effectively prioritize investment opportunities where humanitarian actions are ongoing and provide entry points for more sustainable development trajectories. The profiling process will facilitate informed decision making as part of long-term climate and socially responsive urban and regional infrastructure planning.


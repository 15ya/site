
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Toward Sustainable Fleet Transitions"
date: 2022-01-01
summary: "The decarbonization of the transportation industry is critical to reducing emissions in the built environment. As emissions from medium- and heavy-duty vehicles represent approximately 30% of transportation sector emissions, the transition to zero-emission fleets will have a significant impact. With ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/t/towards-sustainable-fleet-transitions-cover-image.jpg?h=444&mw=340&w=340&hash=CB38071B3F2C0307F16E8B8E6753D527"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/t/towards-sustainable-fleet-transitions-cover-image.jpg?h=444&mw=340&w=340&hash=CB38071B3F2C0307F16E8B8E6753D527"
categories: ['local government']
tags: ['mobility', 'sustainability', 'services']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/toward-sustainable-fleet-transitions)

> **Abstract** 
>
> The decarbonization of the transportation industry is critical to reducing emissions in the built environment. As emissions from medium- and heavy-duty vehicles represent approximately 30% of transportation sector emissions, the transition to zero-emission fleets will have a significant impact. With legislative incentives, such as the electric vehicle tax credits extended by the Inflation Reduction Act and incentive programs at the state level, the sustainable transition of fleets is more financially feasible than ever. 
To help navigate both the scale of opportunity and depth of challenges for electrifying vehicle fleets, Arup partnered with global renewable energy leader Enel to jointly release a new report guiding this transition. Drawing from decades of real-world experience in decarbonization and circularity, Arup and Enel synthesize lessons learned from zero-emission vehicle specialists, energy providers, fleet owners and managers, and circular economy experts to set out a clear roadmap. 
The report walks through the phases of transition, identifying quick wins and innovative opportunities for value stacking throughout the process. With an additional lens on circularity, resilience, and community benefits, the report highlights the value of transitioning strategies that extend beyond the norm — resulting in better outcomes and overall reduced risk. This illustrated and accessible guide aims to help the transportation industry usher in a more sustainable, carbon-free mobility future.


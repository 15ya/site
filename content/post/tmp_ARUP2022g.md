
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Energy Resilience Framework"
date: 2022-01-01
summary: "Energy is critical to our daily lives. Increased integration and complexity mean ensuring energy systems are resilient has never been more challenging. Arup’s Energy Resilience Framework can diagnose – for any energy system – where challenges and opportunities for improvement lie. ..."
featureImage: ""
thumbnail: ""
categories: ['local government']
tags: ['energy', 'resilience', 'sustainability']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/energy-resilience-framework)

> **Abstract** 
>
> Energy is critical to our daily lives. Increased integration and complexity mean ensuring energy systems are resilient has never been more challenging. Arup’s Energy Resilience Framework can diagnose – for any energy system – where challenges and opportunities for improvement lie.


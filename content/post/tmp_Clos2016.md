
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines on Urban and Territorial Planning"
date: 2016-01-01
summary: "The International Guidelines on Urban and Territorial Planning serve both as a source of inspiration and a compass for decision makers and urban professionals when reviewing urban and territorial planning systems. The Guidelines provide national governments, local authorities, civil society organiza ..."
featureImage: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2021-09/International%20Guidelines%20on%20Urban%20and%20Territorial%20Planning.png.webp?itok=vKjZVPde"
thumbnail: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2021-09/International%20Guidelines%20on%20Urban%20and%20Territorial%20Planning.png.webp?itok=vKjZVPde"
categories: ['local government', 'planning']
tags: ['governance', 'land use']  
showDate: false
showReadTime: false
---

**Cities Alliance**, 2016.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/documents/guidelines-urban-and-territorial-planning)

> **Abstract** 
>
> The International Guidelines on Urban and Territorial Planning serve both as a source of inspiration and a compass for decision makers and urban professionals when reviewing urban and territorial planning systems. The Guidelines provide national governments, local authorities, civil society organizations and planning professionals with a global reference framework that promotes more compact, socially inclusive, better integrated and connected cities and territories that foster sustainable urban development and are resilient to climate change.


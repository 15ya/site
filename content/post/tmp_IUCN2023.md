
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Manual of invasive alien species in the Eastern Mediterranean"
date: 2023-01-01
summary: "Marine invasive species can have a disastrous impact on biodiversity, ecosystems, fisheries, human health, tourism and coastal development, and they can be extremely difficult and costly to control. This manual is about the most common invasive alien species (IAS) in the Eastern Mediterranean Sea, m ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2023-002-En.png?itok=qwvhRHU5"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2023-002-En.png?itok=qwvhRHU5"
categories: ['landscape', 'planning']
tags: ['water', 'sustainability', 'urban nature']  
showDate: false
showReadTime: false
---

**IUCN**, 2023.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/50729)

> **Abstract** 
>
> Marine invasive species can have a disastrous impact on biodiversity, ecosystems, fisheries, human health, tourism and coastal development, and they can be extremely difficult and costly to control. This manual is about the most common invasive alien species (IAS) in the Eastern Mediterranean Sea, mostly originating from the Red Sea, creating socioeconomic and ecological impacts on various levels. The manual highlights the current situation of IAS in the Eastern Mediterranean, and mainly focuses on the management measures of IAS, existing legislations and action plans, mitigation measures and implementation of the controlling procedures with different aspects and practices among various disciplines and across regions. The manual is designed for a varied audience from Marine Protected Area (MPA) managers to local authorities, from scientists to non-governmental or-ganisations that are involved in the in the management of IAS.


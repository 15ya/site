
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Land and Property Tax"
date: 2011-01-01
summary: "Land and Property Tax: a Policy Guide is a companion for government officials and land and property professionals seeking to understand how to establish a viable and vibrant land-based taxation system. Using various examples, alternatives and illustrations around the World, the Guide provides a comp ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Land-and-Property-Tax-1.jpg.webp?itok=1D3Ka4e5"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/06/Land-and-Property-Tax-1.jpg.webp?itok=1D3Ka4e5"
categories: ['local government']
tags: ['governance', 'finance']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2011.
[DOWNLOAD HERE](https://unhabitat.org/land-and-property-tax)

> **Abstract** 
>
> Land and Property Tax: a Policy Guide is a companion for government officials and land and property professionals seeking to understand how to establish a viable and vibrant land-based taxation system. Using various examples, alternatives and illustrations around the World, the Guide provides a compelling case for generating local revenue through land and its improvements.
In the Guide, you will find answers to questions such as why land and property taxes are often an important source of local revenue, what the options are for designing and sustaining a land-based tax system, and what needs to be considered in their implementation. The Guide presents a step-by-step approach to implementing a range of land and property taxation policies, strategies, tools and instruments.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Developing a Communication Strategy for a Local Government Association"
date: 2004-01-01
summary: "Communication is now recognized as a key element in the management of any institution, whether it is a multi-national corporation, a small non-governmental organization or a Local Government Association (LGA). Efficient communication is more than a simple transfer of information: it must be bi-direc ..."
featureImage: ""
thumbnail: ""
categories: ['local government']
tags: ['governance']  
showDate: false
showReadTime: false
---

**VNG**, 2004.
[DOWNLOAD HERE](https://www.local2030.org/library/view/325)

> **Abstract** 
>
> Communication is now recognized as a key element in the management of any institution, whether it is a multi-national corporation, a small non-governmental organization or a Local Government Association (LGA). Efficient communication is more than a simple transfer of information: it must be bi-directional, open to exchanges and feedback. In order to survive, the organization must inform everyone it is in contact with. But if it wants to grow, the organization must also listen to all these people and adapt itself in reaction to what it hears. Decentralization and empowerment cannot succeed if the LGA lacks the ability to communicate with all its partners, be they employees, Board members, civil society, the citizens at large or various spheres of government. This document can be used as an application-oriented, easy-reference manual in developing a communication strategy. It is especially tuned to local government institutes and LGAs whose leadership wishes to improve and to strengthen the way the organization communicates, both internally and externally.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Valuation of Unregistered Land: A Practice Manual"
date: 2021-01-01
summary: "Value, and its attribution to unregistered land, is important information for effective land acquisition, taxation and transfer processes and a key component of land administration systems. 
This manual presents a practical approach to the valuation of unregistered land. It is designed to aid implem ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/Valuation-of-Unregistered-Land-866x880.jpg.webp?itok=ubc1YkRn"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/09/Valuation-of-Unregistered-Land-866x880.jpg.webp?itok=ubc1YkRn"
categories: ['local government']
tags: ['land use', 'informality', 'equity', 'urban development']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/valuation-of-unregistered-land-a-practice-manual)

> **Abstract** 
>
> Value, and its attribution to unregistered land, is important information for effective land acquisition, taxation and transfer processes and a key component of land administration systems. 
This manual presents a practical approach to the valuation of unregistered land. It is designed to aid implementation of Valuation of Unregistered Lands: A Policy Guide, prepared by the Global Land Tool Network for the United Nations Human Settlements Programme (UN-Habitat), in combination with the international valuation standards of the International Valuation Standards Council. The manual comprises two parts. The first gives an overview of recommendations related to the valuation of unregistered land rights, and the second provides more detail about what to consider when working in this complex environment. 
The competent valuation of unregistered land rights requires transparent and accountable assessment of all relevant risks. This is a difficult, risk-laden, complex, and often emotionally and politically fraught process, with limited professional capacity and knowledge and a chronic lack of data and cultural/social understanding. It can be argued that “value”, whether defined in purely economic terms or in a wider context of social, environmental, and cultural value, is central to the establishment of tenure security and the identification of legitimate land rights. The manual is designed to be applicable globally, particularly in developing nations. It is based on agreed standards of best practice, due diligence protocols and expert collaboration. It explains the key concepts of valuation, land tenure, bases of value, professional capacity-building and valuation frameworks and provides an example template/aide-memoire for unregistered land valuation that can be used by valuers. 
Governments, policymakers, land professionals, valuation practitioners, capacity development institutions and other interested parties should use this manual to help achieve the Sustainable Development Goals related to conflict prevention, food security, gender equity, justice, and sustainable urban and rural development.


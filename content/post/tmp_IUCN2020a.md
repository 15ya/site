
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Protected Areas Benefits Assessment Tool + (PA-BAT+)"
date: 2020-01-01
summary: "The Protected Areas Benefits Assessment Tool+ (PA-BAT+) aims to collate and assess information about the overall benefits from conservation and protection in protected areas. It can also be used in other area-based conservation sites. This technical guidance provides a quick overview of why understa ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PATRS-004-En.PNG?itok=FHraaTNe"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PATRS-004-En.PNG?itok=FHraaTNe"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['urban nature', 'sustainability', 'governance', 'finance', 'conservation']  
showDate: false
showReadTime: false
---

**IUCN**, 2020.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/49081)

> **Abstract** 
>
> The Protected Areas Benefits Assessment Tool+ (PA-BAT+) aims to collate and assess information about the overall benefits from conservation and protection in protected areas. It can also be used in other area-based conservation sites. This technical guidance provides a quick overview of why understanding the benefits from protected areas is important, provides a deptailed guide to using the PA-BAT+ and explains how to understand and use the results. Seven case studies outline different uses and adaptations of the tool in Croatia, Colombia, Turkey, Myanmar, USA and Ethiopia, as well as describing the use of the tool across natural World Heritage sites.


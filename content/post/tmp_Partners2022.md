
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Equity Guide For Green Stormwater Infrastructure Practitioners"
date: 2022-01-01
summary: "Over the last year, members of the Green Infrastructure Leadership Exchange and Greenprint Partners led a collaborative process to develop a comprehensive resource for public sector stormwater management organizations to advance and measure equity in their green stormwater infrastructure policies, p ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2022/03/Equity-Guide-for-GSI-Greenprint-Partners-227x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2022/03/Equity-Guide-for-GSI-Greenprint-Partners-227x300.png"
categories: ['planning', 'local government', 'urban design']
tags: ['water', 'sustainability', 'participation', 'climate change', 'risk reduction', 'nbs', 'equity']  
showDate: false
showReadTime: false
---

**Greenprint Partners**, 2022.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/equity-guide-for-green-stormwater-infrastructure-practitioners-mar-2022/)

> **Abstract** 
>
> Over the last year, members of the Green Infrastructure Leadership Exchange and Greenprint Partners led a collaborative process to develop a comprehensive resource for public sector stormwater management organizations to advance and measure equity in their green stormwater infrastructure policies, programs, and projects. In March 2022, the Equity Guide for Green Stormwater Infrastructure Practitioners officially launched, offering program managers equity best practices, evaluation frameworks, bright spots and a variety of tools to implement recommendations in the Guide. All of these resources are grounded in a literature review that features insights from 80 published sources and interviews with more than 60 practitioners and community based organizations across North America. Watch an introductory video at https://vimeo.com/691140284. 
The Guide’s intended audience—green infrastructure program managers at public sector stormwater management organizations—now have access to a framework, research, and tools that will help facilitate stronger partnerships between their organizations and the communities they serve. The Guide offers resources around seven core Goals. It helps strengthen each organization’s Internal Readiness to advance equity, and to hone internal Policies and Programs, as well pathways to collaboratively advance goals related to Centering Community, Siting and Investment, Benefits-Driven Project Development, Economic Stability, and Preventing Displacement.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guide to investing in locally controlled forestry"
date: 2012-01-01
summary: "This guide is provides guidance on how to structure enabling investments and prepare the ground for asset investments that yield acceptable returns and reduced risk, not only for investors, but also for local forest right-holders, national governments and society at large. The volume sets out a fram ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2012-084.jpg?itok=COa7ZqJC"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2012-084.jpg?itok=COa7ZqJC"
categories: ['local government']
tags: ['urban nature', 'sustainability', 'finance']  
showDate: false
showReadTime: false
---

**IUCN**, 2012.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/10310)

> **Abstract** 
>
> This guide is provides guidance on how to structure enabling investments and prepare the ground for asset investments that yield acceptable returns and reduced risk, not only for investors, but also for local forest right-holders, national governments and society at large. The volume sets out a framework for structuring investments with tactical advice for building the partnerships necessary for success. Also included are case studies of successful initiatives and a range of useful templates and sources for further information are provided.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Guiding Principles for City Climate Action Planning"
date: 2015-01-01
summary: "The Guiding Principles for City Climate Action Planning reviews typical steps in the city-level climate action planning process in light of a proposed set of globally applicable principles. These principles, shown below, developed through a robust and open multi-stakeholder process, support local of ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['sustainability', 'resilience', 'governance', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://unhabitat.org/the-guiding-principles)

> **Abstract** 
>
> The Guiding Principles for City Climate Action Planning reviews typical steps in the city-level climate action planning process in light of a proposed set of globally applicable principles. These principles, shown below, developed through a robust and open multi-stakeholder process, support local officials, planners and stakeholders in climate action planning. Such plans aim to help cities to reduce greenhouse gas emissions and adopt low emission development trajectories, as well as adapt to the impacts of climate change and build local climate resilience. 
These Guiding Principles are intended to be applied flexibly, together with more detailed ‘how to’ manuals, to help cities more effectively play their role in reducing greenhouse gas emissions and building climate resilience.


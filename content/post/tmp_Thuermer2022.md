
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "ACTION: Participatory science toolkit against pollution"
date: 2022-01-01
summary: "The ACTION toolkit is the ultimate resource collection for everyone interested in doing citizen science the ACTION way. The toolkit draws on expertise in citizen science, participatory design, social innovation, socio-economic studies, pollution, open science, social computing, open data and softwar ..."
featureImage: "https://actionproject.eu/wp-content/uploads/2022/04/Toolkit.png"
thumbnail: "https://actionproject.eu/wp-content/uploads/2022/04/Toolkit.png"
categories: ['urban design', 'planning']
tags: ['participation', 'sustainability', 'governance', 'risk reduction', 'resilience', 'health', 'equity']  
showDate: false
showReadTime: false
---

**ACTION**, 2022.
[DOWNLOAD HERE](https://actionproject.eu/)

> **Abstract** 
>
> The ACTION toolkit is the ultimate resource collection for everyone interested in doing citizen science the ACTION way. The toolkit draws on expertise in citizen science, participatory design, social innovation, socio-economic studies, pollution, open science, social computing, open data and software development in the ACTION team, to ensure it suits the requirements of citizen science projects, addressing the practical problems that they face throughout the different stages of each project.


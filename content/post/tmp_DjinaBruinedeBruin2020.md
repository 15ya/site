
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Camping"
date: 2020-01-01
summary: "This manual will serve as a guide for people who are looking for participatory observation methods. The manual will look into details of urban camping: camping in a caravan on a square (or another urban space) to converse with users of the square and experience the place and it’s daily rhythm by bei ..."
featureImage: ""
thumbnail: ""
categories: ['architecture', 'landscape', 'urban design']
tags: ['public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2020.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/urban-camping/)

> **Abstract** 
>
> This manual will serve as a guide for people who are looking for participatory observation methods. The manual will look into details of urban camping: camping in a caravan on a square (or another urban space) to converse with users of the square and experience the place and it’s daily rhythm by being present. This multiple-day intervention within public space is helpful for understanding the soul of the place, to build a network through spontaneous encounters (triangulation) and informal conversations. It is also an intervention that can very naturally facilitate on-site brainstorming sessions. These different elements and layers can be useful in the exploration phase of area transformation, placemaking projects or participation processes.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "An International Framework for Eco-Industrial Parks"
date: 2017-01-01
summary: "The aim of this publication is to provide an international framework (the “Framework”) with the minimum requirements and performance expectations as to how an industrial park can become an Eco-Industrial Park (EIP). It summarizes the key areas in which the three international organizations that have ..."
featureImage: "https://openknowledge.worldbank.org/server/api/core/bitstreams/c3adb6f0-2d6b-5daa-8d32-d8172d767889/content"
thumbnail: "https://openknowledge.worldbank.org/server/api/core/bitstreams/c3adb6f0-2d6b-5daa-8d32-d8172d767889/content"
categories: ['local government', 'planning']
tags: ['sustainability', 'energy', 'governance', 'land use', 'infrastructure']  
showDate: false
showReadTime: false
---

**World Bank**, 2017.
[DOWNLOAD HERE](https://openknowledge.worldbank.org/handle/10986/29110)

> **Abstract** 
>
> The aim of this publication is to provide an international framework (the “Framework”) with the minimum requirements and performance expectations as to how an industrial park can become an Eco-Industrial Park (EIP). It summarizes the key areas in which the three international organizations that have driven the development of this framework — including the United Nations Industrial Development Organisation (UNIDO), the World Bank Group, and Deutsche Gesellschaft für Internationale Zusammenarbeit GmbH (GIZ) — have aligned regarding what constitutes an Eco-Industrial Park (EIP). The International Framework for Eco-Industrial Parks will guide policymakers and practitioners on the critical elements that will help both governments and the private sector work together in establishing economically, socially and environmentally sustainable eco-industrial parks.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Global Framework for Urban Water and Sanitation"
date: 2019-01-01
summary: "UNICEF’s Global Framework for Urban WASH creates a common vision for UNICEF’s approach to urban WASH programming: it will enable country, regional and global WASH teams to have a clear and shared sense of direction and purpose, as the organisation increases its engagement supporting the most margina ..."
featureImage: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2021-09/Global%20Framework%20for%20Urban%20Water%2C%20Sanitation%20and%20Hygiene.jpg.webp?itok=haCILl5V"
thumbnail: "https://www.citiesalliance.org/sites/default/files/styles/d03/public/2021-09/Global%20Framework%20for%20Urban%20Water%2C%20Sanitation%20and%20Hygiene.jpg.webp?itok=haCILl5V"
categories: ['local government', 'planning']
tags: ['water', 'governance', 'informality', 'infrastructure', 'sustainability', 'services', 'equity']  
showDate: false
showReadTime: false
---

**UNICEF**, 2019.
[DOWNLOAD HERE](https://www.citiesalliance.org/resources/publications/documents/global-framework-urban-water-and-sanitation)

> **Abstract** 
>
> UNICEF’s Global Framework for Urban WASH creates a common vision for UNICEF’s approach to urban WASH programming: it will enable country, regional and global WASH teams to have a clear and shared sense of direction and purpose, as the organisation increases its engagement supporting the most marginalised urban children and their families. 
The Framework is based on UNICEF’s experiences in urban WASH programming in over 50 countries. It is structured around three areas of support: sector-level, service-level and user-level support, with suggested entry points and activities for engagement in urban WASH. The Framework also considers three different urban contexts: urban slums, small towns and urban areas in humanitarian and protracted crisis settings, focusing on areas where UNICEF can add value, in line with the organisation’s equity agenda.


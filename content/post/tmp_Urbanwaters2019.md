
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "How to Prepare for Natural Disasters: A Pre-Disaster Toolkit for Small- to Medium-Size Communities"
date: 2019-01-01
summary: "Prepared by US EPA Urban Waters Federal Partnership, the toolkit was developed to help small- to medium-size communities jump-start their natural disaster planning process. The toolkit is divided into two sections – one for local government officials, including Emergency Management Coordinators (EMC ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/10/Screen-Shot-2019-10-31-at-2.54.08-PM-272x300.png"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/10/Screen-Shot-2019-10-31-at-2.54.08-PM-272x300.png"
categories: ['local government']
tags: ['water', 'climate change', 'risk reduction']  
showDate: false
showReadTime: false
---

**Urbanwaters**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/pre-disaster-toolkit/)

> **Abstract** 
>
> Prepared by US EPA Urban Waters Federal Partnership, the toolkit was developed to help small- to medium-size communities jump-start their natural disaster planning process. The toolkit is divided into two sections – one for local government officials, including Emergency Management Coordinators (EMCs), utility district managers, and local elected officials­­—and a section for community residents. Within each section is information about tools and resources available through federal and state agencies, and disaster relief organizations, and At-a-Glance Preparedness Checklists. The creation of the toolkit was inspired by comments made during a serious of flood management planning workshops for local and regional officials conducted in 2019 in flood-prone regions throughout Texas. What emerged was the need for a one-stop shop for tools and resources to help with planning for hurricanes, floods and other natural disasters. How to Prepare for Natural Disasters: A Pre-Disaster Toolkit for Small- to-Medium Size Communities is designed to help address that need.


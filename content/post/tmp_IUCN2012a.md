
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Promoting local innovations (PLI) for community-based climate change adaptation in coastal areas : a facilitator's guide to the PLI workshop"
date: 2012-01-01
summary: "This facilitator's guide for the Promoting Local Innovations (PLI) workshop has been developed to provide conservation and development practitioners working with coastal communities with an interactive and participatory tool that they can use to promote local innovations for climate change adaptatio ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2012-065.jpg?itok=8C6VnbKk"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2012-065.jpg?itok=8C6VnbKk"
categories: ['landscape', 'urban design']
tags: ['urban nature', 'water', 'participation', 'conservation', 'climate change']  
showDate: false
showReadTime: false
---

**IUCN**, 2012.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/10250)

> **Abstract** 
>
> This facilitator's guide for the Promoting Local Innovations (PLI) workshop has been developed to provide conservation and development practitioners working with coastal communities with an interactive and participatory tool that they can use to promote local innovations for climate change adaptation. The main objective of such a PLI workshop is to facilitate a social learning process between different stakeholder groups (local community, governmental agencies, and academia, NGO's) in order to identify and promote local innovations for climate change adaptation in form of community driven action plans.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Tools for Co-creation"
date: 2022-01-01
summary: "In the UNaLab project, a wide range of co-creation tools and methods have been used across the different urban demonstration areas to explore, design, implement and evaluate the nature-based solutions implemented by the UNaLab cities to tackle their speciﬁc climate and water-related challenges. Thes ..."
featureImage: "https://unalab.enoll.org/wp-content/uploads/2019/11/user-personas.png"
thumbnail: "https://unalab.enoll.org/wp-content/uploads/2019/11/user-personas.png"
categories: ['planning', 'urban design']
tags: ['participation', 'governance', 'nbs']  
showDate: false
showReadTime: false
---

**UNaLab**, 2022.
[DOWNLOAD HERE](https://unalab.enoll.org/)

> **Abstract** 
>
> In the UNaLab project, a wide range of co-creation tools and methods have been used across the different urban demonstration areas to explore, design, implement and evaluate the nature-based solutions implemented by the UNaLab cities to tackle their speciﬁc climate and water-related challenges. These tools have been gathered in an online co-creation toolkit, which can be used as a source of inspiration to prepare co-creative sessions with various stakeholders.


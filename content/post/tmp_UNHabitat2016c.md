
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "GIS Handbook for Municipalities"
date: 2016-01-01
summary: "GIS technology has emerged as a powerful set of tools for managing and analysing spatial data (data tied to a specific point or area on the ground). The various types of spatial data are at the core of many development efforts, and GIS is seen as a solution to a number ofproblems local governments f ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/04/GISHandbook.jpg.webp?itok=kZV50G4y"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/04/GISHandbook.jpg.webp?itok=kZV50G4y"
categories: ['local government', 'planning', 'urban design']
tags: ['land use', 'mobility', 'infrastructure']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://unhabitat.org/gis-handbook-for-municipalities)

> **Abstract** 
>
> GIS technology has emerged as a powerful set of tools for managing and analysing spatial data (data tied to a specific point or area on the ground). The various types of spatial data are at the core of many development efforts, and GIS is seen as a solution to a number ofproblems local governments face in their area of jurisdiction. 
This handbook serves as an introductory guide to geographic information system (GIS) technology for local government and other interested stakeholders. It suggests some common opportunities for GIS application, the benefits a GIS provides to users, and what is required to set up a GIS and sustain it.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Improving the public's health: A resource for local authorities"
date: 2013-01-01
summary: "While detailed guidance is yet to be developed, this report fills the gap by providing information and resources in nine key areas to help council leaders answer these questions. It brings together a wide range of evidence-based interventions about 'what works' in improving public health and reducin ..."
featureImage: "https://www.kingsfund.org.uk/sites/default/files/styles/media_small/public/field/field_publication_cover/improving-the-publics-health-front-cover.jpg?itok=RM01YYWg"
thumbnail: "https://www.kingsfund.org.uk/sites/default/files/styles/media_small/public/field/field_publication_cover/improving-the-publics-health-front-cover.jpg?itok=RM01YYWg"
categories: ['local government']
tags: ['health']  
showDate: false
showReadTime: false
---

**The Kings Fund**, 2013.
[DOWNLOAD HERE](https://www.kingsfund.org.uk/publications/improving-publics-health)

> **Abstract** 
>
> While detailed guidance is yet to be developed, this report fills the gap by providing information and resources in nine key areas to help council leaders answer these questions. It brings together a wide range of evidence-based interventions about 'what works' in improving public health and reducing health inequalities. It presents the business case for different interventions and signposts the reader to further resources and case studies.


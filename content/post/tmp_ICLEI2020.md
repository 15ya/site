
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Efficient Street Lighting Using Led - Policy Solution"
date: 2020-01-01
summary: "Define a policy outlining the requirement to use energy efficient technology in all governmental operations and in all systems providing lighting services to the community. This policy goes hand-in-hand with a public procurement policy, by ensuring sustainable procurement requirements are included r ..."
featureImage: "https://www.thegpsc.org/sites/gpsc/files/policy_led.png"
thumbnail: "https://www.thegpsc.org/sites/gpsc/files/policy_led.png"
categories: ['landscape', 'local government', 'urban design']
tags: ['infrastructure', 'streetscape', 'mobility', 'sustainability']  
showDate: false
showReadTime: false
---

**ICLEI**, 2020.
[DOWNLOAD HERE](https://iclei.org/e-library/policy-solution-from-the-iclei-solution-gateway-on-efficient-street-lighting-using-led-10/)

> **Abstract** 
>
> Define a policy outlining the requirement to use energy efficient technology in all governmental operations and in all systems providing lighting services to the community. This policy goes hand-in-hand with a public procurement policy, by ensuring sustainable procurement requirements are included relevant to lighting and lighting systems.


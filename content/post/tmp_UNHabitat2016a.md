
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Addressing Climate Change in National Urban Policies"
date: 2016-01-01
summary: "National Urban Policy is a tool for government and other stakeholders that can assist with achieving more sustainable urban development. It also facilitates an enabling environment that allows stakeholders to take advantage of urban opportunity. How to address climate change in cities and human sett ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/10/Pages-from-Addressing-Climate-Change-in-National-Urban-Policy.jpg.webp?itok=xFrFoPyU"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2016/10/Pages-from-Addressing-Climate-Change-in-National-Urban-Policy.jpg.webp?itok=xFrFoPyU"
categories: ['local government']
tags: ['governance', 'sustainability', 'resilience', 'climate change', 'services']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2016.
[DOWNLOAD HERE](https://unhabitat.org/addressing-climate-change-in-national-urban-policy)

> **Abstract** 
>
> National Urban Policy is a tool for government and other stakeholders that can assist with achieving more sustainable urban development. It also facilitates an enabling environment that allows stakeholders to take advantage of urban opportunity. How to address climate change in cities and human settlements represents one of the most pressing challenges facing urban policy-makers today. This Guide recommends how to mainstream such considerations into National Urban Policy, thus helping to empower national governments, local governments, and other stakeholders to effectively address climate change.


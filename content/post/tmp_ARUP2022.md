
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Nature based play"
date: 2022-01-01
summary: "The Nature-based Play publication explores the important relationship between play, nature and climate resilience, considering the perspectives of leading urban development, early childhood, play and nature-based solutions experts. This is followed by case studies from cities around the world that d ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/n/nbp-cover.jpg?h=215&mw=340&w=340&hash=10A3BFFA8AE41EB8ED4D2D26FD5AACD9"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/n/nbp-cover.jpg?h=215&mw=340&w=340&hash=10A3BFFA8AE41EB8ED4D2D26FD5AACD9"
categories: ['landscape', 'urban design']
tags: ['age', 'urban nature', 'public space', 'health']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/promotional-materials/section/nature-based-play)

> **Abstract** 
>
> The Nature-based Play publication explores the important relationship between play, nature and climate resilience, considering the perspectives of leading urban development, early childhood, play and nature-based solutions experts. This is followed by case studies from cities around the world that demonstrate the integration of nature-based play into diverse urban environments – through parks and woodlands, neglected buildings and school yards. Each example highlights how the design of these interventions has combined play, nature, culture and climate to create environments that support childhood well-being and development, while also enhance climate resilience. 
The Nature-based Play publication aims to inspire key decision makers, built environment practitioners, local authorities, communities, and businesses to work together to foster connections between nature and play. It also aims to integrate play with nature to create healthier, more playful and more resilient environments where children, communities and our planet can thrive.


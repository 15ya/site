
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Her City Toolbox"
date: 2021-01-01
summary: "Involving girls in urban development will make the city better for everyone. Girls plan and design with diversity and different needs in mind. Participatory processes are key for planning a city that works for everyone. If we let citizens that are rarely heard be the experts, our cities and communit ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/03/cover%20.jpg.webp?itok=-_kjmpiF"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2021/03/cover%20.jpg.webp?itok=-_kjmpiF"
categories: ['planning', 'urban design']
tags: ['gender', 'participation', 'equity']  
showDate: false
showReadTime: false
---

**UN Habitat, Global Utmaning**, 2021.
[DOWNLOAD HERE](https://unhabitat.org/her-city-a-guide-for-cities-to-sustainable-and-inclusive-urban-planning-and-design-together-with)

> **Abstract** 
>
> Involving girls in urban development will make the city better for everyone. Girls plan and design with diversity and different needs in mind. Participatory processes are key for planning a city that works for everyone. If we let citizens that are rarely heard be the experts, our cities and communities will become more inclusive, equal and sustainable. 
The purpose of this initiative is to make methods and tools available to urban actors and cities globally. We support cities to scale up and mainstream girls’ participation in planning as a part of their long-term strategies to build sustainable cities and societies. 
Her City supports urban development from a girl’s perspective. We guide urban actors to implement projects through a step-by-step methodology providing an open and digitally accessible platform for all. We facilitate an ongoing dialogue between professionals and citizens. 
Her City is a joint urban development initiative by UN-Habitat (the United Nations Human Settlements Programme) and the independent think tank Global Utmaning (Global Challenge). It is financed by the Swedish Innovation Agency (Vinnova) with contributions from our partners Block by Block Foundation, White Architects, Swedish Union of Tenants and MethodKit. 
Her City is the result of the Urban Girls Movement launched in 2017 financed by the Swedish International Development Cooperation Agency (Sida), with the purpose to map efficient methods and tools that contribute to increased equality and inclusion in urban development. 
By letting Her guide you through the urban development process, you will improve the participatory urban planning, design and implementation that are key for building a city that works for girls and young women, and ultimately for everyone.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Queering Cities in Australia"
date: 2023-01-01
summary: "To help create sustainable and inclusive cities, we invest in and publish research and perspectives on a range of topics from inclusive community engagement to cities that work for women, designing for ageing communities, and designing for urban childhoods. These research areas are vital for helping ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/q/queering-cities-in-australia/2208_queering-cities-in-australia_report-a4_cover.jpg?h=481&mw=340&w=340&hash=3F31B4E39A1645B5A8F33EE3ED8870CA"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/q/queering-cities-in-australia/2208_queering-cities-in-australia_report-a4_cover.jpg?h=481&mw=340&w=340&hash=3F31B4E39A1645B5A8F33EE3ED8870CA"
categories: ['planning', 'urban design']
tags: ['gender', 'public space', 'urban nature']  
showDate: false
showReadTime: false
---

**ARUP**, 2023.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/queering-cities-in-australia)

> **Abstract** 
>
> To help create sustainable and inclusive cities, we invest in and publish research and perspectives on a range of topics from inclusive community engagement to cities that work for women, designing for ageing communities, and designing for urban childhoods. These research areas are vital for helping people enjoy and feel safe in cities. 
While research from various countries identifies how public spaces can be dangerous and exclusionary for LGBTIQ+ individuals, families and communities, we lack research on how to make public spaces safe, welcoming and inclusive for members of the LGBTIQ+ community. 
Queering Cities in Australia is a collaborative research project between Arup, Maridulu Budyari Gumal Healthy Urban Environments Collaboratory, Western Sydney University, the University of Technology Sydney, and the University of New South Wales. Queering Cities in Australia follows the report Queering Public Space, a collaboration between Arup and the University of Westminster (UK).


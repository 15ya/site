
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Gender and Urban Climate Policy"
date: 2015-01-01
summary: "This handbook indicates ways for local governments to integrate the gender dimensions of climate change into the various stages of policy-making, with a focus is on low- and middle-income countries. The handbook is not meant to be exhaustive, but rather a starting point which introduces gender conce ..."
featureImage: "https://globalabc.org/sites/default/files/members/ICLEI-mainlogo-RGB.png"
thumbnail: "https://globalabc.org/sites/default/files/members/ICLEI-mainlogo-RGB.png"
categories: ['local government', 'planning']
tags: ['gender', 'sustainability', 'informality', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://ndcpartnership.org/toolbox/gender-and-urban-climate-policy)

> **Abstract** 
>
> This handbook indicates ways for local governments to integrate the gender dimensions of climate change into the various stages of policy-making, with a focus is on low- and middle-income countries. The handbook is not meant to be exhaustive, but rather a starting point which introduces gender concepts and gender dimensions of climate change as well as resources, tools and ideas for action to climate policy decision-makers, consultants and practitioners in local governments. It can also assist women’s groups and other civil society and community-based organizations to get involved in local climate policy and to advocate for a gender-sensitive approach. Included within this handbook is a review of the methodology and priorities for gender-sensitive climate policies at urban levels, as well as recommended steps to integrate a gender-sensitive approach into the planning of local action on mitigation and adaptation. The list of resources and tools covers specific as well as more general background information and generic tools, including gender impact assessments, to assist policy decision-makers, consultants and practitioners to tackle climate change in a gender-sensitive manner in a variety of sectors in the urban context.


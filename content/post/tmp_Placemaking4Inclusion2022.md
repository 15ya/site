
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Strategy and guidelines for creation of Placemaking Ambassadors Network"
date: 2022-01-01
summary: "The Placemaking 4 Inclusion (PM4I) ”strategy and guidelines forcreation of Placemaking  Ambassadors Network” is Deliverable 2.4 of the PM4I project. It acts as a handbook for partner organisations on how to individually carry out the Ambassadors Network in their local contexts. ..."
featureImage: "http://placemaking.4learning.eu/wp-content/uploads/sites/13/2022/11/18.png"
thumbnail: "http://placemaking.4learning.eu/wp-content/uploads/sites/13/2022/11/18.png"
categories: ['local government', 'planning']
tags: ['participation', 'governance', 'public space', 'streetscape', 'infrastructure', 'equity']  
showDate: false
showReadTime: false
---

**Placemaking4Inclusion**, 2022.
[DOWNLOAD HERE](http://placemaking.4learning.eu/outputs/)

> **Abstract** 
>
> The Placemaking 4 Inclusion (PM4I) ”strategy and guidelines forcreation of Placemaking  Ambassadors Network” is Deliverable 2.4 of the PM4I project. It acts as a handbook for partner organisations on how to individually carry out the Ambassadors Network in their local contexts.


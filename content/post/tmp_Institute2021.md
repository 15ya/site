
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Metro Tunnel Creative Program - Evaluation Framework"
date: 2021-01-01
summary: "The Melbourne Metro Tunnel Creative Program is a creative and placemaking strategy led by a team of designers, curators, and place managers dedicated to enhancing city life alongside the construction of the Metro Tunnel. This toolkit describes evaluation people-centermetrics and methods to measure t ..."
featureImage: "https://image.isu.pub/210223011302-bc40709c1c2eb9e53640d4d3f73f9388/jpg/page_1.jpg"
thumbnail: "https://image.isu.pub/210223011302-bc40709c1c2eb9e53640d4d3f73f9388/jpg/page_1.jpg"
categories: ['landscape', 'planning', 'architecture', 'urban design']
tags: ['streetscape', 'public space', 'participation']  
showDate: false
showReadTime: false
---

**Gehl Institute**, 2021.
[DOWNLOAD HERE](https://issuu.com/gehlarchitects/docs/evaluation_framework_-_metro_tunnel_creative_progr)

> **Abstract** 
>
> The Melbourne Metro Tunnel Creative Program is a creative and placemaking strategy led by a team of designers, curators, and place managers dedicated to enhancing city life alongside the construction of the Metro Tunnel. This toolkit describes evaluation people-centermetrics and methods to measure the impact of projects in the public realm on everyday life.


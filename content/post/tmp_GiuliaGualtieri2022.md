
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The City at Eye Level for Kids"
date: 2022-01-01
summary: "If you want to co-design neighbourhoods and city centres that are healthy, safe, accessible, interactive and stimulating for children and their parents, here is the solution you need – The City at Eye Level for Kids Manual! The manual includes some of the various existing tools that can be used by p ..."
featureImage: ""
thumbnail: ""
categories: ['planning', 'landscape', 'architecture', 'urban design']
tags: ['age', 'public space', 'streetscape']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2022.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/city-eye-level-kids/)

> **Abstract** 
>
> If you want to co-design neighbourhoods and city centres that are healthy, safe, accessible, interactive and stimulating for children and their parents, here is the solution you need – The City at Eye Level for Kids Manual! The manual includes some of the various existing tools that can be used by practitioners, placemakers and decision-makers to include the children’s perspective into their projects. This booklet allows you to analyse locations critically in various scales, from the micro to the macro level, and it also provides customized tools for parents and children, for urban planners and for trainers. You can freely use one or several tools according to your needs and the type of stakeholders that are involved. Finally, the booklet presents a list of criteria to be considered when planning and creating spaces for children and their caretakers.


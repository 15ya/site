
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Waste Wise Cities"
date: 2020-01-01
summary: "UN-Habitat launched “Waste Wise Cities”, to address the increasing global waste management crisis. Waste from homes, markets, businesses and institutions is thrown on the streets, in drains, dumped next to communities or openly burnt. Not only does this make our cities unsightly and unattractive to  ..."
featureImage: ""
thumbnail: ""
categories: ['local government', 'planning']
tags: ['governance', 'services', 'sustainability']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/waste-wise-cities)

> **Abstract** 
>
> UN-Habitat launched “Waste Wise Cities”, to address the increasing global waste management crisis. Waste from homes, markets, businesses and institutions is thrown on the streets, in drains, dumped next to communities or openly burnt. Not only does this make our cities unsightly and unattractive to tourists and investors, it leads to flooding, air and water pollution, diseases as well as respiratory and other health problems. The urban poor are the most affected. Waste management operations account for a significant proportion of city budgets, but financing for waste management remains inadequate. 
However, cities can effectively tackle the waste management problem when regarding waste as a resource, contributing also to reducing global Green House Gas emissions and local air pollution. To be successful, cities must deal with their waste management issues in their context, while also learning from the experience of other cities.


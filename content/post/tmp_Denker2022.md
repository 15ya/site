
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "The Spiel Mobile"
date: 2022-01-01
summary: "The Spiel Mobile tool provides for mobile, pop-up consultation that is playful, engaging, and meaningful. It creates a space that attracts and engages people of all ages and abilities in a community to come together, and share their insights to feed into the design of playful spaces in their areas.  ..."
featureImage: "https://placemaking-europe.eu/wp-content/uploads/2023/05/PE_Toolbox_-_Spielmobile-1024x683.jpg"
thumbnail: "https://placemaking-europe.eu/wp-content/uploads/2023/05/PE_Toolbox_-_Spielmobile-1024x683.jpg"
categories: ['urban design']
tags: ['participation']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2022.
[DOWNLOAD HERE](https://placemaking-europe.eu/tool/the-spiel-mobile/)

> **Abstract** 
>
> The Spiel Mobile tool provides for mobile, pop-up consultation that is playful, engaging, and meaningful. It creates a space that attracts and engages people of all ages and abilities in a community to come together, and share their insights to feed into the design of playful spaces in their areas. It is particularly important that The Spiel Mobile only be used when the consultation will lead to real and tangible impact for the community. This manual has been provided by A Playful City, a not-for-profit organization that creates more playful, engaging and inclusive cities with and for communities. They can also help with the design or your Spiel Mobile.


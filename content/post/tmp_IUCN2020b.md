
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Guidelines for conserving connectivity through ecological networks and corridors"
date: 2020-01-01
summary: "Connectivity conservation is essential for managing healthy ecosystems, conserving biodiversity and adapting to climate change across all biomes and spatial scales. Well-connected ecosystems support a diversity of ecological functions such as migration, hydrology, nutrient cycling, pollination, seed ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-030-En.PNG?itok=mTD2nX3U"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-PAG-030-En.PNG?itok=mTD2nX3U"
categories: ['landscape']
tags: ['urban nature', 'sustainability', 'water', 'resilience', 'land use']  
showDate: false
showReadTime: false
---

**IUCN**, 2020.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/49061)

> **Abstract** 
>
> Connectivity conservation is essential for managing healthy ecosystems, conserving biodiversity and adapting to climate change across all biomes and spatial scales. Well-connected ecosystems support a diversity of ecological functions such as migration, hydrology, nutrient cycling, pollination, seed dispersal, food security, climate resilience and disease resistance. These Guidelines are based on the best available science and practice for maintaining, enhancing and restoring ecological connectivity among and between protected areas, other effective areas based conservation measures (OECMs) and other intact ecosystems. For the first time, this publication introduces a common definition and recommends formal recognition of ecological corridors to serve as critical building blocks of ecological networks in conjunction with protected areas and OECMs. Furthermore, these Guidelines also include 25 case studies that demonstrate current approaches to conserving ecological connectivity and ecological networks for different ecosystems and species, and at different spatial and temporal scales.


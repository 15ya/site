
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Urban Eats: how cities can leverage opportunities to build resilient food systems through circular pathways"
date: 2022-01-01
summary: "Globally, 150 million urban residents are food insecure, yet, cities generate 2.8 billion tons of organic waste each year. More people could be pushed into food insecurity as global food prices are on track to rise 23% this year, according to the World Bank.  
Cities are particularly vulnerable to s ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/u/urban-eats-cover.jpg?h=241&mw=340&w=340&hash=EEE118CD01CB6D100E95834A9BE3B92E"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/u/urban-eats-cover.jpg?h=241&mw=340&w=340&hash=EEE118CD01CB6D100E95834A9BE3B92E"
categories: ['local government', 'planning']
tags: ['food', 'resilience', 'sustainability', 'urban nature']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://www.arup.com/perspectives/publications/research/section/urban-eats-how-cities-can-leverage-opportunities-to-build-resilient-food-systems)

> **Abstract** 
>
> Globally, 150 million urban residents are food insecure, yet, cities generate 2.8 billion tons of organic waste each year. More people could be pushed into food insecurity as global food prices are on track to rise 23% this year, according to the World Bank.  
Cities are particularly vulnerable to shocks related to food prices, food production and food supply chain as they consume large quantities of food and are unable to independently produce sufficient food to feed its residents. 
Cities must take urgent action to reduce, redistribute and repurpose food waste. By tackling food waste management which drastically influences the food system, cities can improve overall food security while reducing greenhouse gas emissions and creating opportunities for green economic growth.
This report, with Resilient Cities Network and Arup, highlights vulnerabilities in our urban food systems, but also presents opportunities for cities to reduce food insecurity through resilient and circular economy approaches.


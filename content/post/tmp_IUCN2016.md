
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A reef manager’s guide to fostering community stewardship"
date: 2016-01-01
summary: "This guide has been written to inspire and empower reef managers and community leaders to develop reef stewardship programs. Stewardship is a way of empowering local communities to take a more active role in sustaining the natural resources on which they depend. ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2016-052.JPG?itok=gKS3hlZZ"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2016-052.JPG?itok=gKS3hlZZ"
categories: ['landscape', 'urban design']
tags: ['water', 'urban nature', 'participation', 'nbs', 'sustainability', 'resilience']  
showDate: false
showReadTime: false
---

**IUCN**, 2016.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/46289)

> **Abstract** 
>
> This guide has been written to inspire and empower reef managers and community leaders to develop reef stewardship programs. Stewardship is a way of empowering local communities to take a more active role in sustaining the natural resources on which they depend.


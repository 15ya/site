
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Community Cans Artmaking Guide"
date: 2021-01-01
summary: "The Community Cans Artmaking Guide grew from projects and partnerships in the City of Philadelphia to reduce the amount of litter found in the streets and water bodies. 
“The Community Cans Program was founded in 2018 as a collaboration between the City of Philadelphia Zero Waste and Litter Cabinet  ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/06/CC-artmaking-guide-cover-231x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2021/06/CC-artmaking-guide-cover-231x300.jpg"
categories: ['landscape', 'urban design']
tags: ['art', 'public space', 'participation']  
showDate: false
showReadTime: false
---

**City of Philadelphia Zero Waste and Litter Cabinet**, 2021.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/community-cans-artmaking-guide-june-2021/)

> **Abstract** 
>
> The Community Cans Artmaking Guide grew from projects and partnerships in the City of Philadelphia to reduce the amount of litter found in the streets and water bodies. 
“The Community Cans Program was founded in 2018 as a collaboration between the City of Philadelphia Zero Waste and Litter Cabinet (the Cabinet), Philadelphia Streets Department, and Philadelphia Commerce Department…In 2018, the Partnership for the Delaware Estuary (PDE) received grant funding from the United States Environmental Protection Agency to do a project that would reduce litter flowing into storm drains in the city of Philadelphia. Along with recognizing that community organizations need assistance with funding the program in their neighborhoods, the Cabinet also recognizes that many groups need assistance getting an artist to create the designs on the lids of the cans. So, a portion of the grant was used to engage Philadelphia’s Mural Arts Institute, which assisted community partners with lid design, and with the creation of this guide helping future groups understand the process of hiring an artist and designing and painting the lids.”



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Planning for climate change: Guide - A strategic, values-based approach for urban planners"
date: 2014-01-01
summary: "This report was developed for city planners to better understand, assess and take action on climate change at the local level. Specifically targeted to the needs of planners and allied professionals in low and middle-income countries where the challenges of planning for climate change are particular ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/07/Planning-for-Climate-Change-1.jpg.webp?itok=HPvf3FzB"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2014/07/Planning-for-Climate-Change-1.jpg.webp?itok=HPvf3FzB"
categories: ['planning', 'local government']
tags: ['governance', 'sustainability', 'resilience', 'climate change']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2014.
[DOWNLOAD HERE](https://unhabitat.org/planning-for-climate-change-guide-a-strategic-values-based-approach-for-urban-planners)

> **Abstract** 
>
> This report was developed for city planners to better understand, assess and take action on climate change at the local level. Specifically targeted to the needs of planners and allied professionals in low and middle-income countries where the challenges of planning for climate change are particularly high.
The guide's strategic,values based planning framework: - promotes a participatory planning process that integrates local participation and good decision-making. - provides practical tools for addressing climate change through different urban planning processes. - supports the "mainstreaming" of climate change actions into other local government policy instruments.


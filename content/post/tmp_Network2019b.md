
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Designing and Implementing a Pro-Poor Land Recordation System"
date: 2019-01-01
summary: "The challenges to tenure security in both urban and rural areas are not only large, but they are increasing due to the different types of pressures making land more and more scarce. There is growing acceptance that only by recognizing and supporting a continuum of land rights, can tenure security be ..."
featureImage: "https://gltn.net/wp-content/uploads/2019/09/Designing-and-Implementing-a-Pro-Poor-Land-679x880.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2019/09/Designing-and-Implementing-a-Pro-Poor-Land-679x880.jpg"
categories: ['local government']
tags: ['governance', 'informality', 'land use', 'equity']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2019.
[DOWNLOAD HERE](https://gltn.net/download/designing-and-implementing-a-pro-poor-land-recordation-system/)

> **Abstract** 
>
> The challenges to tenure security in both urban and rural areas are not only large, but they are increasing due to the different types of pressures making land more and more scarce. There is growing acceptance that only by recognizing and supporting a continuum of land rights, can tenure security be reached for all people in an inclusive way. 
GLTN’s partner network began implementing this vision in 2006 with the development of land tools, 18 of which have now been designed and tested, and are increasingly implemented at scale. The pro-poor land recordation system outlined in this publication is one of these tools, and is designed to be implemented on its own or, better, in combination with other land tools to reach inclusive tenure security.


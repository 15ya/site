
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Smart City Guidance Package"
date: 2019-01-01
summary: "The Smart City Guidance Package (SCGP) helps to plan and implement smart city and low energy district projects in an integrated way by describing common situations and giving real-life examples. It bundles the generously shared experiences and expertise of cities, businesses, citizens, research inst ..."
featureImage: "https://smart-cities-marketplace.ec.europa.eu/sites/default/files/styles/node_image/public/2019-07/Capture.JPG?itok=6w2Ea4Bn"
thumbnail: "https://smart-cities-marketplace.ec.europa.eu/sites/default/files/styles/node_image/public/2019-07/Capture.JPG?itok=6w2Ea4Bn"
categories: ['local government', 'planning', 'urban design']
tags: ['services', 'finance', 'public space', 'mobility', 'data', 'sustainability', 'governance', 'participation', 'energy']  
showDate: false
showReadTime: false
---

**EIP-SCC**, 2019.
[DOWNLOAD HERE](https://smart-cities-marketplace.ec.europa.eu/news-and-events/news/2019/smart-city-guidance-package)

> **Abstract** 
>
> The Smart City Guidance Package (SCGP) helps to plan and implement smart city and low energy district projects in an integrated way by describing common situations and giving real-life examples. It bundles the generously shared experiences and expertise of cities, businesses, citizens, research institutes and Non-Governmental Organisations (NGOs) that work together in the European Innovation Partnership Smart Cities and Communities (EIP-SCC). 
Across the world, many cities and urban stakeholders have the ambition to create sustainable cities, adjusted to the era of digitalization, which are pleasant to live in. The wealth of urban data, the increased connectivity of urban objects through the Internet-of-Things (IoT) and advanced ICT, energy and mobility technologies, have opened new avenues for the application of smart solutions and the transition to clean energy and mobility systems in cities. Cities are looking into this potential, experimenting in living labs and applying smart technologies in ambitious integrated projects, such as the Horizon2020 SCC-01 lighthouse projects. 
However, our current approaches to the integrated planning and management of smart city and low energy district projects are not sufficiently taking into account the full life-cycle of planned investments in the built environment, and the entire community influenced by them. This requires a genuine long-term perspective beyond the political cycle at the heart of any smart city or low energy district strategy, more inclusive participatory and consultation processes, novel business models and better collaboration within and across traditional policy and administrative boundaries. If these conditions are not in place,projects might be difficult to prepare and implement, underperforming in terms of reduction of CO2 and energy use, or not valued by end-users.


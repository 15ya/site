
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Eye Level Game: How to analyse a space or area based on the human scale principles?"
date: 2019-01-01
summary: "Bring new liveliness in your street by hosting an Eye Level Game: celebrate the importance of human scale and approach an urban area from the pedestrian’s point of view. This game will not only help with the engagement of the local community in improving their own street, it will also give those wit ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'architecture', 'urban design']
tags: ['participation', 'governance', 'public space', 'streetscape']  
showDate: false
showReadTime: false
---

**City at Eye Level**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/eye-level-game/)

> **Abstract** 
>
> Bring new liveliness in your street by hosting an Eye Level Game: celebrate the importance of human scale and approach an urban area from the pedestrian’s point of view. This game will not only help with the engagement of the local community in improving their own street, it will also give those with a background in urban planning, development, urban design and architecture new perspectives on streets and public places.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Where to Start? A Guide to Land-Based Finance in Local Governance"
date: 2020-01-01
summary: "One of the most significant challenges facing urban authorities in developing countries is finding the funds needed to support and sustain urban development. Without funds, the authorities are unable to meet the ever-growing demand for basic services, maintenance and new infrastructure – including l ..."
featureImage: "https://gltn.net/wp-content/uploads/2020/04/Where-to-Start-A-Guide-to-Land-based-Finance-in-Local-Governance-1-693x880.jpg"
thumbnail: "https://gltn.net/wp-content/uploads/2020/04/Where-to-Start-A-Guide-to-Land-based-Finance-in-Local-Governance-1-693x880.jpg"
categories: ['local government']
tags: ['finance', 'urban development', 'land use', 'informality', 'equity']  
showDate: false
showReadTime: false
---

**Global Land Tool Network**, 2020.
[DOWNLOAD HERE](https://gltn.net/2020/04/07/where-to-start-a-guide-to-land-based-finance-in-local-governance/)

> **Abstract** 
>
> One of the most significant challenges facing urban authorities in developing countries is finding the funds needed to support and sustain urban development. Without funds, the authorities are unable to meet the ever-growing demand for basic services, maintenance and new infrastructure – including land services that ensure tenure security. 
One source of revenue is taxes – specifically land tax – which has proven to be a successful tax base in many countries and is receiving increasing attention in many others. This Guide aims to expand the understanding of local leaders on taxes and fees related to land and the advantages of this approach, it provides a roadmap for taking an inventory of the local context, it discusses the institutional and societal challenges that must be overcome, and describes the process for developing an action plan intended to convert aspirations into actions. 
Together with the UN-Habitat and GLTN training package Leveraging Land: Land-based finance for local governments, this Guide contains many examples, strategies and resources that make it an essential tool to meet the challenge of finding locally appropriate solutions for delivering effective services.


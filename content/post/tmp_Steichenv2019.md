
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "In the Eye of the Storm: A People’s Guide to Transforming Crisis & Advancing Equity in the Disaster Continuum"
date: 2019-01-01
summary: "“This toolkit is designed to guide NAACP units and their Environmental and Climate Justice (ECJ) Committees through the process of building equity into the four phases of emergency management: prevention and mitigation, preparedness and resilience building, response and relief, and recovery and rede ..."
featureImage: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/06/NAACP-eye-of-storm-231x300.jpg"
thumbnail: "https://d3h55oe312fhj3.cloudfront.net/wp-content/uploads/2019/06/NAACP-eye-of-storm-231x300.jpg"
categories: ['planning']
tags: ['governance', 'risk reduction', 'participation', 'climate change', 'equity']  
showDate: false
showReadTime: false
---

**National Association for the Advancement of Colored People**, 2019.
[DOWNLOAD HERE](https://urbanwaterslearningnetwork.org/resources/in-the-eye-of-the-storm-a-peoples-guide-to-transforming-crisis-advancing-equity-in-the-disaster-continuum-june-2019/)

> **Abstract** 
>
> “This toolkit is designed to guide NAACP units and their Environmental and Climate Justice (ECJ) Committees through the process of building equity into the four phases of emergency management: prevention and mitigation, preparedness and resilience building, response and relief, and recovery and redevelopment. The first module introduces big picture concepts on equity based emergency management and each of the following modules covers an individual emergency management phase. Each module can stand-alone and some communities might find that certain modules are more relevant to their community’s needs than others. With that said, we recommend reviewing each of the modules in this toolkit to ensure that equity is built into each phase of the emergency management continuum.”
Modules include:
The Big Picture on Equity Based Emergency Management
Emergency Prevention and Mitigation
Emergency Preparedness and Resilience Building
Emergency Response and Relief
Emergency Recovery and Redevelopment
Advocating for Equity in Emergency Management Policy


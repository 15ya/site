
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Public Space Site-Specific Assessment: Guidelines to Achieve Quality Public Spaces at Neighbourhood Level"
date: 2020-01-01
summary: "The Site-specific Assessment consists of a series of activities and tools to understand the quality of public spaces and influence, through a participatory process, the design of the site. The assessment focuses on a selected open public space and its five minutes walking radius (equivalent to 400-m ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/Public%20space%20site%20specific.png.webp?itok=3LstJ_RY"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2020/07/Public%20space%20site%20specific.png.webp?itok=3LstJ_RY"
categories: ['architecture', 'landscape', 'urban design']
tags: ['public space', 'participation']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2020.
[DOWNLOAD HERE](https://unhabitat.org/public-space-site-specific-assessment-guidelines-to-achieve-quality-public-spaces-at-neighbourhood)

> **Abstract** 
>
> The Site-specific Assessment consists of a series of activities and tools to understand the quality of public spaces and influence, through a participatory process, the design of the site. The assessment focuses on a selected open public space and its five minutes walking radius (equivalent to 400-meter distance) referred in the document as the ‘walkable radius’. The guideline supports the user on how to gather the right data and what information is needed within the selected area in order to come up with adequate design and planning solutions.
While many upgrading projects adopt civic engagement activities, the transition phase between community needs and the expert designs is often lost in translation. The tool ensures that the conversation between the community and the experts is bridged through a series of activities that provides a platform for exchange between the different parties. This demand a certain level of participation from community members, technical experts and the local authorities in each of the listed activities.
The result from the assessment is a collection of qualitative and quantitative information gathered by and with the community and are used to measure public space indicators that will inform the design elaborated by the experts. In this process, municipalities are guided about how and where to allocate resources for upgrading public spaces that contributes to SDG11.7 - By 2030, provide universal access to safe, inclusive and accessible, green and public spaces, in particular for women and children, older persons and persons with disabilities



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Pop-up cafe tool manual"
date: 2020-01-01
summary: "Pop-up cafes are an easy and fun way to engage with your local community and a great way to get in touch with the people living and using the neighbourhood. The pop-up cafe tool manual walks you through all the steps it takes for you to organise your event. You can realise a pop-up cafe with two cha ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'urban design']
tags: ['participation', 'public space']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2020.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/popup-cafe/)

> **Abstract** 
>
> Pop-up cafes are an easy and fun way to engage with your local community and a great way to get in touch with the people living and using the neighbourhood. The pop-up cafe tool manual walks you through all the steps it takes for you to organise your event. You can realise a pop-up cafe with two chairs and a bit of coffee or create a theme pop-up cafe street party. Read the manual and get started to meet and enjoy time with your local community.


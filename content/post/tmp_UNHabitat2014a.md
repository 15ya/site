
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "A New Strategy of Sustainable Neighbourhood Planning: Five principles - Urban Planning"
date: 2014-01-01
summary: "UN-Habitat supports countries to develop urban planning methods and systems to address current urbanization challenges such as population growth, urban sprawl, poverty, inequality, pollution, congestion, as well as urban biodiversity, urban mobility, and energy. The proposed approach is based on 5 p ..."
featureImage: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/10/Pages%20from%2064.%205%20principles%20of%20neighbourhood%20design.jpg.webp?itok=fWHZnFjW"
thumbnail: "https://unhabitat.org/sites/default/files/styles/cover_image_lg/public/2019/10/Pages%20from%2064.%205%20principles%20of%20neighbourhood%20design.jpg.webp?itok=fWHZnFjW"
categories: ['local government', 'planning', 'urban design']
tags: ['urban nature', 'governance', 'streetscape', 'housing', 'land use', 'infrastructure', 'mobility', 'equity']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2014.
[DOWNLOAD HERE](https://unhabitat.org/five-principles-of-neighbourhood-design)

> **Abstract** 
>
> UN-Habitat supports countries to develop urban planning methods and systems to address current urbanization challenges such as population growth, urban sprawl, poverty, inequality, pollution, congestion, as well as urban biodiversity, urban mobility, and energy. The proposed approach is based on 5 principles that support the 3 key features of sustainable neighbourhoods and cities: compact, integrated, and connected. These principles are:  1. Adequate space for streets and an efficient street network  2. High density -at least 15,000 people per km²  3. Mixed land-use  4. Social mix  5. Limited land-use specialization


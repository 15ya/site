
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Impact Measurement Tool: How do I measure impact as a placemaker?"
date: 2019-01-01
summary: "Placemakers are creating multiple values, as proven by diverse research studies in the field. With local governments and semi-governments stepping back, the societal challenges of our cities need those initiatives. Placemakers are in a constant process of proving themselves and justifying the value  ..."
featureImage: ""
thumbnail: ""
categories: ['landscape', 'architecture', 'urban design']
tags: ['public space', 'streetscape', 'participation']  
showDate: false
showReadTime: false
---

**Placemaking Europe Toolbox**, 2019.
[DOWNLOAD HERE](https://placemaking-europe.eu/toolbox/impact-measurement/)

> **Abstract** 
>
> Placemakers are creating multiple values, as proven by diverse research studies in the field. With local governments and semi-governments stepping back, the societal challenges of our cities need those initiatives. Placemakers are in a constant process of proving themselves and justifying the value of their work to the different stakeholders in order to convince them to invest differently. Doing so, placemakers stand much stronger when they have data demonstrating the “soft” impact of their projects (social, human, cultural) as well as their “hard” impact (financial, physical). Therefore, carrying out the impact assessment of their projects is a means for placemakers to learn how to improve their work to achieve their mission, to convince the different stakeholders that placemaking is actually creating value for everyone and to build the project with them, aiming at a bigger impact on the long term.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "European guidelines on protected areas and invasive alien species"
date: 2014-01-01
summary: "Invasive alien species (IAS) have been identified as one of the most important direct drivers of biodiversity loss and ecosystem service changes. Many international policy instruments, guidelines and technical tools have been developed to address this threat. The Bern Convention, with the technical  ..."
featureImage: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2014-070.JPG?itok=mL3-SYS6"
thumbnail: "https://portals.iucn.org/library/sites/library/files/styles/publication/public/book_covers/BC-2014-070.JPG?itok=mL3-SYS6"
categories: ['landscape']
tags: ['urban nature', 'climate change', 'sustainability']  
showDate: false
showReadTime: false
---

**IUCN**, 2014.
[DOWNLOAD HERE](https://portals.iucn.org/library/node/45003)

> **Abstract** 
>
> Invasive alien species (IAS) have been identified as one of the most important direct drivers of biodiversity loss and ecosystem service changes. Many international policy instruments, guidelines and technical tools have been developed to address this threat. The Bern Convention, with the technical support of the IUCN SSC Invasive Species Specialist Group, is developing a series of voluntary instruments (codes of conduct and guidelines) covering a number of industries, activities or contexts potentially responsible for the introduction of alien species. The aim of these guidelines is to present key principles that should be adopted for protected areas (PAs), in order to prevent and manage the threat of invasive species at the local, national and supra-national scale. They are aimed mainly at PA managers and staff, practitioners, decision makers at all levels, and local communities. These European Guidelines on Protected Areas and IAS should be considered as an implementation of the European Strategy on Invasive Alien Species and aims to contribute to the ongoing development of the EU strategy on IAS.



---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Light, Nature, Architecture"
date: 2023-01-01
summary: "A Guide to Holistic Lighting Design. In this planning guide, the renowned lighting designer Ulrike Brandi documents all her findings on the topics of lighting design, daylight, sustainability and healthy living spaces. ..."
featureImage: "https://image.isu.pub/230411140226-64c7c9d0c38f8fb3150f7957a5c13aa7/jpg/page_1_thumb_large.jpg"
thumbnail: "https://image.isu.pub/230411140226-64c7c9d0c38f8fb3150f7957a5c13aa7/jpg/page_1_thumb_large.jpg"
categories: ['architecture', 'landscape']
tags: ['urban nature', 'sustainability', 'health', 'housing']  
showDate: false
showReadTime: false
---

**Ulrike Brandi**, 2023.
[DOWNLOAD HERE](https://issuu.com/birkhauser.ch/docs/look_inside_light_nature_architecture)

> **Abstract** 
>
> A Guide to Holistic Lighting Design. In this planning guide, the renowned lighting designer Ulrike Brandi documents all her findings on the topics of lighting design, daylight, sustainability and healthy living spaces.


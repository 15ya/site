
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "CONSUL DEMOCRACY"
date: 2015-01-01
summary: "CONSUL DEMOCRACY is the most complete citizen participation tool for an open, transparent and democratic government. ..."
featureImage: "https://consulproject.org/img/sdg/sdg_consul_en.png"
thumbnail: "https://consulproject.org/img/sdg/sdg_consul_en.png"
categories: ['local government', 'planning']
tags: ['participation']  
showDate: false
showReadTime: false
---

**UN Habitat**, 2015.
[DOWNLOAD HERE](https://consulproject.org/en/)

> **Abstract** 
>
> CONSUL DEMOCRACY is the most complete citizen participation tool for an open, transparent and democratic government.


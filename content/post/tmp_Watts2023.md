
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Modern Construction Handbook"
date: 2023-01-01
summary: "Due to its regular revisions, the Modern Construction Handbook has become a classic of advanced building construction literature, not least because of its clear structure with the chapters Material, Wall, Roof, Supporting Structure, Environment and Applications. ..."
featureImage: "https://birkhauser.com/_next/image?url=https%3A%2F%2Fbirk-data-feed.s3.eu-central-1.amazonaws.com%2F624ece3cb79f851d94a47b0c%2Fcover-image-original--0.70708585.jpg&w=1920&q=75"
thumbnail: "https://birkhauser.com/_next/image?url=https%3A%2F%2Fbirk-data-feed.s3.eu-central-1.amazonaws.com%2F624ece3cb79f851d94a47b0c%2Fcover-image-original--0.70708585.jpg&w=1920&q=75"
categories: ['architecture']
tags: ['infrastructure', 'housing']  
showDate: false
showReadTime: false
---

**Andrew Watts**, 2023.
[DOWNLOAD HERE](https://issuu.com/birkhauser.ch/docs/modern_construction_handbook_6a_look_inside)

> **Abstract** 
>
> Due to its regular revisions, the Modern Construction Handbook has become a classic of advanced building construction literature, not least because of its clear structure with the chapters Material, Wall, Roof, Supporting Structure, Environment and Applications.


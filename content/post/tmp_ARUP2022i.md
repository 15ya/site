
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "Exploring sustainable food systems"
date: 2022-01-01
summary: "A sustainable, resilient and equitable food system will be fundamental to feeding a growing and increasingly urban population in the future. 
Today’s food system is one of the greatest contributors to climate change at the same time as being at the greatest risk from a warming climate, from loss of  ..."
featureImage: "https://www.arup.com/-/media/arup/images/publications/s/sustainable-food-systems/exploring-sustainable-food-systems-cover.jpg?h=191&mw=340&w=340&hash=83A92F1537DD1FA7C2998935BF5A7E9D"
thumbnail: "https://www.arup.com/-/media/arup/images/publications/s/sustainable-food-systems/exploring-sustainable-food-systems-cover.jpg?h=191&mw=340&w=340&hash=83A92F1537DD1FA7C2998935BF5A7E9D"
categories: ['local government', 'planning', 'landscape', 'urban design']
tags: ['food', 'urban nature', 'sustainability', 'energy']  
showDate: false
showReadTime: false
---

**ARUP**, 2022.
[DOWNLOAD HERE](https://indd.adobe.com/view/871dd334-afa8-493b-a1be-1101a8872f0d)

> **Abstract** 
>
> A sustainable, resilient and equitable food system will be fundamental to feeding a growing and increasingly urban population in the future. 
Today’s food system is one of the greatest contributors to climate change at the same time as being at the greatest risk from a warming climate, from loss of biodiversity to less arable land. There has never been a greater opportunity to imagine and create a resilient food system that provides healthy food for everyone, while supporting the biosphere of a thriving planet. 
This timely report examines the global trends shaping the key stages of the food system, from agriculture and production to our supply chains and consumption habits. The report examines how the food system can work together to become truly sustainable.


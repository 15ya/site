+++
title = "Manifesto"
description = "Manifesto"
aliases = ["about-us", "contact", "manifesto"]
author = "Vija VIESE"
showDate= false
showShare= false
showReadTime= false
+++



Fellow urban professionals, 

Our cities have an immense impact on the [environment](https://www.iea.org/reports/global-status-report-for-buildings-and-construction-2019) - from the pollution caused by manufacturing and building to the depletion of natural resources, to the energy used to heat, cool, and maintain our homes.
With only a few years left to achieve the [Sustainable Development Goals](https://www.undp.org/sustainable-development-goals), we, as urban professionals, have the responsibility to build better, to educate ourselves and to raise awareness among the people around us.
New sustainable technologies and methods are developed every day, but research shows that in some professional fields it takes up to 15 years for these innovations to become commonly accepted into practice. 

**Well, we simply do not have 15 years to wait anymore.
We have the [knowledge](https://www.ipcc.ch/report/sixth-assessment-report-cycle/); we only need the will.**

There is no time to waste; it must start with us, the people on the ground – drawing, designing, developing.
We must take action to bridge this gap between research and practice in our own work and lead by example.


What do we need to start implementing sustainable practices today? We need roadmaps, we need frameworks, we need concrete steps, and we need accountability.
A myriad of ready-to-use tools and guides already exist, mostly hidden away in depths of online databases of expert organizations creating them.

15 Years Ahead is here to guide you directly to the tools you need.
We believe that the implementation gap is often due to not knowing that tools exist or not having access to them.
Our mission is to find the most useful and easily applicable tools for all urban professionals and to bring these amazing resources to light, thus contributing in our own way to accelerating the just transition and localizing the SDGs.

**Act now - think 15 Years Ahead!** 

![](../logos/15YA-Visuals_Logo-think-pink.png)

We do not own or store any of the tools accessible from the website. The database is not comprehensive and is constantly evolving. For any suggestions on items to add or problems with existing items (broken links, wrong images, etc.), please **[contact us](mailto:vija.viese@gmail.com)**.

"""
This scripts read a bib file in `bib/` and turns each entry into a post under `content/post/`
"""

import bibtexparser


# Groups that go in CATEGORIES instead of TAGS
TAGEGORIES = (
        'local government',
        'architecture',
        'landscape',
        'urban design',
        'planning',
        )

def entry_to_str(entry:dict):
    """Affect keys from entry to markdown string"""

    #                bibtex                # Hugo content
    entry.setdefault("title",        '')# title
    entry.setdefault("featureimage", '')# TTODO
    entry.setdefault("groups",       '')# tags or categories
    entry.setdefault("abstract",     '')# abstract
    entry.setdefault("note",         '')# image

    if not entry["note"].startswith('http'):
        entry["note"] = ''

    groups = entry['groups'].lower().split(',')
    cats = []
    tags = []

    #print(entry['groups'])
    #print(groups)
    for grp in groups:
        grp_ = grp.strip().lower()
        if grp_ in TAGEGORIES:
            #print(grp_+' goes to cats')
            cats +=  [grp_]
        else:
            #print(grp_+' goes to tags')
            tags +=  [grp_]

    #print(cats)
    #print(tags)

    post_str = r'''
---
NOTE: "Generated from bibfile with bib/database_to_content.py"
title: "{title}"
date: {year}-01-01
summary: "{summary}"
featureImage: "{featureimage}"
thumbnail: "{featureimage}"
categories: {categories}
tags: {tags}  
showDate: false
showReadTime: false
---

**{inst}**, {year}.
[DOWNLOAD HERE]({url})

> **Abstract** 
>
> {abstract}

'''.format(
            title=entry["title"].replace("{","").replace('}',''),
            featureimage=entry["note"],
            inst=entry['institution'].replace("{","").replace('}',''),
            abstract=entry["abstract"],
            url=entry['url'].replace("{","").replace('}','').replace(" ", ""),
            tags=tags,
            categories=cats,
            year=entry['year'].replace("{","").replace('}',''),
            summary=entry["abstract"][:300]+' ...',
            )
    return post_str

with open('DATABASE.bib', encoding='utf8') as bibtex_file:
    DB = bibtexparser.load(bibtex_file)

DBD = DB.entries_dict

for item in DBD.items():

    print(item[0])

    filename = '../content/post/tmp_'+item[0]+'.md'
    FILESTRING = entry_to_str(item[1])

    # Writing to file
    with open(filename, "w", encoding='utf8') as file1:
        # Writing data to a file
        file1.writelines(FILESTRING)

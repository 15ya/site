[![logo](./static/logos/15YA-Visuals_Logo-think-pink.png)](https://15ya.gitlab.io/site)

*15 Years Ahead is an urban toolkit aggregator to help diverse urban professionals implement best practices in their work in accelerating the just transition and localizing the SDGs.*


- This is a [Hugo](https://gohugo.io) website using [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and theme [Clarity](https://neonmirrors.net/post/2020-07/clarity-theme-for-hugo-released/).
- The database is curated by [Vija Viese](https://www.linkedin.com/in/vija-viese/) and the site maintained by [Baptiste Reyne](https://orcid.org/0000-0003-2310-864X).
- Pages (posts) are generated from a `bibtex` file using `python/bibtexparser` (see `/bib/`).

#[GO TO SITE](https://15ya.gitlab.io/site)
